﻿#include "pch.h"
#include "ResourceManager.h"
#include "Label.h"
#include "Texture.h"
#include "Common/DirectXHelper.h"
#include "Common/DeviceResources.h"
#include "TiledMap.h"
#include "tinyxml.h"
#include "XmlHelper.h"
#include "SpriteSheetHelper.h"

using namespace DX;
using namespace Tringine;
using namespace Tringine::Util;

int ResourceManager::NextSpritesheetId = 1;
ResourceManager* ResourceManager::Instance = nullptr;
std::vector< std::shared_ptr<ResourceDescription>> ResourceManager::LoadedResources = std::vector< std::shared_ptr<ResourceDescription>>();

ResourceManager::ResourceManager()
{
}

ResourceManager* ResourceManager::GetInstance()
{
	if (Instance == nullptr)
	{
		Instance = new ResourceManager();
	}
	return Instance;
}

void ResourceManager::RequestResources(std::vector< std::shared_ptr<ResourceDescription>> requestedResources)
{
	std::vector< std::shared_ptr<ResourceDescription>> toRemove;
	std::vector< std::shared_ptr<ResourceDescription>> toLoad;
	for (int loadedResourceIndex = 0; loadedResourceIndex < LoadedResources.size(); loadedResourceIndex++)
	{
		std::shared_ptr < ResourceDescription> description = LoadedResources[loadedResourceIndex];
		if (description->GetScope() != ResourceScope::RS_Stage)
		{
			toRemove.push_back(description);
		}
	}

	for (int descriptionIndex = 0; descriptionIndex < requestedResources.size(); descriptionIndex++)
	{
		std::shared_ptr<ResourceDescription> description = requestedResources[descriptionIndex];
		bool alreadyLoaded = false;
		for (int loadedResourceIndex = 0; loadedResourceIndex < LoadedResources.size(); loadedResourceIndex++)
		{
			std::wstring loadedName = LoadedResources[loadedResourceIndex]->GetName();
			if (loadedName == description->GetName())
			{
				alreadyLoaded = true;
				break;
			}
		}
		if (alreadyLoaded)
		{
			for (int toRemoveIndex = 0; toRemoveIndex < toRemove.size(); toRemoveIndex++)
			{
				if (toRemove[toRemoveIndex] == description)
				{
					toRemove.erase(toRemove.begin() + toRemoveIndex);
					break;
				}
			}
			continue;
		}

		toLoad.push_back(description);
	}

	for (int toRemoveIndex = 0; toRemoveIndex < toRemove.size(); toRemoveIndex++)
	{
		RemoveResource(toRemove[toRemoveIndex]);
	}

	for (int toLoadIndex = 0; toLoadIndex < toLoad.size(); toLoadIndex++)
	{
		LoadResource(toLoad[toLoadIndex]);
	}
}

void ResourceManager::LoadResource(std::shared_ptr <ResourceDescription> description)
{
	OutputDebugString((description->GetName() + L"\n").data());
	switch (description->GetType())
	{
	case ResourceType::RT_Font:
		LoadFont(*description);
		break;
	case ResourceType::RT_Map:
		LoadMapFromFile(*description);
		break;
	case ResourceType::RT_SpriteSheet:
		LoadAnimationsFromSpriteSheetFile(*description);
		break;
	case ResourceType::RT_Texture:
		LoadTextureFromFile(*description);
		break;
	}
	LoadedResources.push_back(description);
}

void ResourceManager::RemoveResource(std::shared_ptr<ResourceDescription> description)
{
	for (int loadedResourceIndex = 0; loadedResourceIndex < LoadedResources.size(); loadedResourceIndex++)
	{
		std::shared_ptr<ResourceDescription> loadedDescription = LoadedResources[loadedResourceIndex];
		if (loadedDescription == description)
		{
			LoadedResources.erase(LoadedResources.begin() + loadedResourceIndex);
			break;
		}
	}

	switch(description->GetType())
	{
	case ResourceType::RT_Font:
		Font::Remove(description->GetName());
		break;
	case ResourceType::RT_Map:
		TiledMap::Remove(description->GetName());
		break;
	case ResourceType::RT_SpriteSheet:
		Animation::Remove(description->GetName());
		break;
	case ResourceType::RT_Texture:
		Texture::Remove(description->GetName());
		break;
	}
	delete &description;
}

void ResourceManager::LoadFont(ResourceDescription &description)
{
	std::wifstream fileStream;
	fileStream.open(description.GetPath());

	std::wstring part;

	bool isOpen = fileStream.is_open();

	// extract font Name
	std::shared_ptr<Font> font = std::make_shared<Font>();

	fileStream >> part >> part; //info face="Arial"
	font->Name = GetAttributeStringValue(part);

	// get font Size
	fileStream >> part; //Size=73
	font->Size = GetAttributeIntValue(part);

	//bold, italic, charset, unicode, stretchH, smooth, aa, padding, spacing
	fileStream >> part >> part >> part >> part >> part >> part >> part; //bold=0 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 

	//get padding
	fileStream >> part; //padding=5,5,5,5 
	part = GetAttributeValue(part);//5,5,5,5

	font->PixelPadding.Top = (float)std::stoi(GetAttributeValueAtIndex(part, 0));
	font->PixelPadding.Right = (float)std::stoi(GetAttributeValueAtIndex(part, 1));
	font->PixelPadding.Bottom = (float)std::stoi(GetAttributeValueAtIndex(part, 2));
	font->PixelPadding.Left = (float)std::stoi(GetAttributeValueAtIndex(part, 3));

	fileStream >> part; //spacing=0,0

	//Get LineHeight (how much to move down for each line), and normalize (between 0.0 and 1.0 based on Size of font)
	fileStream >> part >> part >> part; //common lineHeight=95
	font->PixelLineHeight = (float)GetAttributeIntValue(part);

	//Get base height (height of all characters), and normalize (between 0.0 and 1.0 based on Size of font)
	fileStream >> part; //base=68
	font->PixelBaseHeight = (float)GetAttributeIntValue(part);

	//Get texture width
	fileStream >> part; //scaleW=512
	float textureWidth = (float)GetAttributeIntValue(part);

	//Get texture height
	fileStream >> part; //scaleH=512
	float textureHeight = (float)GetAttributeIntValue(part);

	//get pages, packed, redch, greench, bluech, alphach, page id
	fileStream >> part >> part >> part >> part >> part >> part; // pages=1 packed=0
	fileStream >> part >> part; //page id=0

	//get texture filename
	std::wstring wtmp;
	fileStream >> wtmp; 

	//file="Arial.png"
	wtmp = GetAttributeValue(wtmp);
	font->FontImage = wtmp.substr(1, wtmp.length() - 2);

	//Get number of characters
	fileStream >> part >> part; //chars count=97
	font->numCharacters = GetAttributeIntValue(part);

	//initialize the character list
	font->CharPointerList = new FontChar[font->numCharacters];

	for (int charIndex = 0; charIndex < font->numCharacters; ++charIndex)
	{
		FontChar *currentChar = &font->CharPointerList[charIndex];
		//Get unicode id
		fileStream >> part >> part;//char id=0
		currentChar->id = GetAttributeIntValue(part);

		//Get x
		fileStream >> part;//x=392
		currentChar->TexturePosition.X = (float)GetAttributeIntValue(part) / textureWidth;

		//Get y
		fileStream >> part; //y=340
		currentChar->TexturePosition.Y = (float)GetAttributeIntValue(part) / textureHeight;

		//Get width
		fileStream >> part; // width=47
		int charWidth = GetAttributeIntValue(part);
		currentChar->PixelSize.Width = (float)charWidth;
		currentChar->TextureSize.Width = (float)charWidth / textureWidth;

		// get height
		fileStream >> part; //Height=57
		int charHeight = GetAttributeIntValue(part);
		currentChar->PixelSize.Height = (float)charHeight;
		currentChar->TextureSize.Height = (float)charHeight / textureHeight;

		// get xoffset
		fileStream >> part; //Xoffset=-6
		currentChar->PixelOffset.X = (float)GetAttributeIntValue(part);

		// get yoffset
		fileStream >> part; //Yoffset=16
		currentChar->PixelOffset.Y = (float)GetAttributeIntValue(part);

		// get xadvance
		fileStream >> part; //Xadvance=65
		currentChar->PixelAdvanceX = (float)GetAttributeIntValue(part);
		currentChar->numKernings = 0;

		//Get page
		//Get channel
		fileStream >> part >> part; // page=0    chnl=0
	}

	// get number of kernings
	fileStream >> part >> part; // kernings count=96
	int allKerningsCount = GetAttributeIntValue(part);

	// initialize the kernings list
	FontKerning*  allKerningList = new FontKerning[allKerningsCount];
	for (int kerningIndex = 0; kerningIndex < allKerningsCount; kerningIndex++)
	{
		FontKerning *kerning = &allKerningList[kerningIndex];
		// get first character
		fileStream >> part >> part; // kerning first=87
		kerning->FromCharId = GetAttributeIntValue(part);

		// get second character
		fileStream >> part; // second=45
		kerning->ToCharId = GetAttributeIntValue(part);

		// get amount
		fileStream >> part; // amount=-1
		float t = (float)GetAttributeIntValue(part);
		kerning->PixelAmount = t;

		FontChar *c = font->GetChar(kerning->FromCharId);
		if (c != nullptr)
		{
			c->numKernings++;
		}
	}

	for (int charIndex = 0; charIndex < font->numCharacters; charIndex++)
	{
		FontChar *c = &font->CharPointerList[charIndex];
		c->KerningsList = new FontKerning[c->numKernings];
		int charKerningIndex = 0;
		for (int kerningIndex = 0; kerningIndex < allKerningsCount; kerningIndex++)
		{
			FontKerning *kerning = &allKerningList[kerningIndex];
			if (kerning->FromCharId == c->id)
			{
				c->KerningsList[charKerningIndex++] = *kerning;
			}
		}
	}

	delete allKerningList;

	wchar_t *ws1 = L"Assets/";
	std::wstring fontImagePath(ws1);
	fontImagePath += font->FontImage;
	ResourceDescription imageDescription(ResourceType::RT_Texture, fontImagePath);
	ResourceManager::LoadTextureFromFile(imageDescription);
	font->TexturePointer = Texture::Get(imageDescription.GetName());

	Font::Add(font->Name, font);
}

void ResourceManager::LoadTextureFromFile(ResourceDescription &description)
{
	if (Texture::DescriptorHeapEndPointer >= Texture::MaxTextureDescriptors)
	{
		throw new std::exception("Max textures limit reached");
	}

	std::wstring imageFileName = description.GetPath();
	std::shared_ptr<Texture> texture = Texture::LoadFromFile(imageFileName.c_str());
	Texture::Add(description.GetName(), texture);
}

std::wstring ResourceManager::GetAttributeStringValue(std::wstring &attribute) 
{
	std::wstring attributeValue = GetAttributeValue(attribute);
	return attributeValue.substr(1, attributeValue.size() - 2);
};

int ResourceManager::GetAttributeIntValue(std::wstring &attribute) {
	std::wstring attributeValue = GetAttributeValue(attribute);
	return std::stoi(attributeValue);
};

std::wstring ResourceManager::GetAttributeValue(std::wstring &attribute)
{
	size_t valueStartPos = attribute.find(L"=") + 1;
	return attribute.substr(valueStartPos, attribute.size() - valueStartPos);
};

std::wstring ResourceManager::GetAttributeValueAtIndex(std::wstring attributeValue, UINT index)
{
	std::wstring indexedValue = L"";
	int currentValueIndex = -1;
	while (attributeValue.length() > 0)
	{
		currentValueIndex++;
		size_t indexedValuePosition = attributeValue.find(L",");
		if (indexedValuePosition == -1)
		{
			if (currentValueIndex == index)
			{
				indexedValue = attributeValue;
			}
			break;
		}
		if (currentValueIndex == index)
		{
			indexedValue = attributeValue.substr(0, indexedValuePosition);
			break;
		}
		size_t cutFromIndex = indexedValuePosition + 1;
		size_t cutLength = attributeValue.length() - cutFromIndex;
		attributeValue = attributeValue.substr(cutFromIndex, cutLength);
	}
	return indexedValue; 
};

void ResourceManager::LoadMapFromFile(ResourceDescription &description)
{
	std::string fileName = StringHelper::ToStdString(description.GetPath());
	TiXmlDocument mapXml(fileName);
	if (mapXml.LoadFile())
	{
		std::shared_ptr < TiledMap> map = std::make_shared<TiledMap>(mapXml);
		std::vector<std::wstring> imagesToLoad = map->GetImagesToLoad();
		for (int imageIndex = 0; imageIndex < imagesToLoad.size(); imageIndex++)
		{
			std::wstring path = imagesToLoad[imageIndex];
			ResourceDescription description(ResourceType::RT_Texture, path);
			LoadTextureFromFile(description);
		}
		TiledMap::Add(description.GetName(), map);
	}
	else
	{
		throw std::exception("Can't read map from file");
	}
}

void ResourceManager::LoadAnimationsFromSpriteSheetFile(ResourceDescription &description)
{
	std::string fileName = StringHelper::ToStdString(description.GetPath());
	TiXmlDocument xml(fileName);
	if (xml.LoadFile())
	{		
		std::wstring textureFileName = SpriteSheetHelper::GetImageFilePathFromXml(xml);
		size_t pathEndIndex = fileName.find_last_of("/") + 1;
		std::string directoryPath = fileName.substr(0, pathEndIndex);
		std::wstring textureFilePath = std::wstring(directoryPath.begin(), directoryPath.end()).append(textureFileName);

		ResourceDescription description(ResourceType::RT_Texture, textureFilePath);
		LoadTextureFromFile(description);

		SpriteSheetHelper::LoadAnimationsFromXml(xml, Texture::Get(description.GetName()));
	}
	else
	{
		throw std::exception("Can't read sprite sheet from file");
	}
}