﻿#include "pch.h"
#include "TiledObject.h"

using namespace Tringine::Util;

namespace Tringine
{
	TiledObject::TiledObject(TiXmlElement* element):TiledEntry(element)
	{
		_area.Position.X = XmlHelper::GetIntAttributeValue(element, "x", _area.Position.X);
		_area.Position.Y = XmlHelper::GetIntAttributeValue(element, "y", _area.Position.Y);
		_area.Size.Width = XmlHelper::GetIntAttributeValue(element, "width", _area.Size.Width);
		_area.Size.Height = XmlHelper::GetIntAttributeValue(element, "height", _area.Size.Height);
		_type = XmlHelper::GetWStringAttributeValue(element, "type", L"unknown");

		TiXmlHandle handle(element);
		TiXmlElement* polylineElement = handle.FirstChild("polyline").Element();
		if (polylineElement != nullptr)
		{
			_polyline = std::make_shared<Polyline>(polylineElement, _area.Position);
		}
	}

	IntegerRectangle TiledObject::GetArea()
	{
		return _area;
	}

	std::wstring TiledObject::GetTypeName()
	{
		return _type;
	}

	std::shared_ptr < Polyline> TiledObject::GetPolyline()
	{
		return _polyline;
	}
}