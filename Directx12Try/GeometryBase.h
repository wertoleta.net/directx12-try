﻿#pragma once

#include "Common\DeviceResources.h"
#include "Structures.h"
#include "Common\StepTimer.h"
#include "DynamicResource.h"
#include "ShaderStructures.h"

using namespace DirectX;
using namespace TringineDx;

namespace Tringine
{
	namespace Geometry
	{
		/*
			 ласс предоставл¤ет базовый функционал дл¤ работы с геометрией
		*/
		class GeometryBase : public DynamicResource
		{
		public:
			//Ѕазовый конструктор
			GeometryBase();

			//Ѕазовый деструктор
			~GeometryBase();

			//¬ид на ресурс буфера вершин
			D3D12_VERTEX_BUFFER_VIEW VertexBufferView;

			//¬ид на ресурс буфера индексов
			D3D12_INDEX_BUFFER_VIEW IndexBufferView;

			/*
				ќтрисовка геометрии
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList - список команд в который добавл¤ютс¤ команды на отрисовку геометрии
			*/
			virtual void Draw(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandList);

			//”далене буферов выгрузки геометрии
			void Cleanup();

		protected:
			//–есурс буфера вершин
			Microsoft::WRL::ComPtr<ID3D12Resource> _vertexBuffer;

			//–есурс буфера индексов
			Microsoft::WRL::ComPtr<ID3D12Resource> _indexBuffer;

			//–есурс выгрузки буфера вершин
			Microsoft::WRL::ComPtr<ID3D12Resource> _vertexUploadBuffer;

			//–есурс выгрузки буфера индексов
			Microsoft::WRL::ComPtr<ID3D12Resource> _indexUploadBuffer;

			// оличество индексов на один элемент геометрии
			UINT _indexCountPerInstance;
			// оличество элементов геометрии
			UINT _instancesCount;
			//ѕоложение первого индекса в буфере индексов
			UINT _startIndexLocation;
			//«начение добавл¤емое к каждому индексу перед чтением вершины из буфера вершин
			UINT _baseVertexLocation;
			//«начение добавл¤емое к каждому индексу перед чтением данных элементов из буфера вершин
			UINT _startInstanceLocation;

			//“опологи¤ отрисовки примитива
			D3D_PRIMITIVE_TOPOLOGY _topology;

			//»нициалиаци¤ базовых значений дл¤ отрисовки по еденице (per instance draw) 
			virtual void InitInstanceValues() = 0;

			/*
				—оздание буфера вершин
				ID3D12Device &device - устройство дл¤ которого создаЄтс¤ буфер вершин
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавл¤ютс¤ команды на создание буфера вершин
				T vertices[] - массив вершин из которых создаЄтс¤ буффер
				UINT verticesCount - количество вершин в буфере
			*/
			void CreateVertexBuffer(
				ID3D12Device& device,
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList,
				BYTE* verticesPointer,
				UINT verticeSize,
				UINT verticesCount);

			/*
				—оздание буфера индексов
				ID3D12Device &device - устройство дл¤ которого создаЄтс¤ буфер индексов
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавл¤ютс¤ команды на создание буфера индексов
				unsigned short indexes[] - массив индексов дл¤ которого создаЄтс¤ буфер
				UINT indexesCount - количество индексов в буфере
			*/
			void CreateIndexBuffer(
				ID3D12Device& device,
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList,
				unsigned short indexes[],
				UINT indexesCount);

		private:
		};
	}
}