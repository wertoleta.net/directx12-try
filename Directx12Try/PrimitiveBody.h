﻿#pragma once

#include "Types.h"
#include "GeometryBase.h"

using namespace Tringine::Geometry;

namespace Tringine
{
	namespace Physics
	{
		enum MovableType
		{
			Static,
			Dynamic,
			Kinetic
		};

		struct Position
		{
		private:
			FloatPoint _current;
			FloatPoint _previous;

		public:
			Position(float x, float y) :
				_current(x, y),
				_previous(x, y) {}
			Position(FloatPoint spawn) :
				_current(spawn),
				_previous(spawn) {}

			void MoveOn(float additionalX, float additionalY)
			{
				MoveTo(_current.X + additionalX, _current.Y + additionalY);
			}

			void MoveTo(float newX, float newY)
			{
				_previous = _current;
				_current = FloatPoint(newX, newY);
			}

			void MoveTo(FloatPoint newPosition)
			{
				this->MoveTo(newPosition.X, newPosition.Y);
			}

			FloatPoint GetPrevious()
			{
				return _previous;
			}

			FloatPoint GetCurrent()
			{
				return _current;
			}
		};

		class PrimitiveBody
		{
		private:
			MovableType _type;
			float _weight;
			FloatPoint _velocity;
			FloatPoint _maxVelocity;

		protected:
			Position _position;

		public:
			PrimitiveBody();
			PrimitiveBody(MovableType type);
			PrimitiveBody(MovableType type, FloatPoint spawnPoint);

			MovableType GetType();

			virtual void MoveTo(FloatPoint newPosition);
			virtual void MoveOn(FloatPoint newPosition);
			virtual void MoveOn(float x, float y);

			float GetWeight();

			FloatPoint GetCurrentPosition();
			FloatPoint GetPreviousPosition();

			FloatPoint GetVelocity();
			void SetVelocity(float x, float y);
			void AddVelocity(float x, float y);

			FloatPoint GetMaxVelocity();
			void SetMaxVelocity(float x, float y);
			void ApplyVelocity(float timeScale);

			virtual bool IsCollide(std::shared_ptr < PrimitiveBody> anotherBody);
		};
	}
}

