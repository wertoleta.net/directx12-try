﻿#include "pch.h"
#include "ResourceDescription.h"

namespace Tringine
{
	ResourceDescription::ResourceDescription(ResourceType type, std::wstring path):ResourceDescription(type, ResourceScope::RS_Stage, path)
	{		
	}

	ResourceDescription::ResourceDescription(ResourceType type, ResourceScope scope, std::wstring path):
		_type(type),
		_scope(scope),
		_path(path)
	{
	}

	ResourceType ResourceDescription::GetType()
	{
		return _type;
	}

	std::wstring ResourceDescription::GetPath()
	{
		return _path;
	}

	std::wstring ResourceDescription::GetName()
	{
		std::wstring name;
		name.resize(_path.length());
		errno_t error = _wsplitpath_s(_path.c_str(), NULL, 0, NULL, 0, &name[0], name.size(), NULL, 0);
		if (error == EINVAL)
		{
			throw new std::exception("ERROR: Wrong path. Cant retrive resource name.");
		}
		return StringHelper::ToUpperFirstLetterOfEveryWord(StringHelper::ResizeToRealLength(name));
	}

	ResourceScope ResourceDescription::GetScope()
	{
		return _scope;
	}
}