﻿#pragma once

#include "Common\DeviceResources.h"
#include "ShaderStructures.h"

using namespace TringineDx;

namespace TringineDx
{
	/*
		Класс предоставляющий инструменты для работы с ConstantBuffer'ом для DX12.
		Инициализирует буффер коснтант и предоставляет менеджмент индексов расположения данных (объктов) в этом буфере.
		Каждый объект в игре хранит свои данные mvp матрицы в буфере констант.
	*/
	class ConstantBuffer
	{
	public:
		//Выровненный размер одной части буфера констант (Для одного объекта)
		static const UINT AlignedSize = (sizeof(ModelViewProjectionConstantBuffer) + 255) & ~255;

		//Ссылка на основной буфер констант используемый другими классами по умолчанию
		static ConstantBuffer *MainInstancePointer;
		
		/*
			Очищает используемую память при удалений объекта
		*/
		~ConstantBuffer();

		/*
			Создаёт буфер констант в виде ресурса GPU
			ID3D12Device *device - ссылка на устройство DX12 для которого создаются ресурсы
		*/
		void Init(ID3D12Device &device);

		/*
			Записать MVP матрицу в GPU буффер констант 
			int shift - сдвиг относительно начала буфера констант в GPU памяти
			ModelViewProjectionConstantBuffer &mvpBuffer - буфер констант сущности для записи в GPU
		*/
		void StoreConstants(int shift, ModelViewProjectionConstantBuffer &mvpBuffer);

		//Получить следующий не занятый индекс в буфере констант
		UINT GetNextConstantIndex();

		//Сброс счётчика индексов буфера констант
		void ResetConstantIndexes();

		//Установить текущий кадр для тройной буферизации
		void SetCurrentFrameIndex(int frameIndex);

		//Получить адрес в GPU для сущности под индексом index в текущем кадре
		D3D12_GPU_VIRTUAL_ADDRESS GetIndexedGpuVirtualAddress(int index);

	private:
		//Общий рамер буфера констант (AlignedSize * максимальное количество сущностей)
		static const UINT TotalSize = AlignedSize * 256;

		//Следующий индекс сущности
		UINT _nextConstantIndex;

		//Адрес в GPU текущего кадра в тройном буфере
		D3D12_GPU_VIRTUAL_ADDRESS _currentFrameGpuVirtualAddress;

		//Буфера констант для каждого кадра 
		Microsoft::WRL::ComPtr<ID3D12Resource> _buffer[DX::c_frameCount];

		//Сопоставленные адреса буферов констант в GPU для каждого кадра
		UINT8 *_mappedBuffer[DX::c_frameCount];

		//Сопоставленный адрес в GPU текущего кадра в тройном буфере
		UINT8 *_currentFrameMapped;
	};
}