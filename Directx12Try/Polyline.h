﻿#pragma once
#include "Types.h"
#include "tinyxml.h"
#include "XmlHelper.h"
#include "StringHelper.h"

namespace Tringine
{
	namespace Util
	{
		class Polyline
		{
		private:
			std::vector<FloatPoint> _points;
			float _totalLength = 0.0;

		public:
			Polyline(TiXmlElement* element);
			Polyline(TiXmlElement* element, FloatPoint origin);

			int GetPointsCount();
			int GetSegmentsCount();
			FloatPoint GetPointAt(int index);
			FloatSegment GetSegmentAt(int index);
			float GetLength();
		};
	}
}

