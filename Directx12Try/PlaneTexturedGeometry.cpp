﻿#include "pch.h"
#include "PlaneTexturedGeometry.h"


namespace Tringine
{
	namespace Geometry
	{
		unsigned short PlaneTexturedGeometry::Indices[] =
		{
			0, 1, 2,
			3, 0, 2
		};

		PlaneTexturedGeometry::PlaneTexturedGeometry() :
			_size(1.0f, 1.0f),
			_origin(0.5f, 0.5f),
			_textureSides(0.0f, 0.0f, 1.0f, 1.0f)
		{
		}

		PlaneTexturedGeometry::PlaneTexturedGeometry(XMFLOAT2 size, XMFLOAT2 origin) :
			_size(size),
			_origin(origin),
			_textureSides(0.0f, 0.0f, 1.0f, 1.0f)
		{
		}

		PlaneTexturedGeometry::PlaneTexturedGeometry(XMFLOAT2 size, XMFLOAT2 origin, XMFLOAT4 textureSides) :
			_size(size),
			_origin(origin),
			_textureSides(textureSides)
		{
		}


		PlaneTexturedGeometry::~PlaneTexturedGeometry()
		{
		}

		void PlaneTexturedGeometry::InitInstanceValues()
		{
			_indexCountPerInstance = 6;
			_instancesCount = 1;
			_startIndexLocation = 0;
			_baseVertexLocation = 0;
			_startInstanceLocation = 0;
		}

		void PlaneTexturedGeometry::Initialize(
			ID3D12Device& device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList)
		{
			InitInstanceValues();
			float left = -_origin.x;
			float top = -_origin.y;
			float right = -_origin.x + _size.x;
			float bottom = -_origin.y + _size.y;
			TexturedVertex planeVertices[] =
			{
				{ XMFLOAT3(left, top, 0.0f),	XMFLOAT2(_textureSides.x, _textureSides.y) },
				{ XMFLOAT3(right, top, 0.0f), XMFLOAT2(_textureSides.z, _textureSides.y) },
				{ XMFLOAT3(right, bottom, 0.0f), XMFLOAT2(_textureSides.z, _textureSides.w) },
				{ XMFLOAT3(left, bottom, 0.0f), XMFLOAT2(_textureSides.x, _textureSides.w) },
			};
			CreateVertexBuffer(device, commandsList, reinterpret_cast<BYTE*>(planeVertices), sizeof(TexturedVertex), sizeof(planeVertices) / sizeof(TexturedVertex));
			CreateIndexBuffer(device, commandsList, Indices, sizeof(Indices) / sizeof(unsigned short));
		}
	}
}