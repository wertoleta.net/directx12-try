﻿#pragma once

#include "Structures.h"
#include <ppltasks.h>

using namespace DirectX;

namespace TringineDx
{
	/*
		Класс для загрузки шейдеров и их байтового представления.
	*/
	class ShaderLoadingInfo
	{
	public:
		/*
			Базовый конструктор
			std::wstring &path - путь к файлу который содержит шейдер
			std::vector<byte> *dataPointer - указатель на векторый в который запишутся байтовые данные шейдера
			Concurrency::task<void> *taskPointer - указатель на таск в который запишется задание чтения шейдера из файла
		*/
		ShaderLoadingInfo(std::wstring path, std::vector<byte> *dataPointer, Concurrency::task<void> *taskPointer);

		/*
			Записывает задание в таск на который указывает _taskPointer
			Concurrency::task<void> &task - задание которое следует записать
		*/
		void SetTask(Concurrency::task<void> &task);

		//Геттер для _path - получение знчения пути к файлу из которого следует читать данные
		std::wstring& GetPath();
		
		//Указатель на байтовое представление загруженного шейдера
		std::vector<byte>* DataPointer;

		/*
			Загрузка списка шейдеров из файла
			std::vector<TringineDx::ShaderLoadingInfo> &shadersInfo - список шейдеров для загрузки. 
			У них должны быть указаны пути откуда читать файлы и ссылки на таски в которые запишутся задания чтения.
		*/
		static void LoadShadersAsynch(std::vector<TringineDx::ShaderLoadingInfo> &shadersInfo);
	private:
		//Путь к файлу содержащему шейдер
		std::wstring _path;

		//Указатель на таск в который нужно записать задание чтения
		Concurrency::task< void >* _taskPointer;
	};
}
