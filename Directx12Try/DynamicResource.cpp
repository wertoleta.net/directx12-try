﻿#include "pch.h"
#include "DynamicResource.h"
#include <stdlib.h>

namespace TringineDx
{
	DynamicResourceList	DynamicResource::CreateQueue;
	DynamicResource::CleanupQueueToFence	DynamicResource::CleanupQueue;
	DynamicResourceList DynamicResource::AllResources;

	DynamicResource::DynamicResource()
	{
		_lastDrawFence = nullptr;
	}


	DynamicResource::~DynamicResource()
	{
	}

	void DynamicResource::AddCreatedResource(std::shared_ptr<DynamicResource> resource)
	{
		DynamicResource::AllResources.push_back(resource);
		DynamicResource::CreateQueue.push_back(resource);
	}

	void DynamicResource::Initialize(
		ID3D12Device &device,
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList) 
	{
	}

	void DynamicResource::Cleanup() {}

	Microsoft::WRL::ComPtr<ID3D12Fence> DynamicResource::InitializeAllNew(
		ID3D12Device &device,
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList)
	{
		if (DynamicResource::CreateQueue.size() == 0)
		{
			return nullptr;
		}

		DynamicResourceList *createdResources = new DynamicResourceList();

		while (DynamicResource::CreateQueue.size() > 0)
		{
			std::shared_ptr<DynamicResource> resource = DynamicResource::CreateQueue.back();
			resource->Initialize(device, commandsList);
			DynamicResource::CreateQueue.pop_back();
			createdResources->push_back(resource);
		}
		Microsoft::WRL::ComPtr<ID3D12Fence> fence;
		ThrowIfFailed(device.CreateFence(0, D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));

		CleanupQueue.insert({ fence, createdResources });

		return fence;
	}

	void DynamicResource::Use()
	{
		_isUsed = true;
	}

	void DynamicResource::ResetUse()
	{
		_isUsed = false;
	}

	void DynamicResource::SetLastDrawFence(Microsoft::WRL::ComPtr < ID3D12Fence> fence)
	{
		_lastDrawFence = fence;
	}

	bool DynamicResource::IsUsed()
	{
		return _isUsed;
	}

	bool DynamicResource::IsCanBeDeleted()
	{
		return !_isUsed && (_lastDrawFence == nullptr || _lastDrawFence->GetCompletedValue() > 0);
	}

	void DynamicResource::GarbageCollect(Microsoft::WRL::ComPtr < ID3D12Fence> fence)
	{
		std::vector<int> resourceIndexesToRemove;
		for (size_t resourceIndex = 0; resourceIndex < AllResources.size(); resourceIndex++)
		{
			std::shared_ptr<DynamicResource> resource = AllResources[resourceIndex];
			if (resource->IsUsed())
			{
				resource->SetLastDrawFence(fence);
			}
			else if (resource.use_count() == 2 && resource->IsCanBeDeleted())
			{
				resourceIndexesToRemove.push_back(resourceIndex);
			}
			resource->ResetUse();
		}
		for (int i = resourceIndexesToRemove.size() - 1; i >= 0 ; i--)
		{
			AllResources.erase(AllResources.begin() + resourceIndexesToRemove[i]);
		}
	}

	void DynamicResource::CleanupAllInitialized()
	{
		for (auto iterator = CleanupQueue.cbegin(); iterator != CleanupQueue.cend(); ) 
		{
			Microsoft::WRL::ComPtr<ID3D12Fence> fence = iterator->first;
			DynamicResourceList* resources = iterator->second;
			if (fence->GetCompletedValue() > 0)
			{
				for (int i = 0; i < resources->size(); i++)
				{
					(*resources)[i]->Cleanup();
				}
				delete resources;

				iterator = CleanupQueue.erase(iterator);
			}
			else
			{
				iterator = std::next(iterator);
			}
		}
	}
}
