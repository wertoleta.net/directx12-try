﻿#include "pch.h"
#include "RectangleSpace.h"

const float GravityPower = 0.5f;

namespace Tringine
{
	namespace Physics
	{
		RectangleSpace::RectangleSpace()
		{
		}

		RectangleSpace::~RectangleSpace()
		{
		}

		void RectangleSpace::Update(float timeScale)
		{
			Group::Update(timeScale);
			ResetBodiesStatuses(timeScale);
			for (UINT firstBodyIndex = 0; firstBodyIndex < _bodies.size(); firstBodyIndex++)
			{
				std::shared_ptr < RectangleBody> firstBody = _bodies[firstBodyIndex];
				if (firstBody->GetType() == MovableType::Dynamic)
				{
					for (UINT secondBodyIndex = 0; secondBodyIndex < _bodies.size(); secondBodyIndex++)
					{
						std::shared_ptr < RectangleBody> secondBody = _bodies[secondBodyIndex];
						bool alreadyCalculated = secondBodyIndex < firstBodyIndex && secondBody->GetType() == MovableType::Dynamic;
						bool sameBody = firstBodyIndex == secondBodyIndex;
						if (sameBody || alreadyCalculated)
						{
							continue;
						}
						if (firstBody->IsOverlaps(secondBody))
						{
							if (firstBody->IsSideSolid(Side::All) && secondBody->IsSideSolid(Side::All))
							{
								CalculateFullSolidCollisions(firstBody, secondBody);
							}
							else
							{
								CalculatePartitialSolidCollisions(firstBody, secondBody);
							}
						}
						else if (firstBody->IsTouches(secondBody))
						{
							CalculateTouches(firstBody, secondBody);						
						}
					}
					firstBody->UpdateDebugEntity();
				}
				firstBody->StorePreviousCorners();
			}
			ApplyGravity(timeScale);
		}

		BodyList RectangleSpace::GetAllOverlaped(std::shared_ptr<RectangleBody> body)
		{
			BodyList list;
			for (UINT secondBodyIndex = 0; secondBodyIndex < _bodies.size(); secondBodyIndex++)
			{
				std::shared_ptr < RectangleBody> secondBody = _bodies[secondBodyIndex];
				bool sameBody = body == secondBody;
				if (sameBody)
				{
					continue;
				}
				if (body->IsOverlaps(secondBody))
				{
					list.push_back(secondBody);
				}
			}
			return list;
		}

		void RectangleSpace::ResetBodiesStatuses(float timeScale)
		{
			for (UINT bodyIndex = 0; bodyIndex < _bodies.size(); bodyIndex++)
			{
				std::shared_ptr < RectangleBody> body = _bodies[bodyIndex];
				body->ResetInteractions();
				if (body->GetType() == MovableType::Dynamic)
				{
					body->ApplyVelocity(1.0f);
				}
			}
		}

		void RectangleSpace::ApplyGravity(float timeScale)
		{
			for (UINT bodyIndex = 0; bodyIndex < _bodies.size(); bodyIndex++)
			{
				std::shared_ptr < RectangleBody> body = _bodies[bodyIndex];
				if (body->GetType() == MovableType::Dynamic)
				{
					body->AddVelocity(0.0f, GravityPower);
				}
			}
		}

		void RectangleSpace::CalculateFullSolidCollisions(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody)
		{
			float massCoefficient1 = 0.5f;
			float massCoefficient2 = 0.5f;
			if (secondBody->GetType() == MovableType::Dynamic)
			{
				float totalWeight = fmax(firstBody->GetWeight() + secondBody->GetWeight(), 1.0f);
				massCoefficient1 = firstBody->GetWeight() / totalWeight;
			}
			else
			{
				massCoefficient1 = 1.0f;
			}
			massCoefficient2 = 1.0f - massCoefficient1;
			FloatSize overlapSize = firstBody->GetOverlapSize(secondBody);
			FloatPoint velocity = firstBody->GetVelocity();
			if (overlapSize.Width <= overlapSize.Height)
			{
				if (firstBody->GetPreviousPosition().X < secondBody->GetPreviousPosition().X)
				{
					massCoefficient1 = -massCoefficient1;
					firstBody->AddCollidedSide(Side::Right);
					secondBody->AddCollidedSide(Side::Left);
				}
				else
				{
					massCoefficient2 = -massCoefficient2;
					firstBody->AddCollidedSide(Side::Left);
					secondBody->AddCollidedSide(Side::Right);
				}
				firstBody->MoveOn(overlapSize.Width * massCoefficient1, 0.0f);
				secondBody->MoveOn(overlapSize.Width * massCoefficient2, 0.0f);

				if ((velocity.X > 0.0f && massCoefficient1 < 0.0f) ||
					(velocity.X < 0.0f && massCoefficient1 > 0.0f))
				{
					firstBody->SetVelocity(0.0f, velocity.Y);
				}
			}
			else
			{
				if (firstBody->GetPreviousPosition().Y < secondBody->GetPreviousPosition().Y)
				{
					massCoefficient1 = -massCoefficient1;
					firstBody->AddCollidedSide(Side::Bottom);
					secondBody->AddCollidedSide(Side::Top);
				}
				else
				{
					massCoefficient2 = -massCoefficient2;
					firstBody->AddCollidedSide(Side::Top);
					secondBody->AddCollidedSide(Side::Bottom);
				}
				firstBody->MoveOn(0.0f, overlapSize.Height * massCoefficient1);
				secondBody->MoveOn(0.0f, overlapSize.Height * massCoefficient2);

				if ((velocity.Y > 0.0f && massCoefficient1 < 0.0f) ||
					(velocity.Y < 0.0f && massCoefficient1 > 0.0f))
				{
					firstBody->SetVelocity(velocity.X, 0.0f);
				}
			}
		}

		void RectangleSpace::CalculatePartitialSolidCollisions(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody)
		{
			float massCoefficient1 = GetMassCoefficient(firstBody, secondBody);
			float massCoefficient2 = 1.0f - massCoefficient1;
			FloatSize overlapSize = firstBody->GetOverlapSize(secondBody);
			FloatPoint velocity = firstBody->GetVelocity();
			Side collidedSides = Side::None;

			if (firstBody->IsSideSolid(Side::Bottom) && secondBody->IsSideSolid(Side::Top) && firstBody->GetPreviousCorners().Bottom < secondBody->GetPreviousCorners().Top)
			{
				collidedSides = collidedSides | Side::Bottom;
			}
			if (firstBody->IsSideSolid(Side::Top) && secondBody->IsSideSolid(Side::Bottom) && firstBody->GetPreviousCorners().Top > secondBody->GetPreviousCorners().Bottom)
			{
				collidedSides = collidedSides | Side::Top;
			}
			if (firstBody->IsSideSolid(Side::Right) && secondBody->IsSideSolid(Side::Left) && firstBody->GetPreviousCorners().Right < secondBody->GetPreviousCorners().Left)
			{
				collidedSides = collidedSides | Side::Right;
			}
			if (firstBody->IsSideSolid(Side::Left) && secondBody->IsSideSolid(Side::Right) && firstBody->GetPreviousCorners().Left > secondBody->GetPreviousCorners().Right)
			{
				collidedSides = collidedSides | Side::Left;
			}
			if ((collidedSides & Side::Horizontal) != Side::None && (collidedSides & Side::Vertical) != Side::None)
			{
				if (overlapSize.Width <= overlapSize.Height)
				{
					collidedSides = collidedSides & Side::Horizontal;
				}
				else
				{
					collidedSides = collidedSides & Side::Vertical;
				}
			}
			switch (collidedSides)
			{
			case Side::Bottom:
				firstBody->MoveOn(0.0f, -overlapSize.Height * massCoefficient1);
				secondBody->MoveOn(0.0f, overlapSize.Height * massCoefficient2);
				firstBody->AddCollidedSide(Side::Bottom);
				secondBody->AddCollidedSide(Side::Top);
				if (velocity.Y > 0.0f)
				{
					firstBody->SetVelocity(velocity.X, 0.0f);
				}
				break;
			case Side::Top:
				firstBody->MoveOn(0.0f, overlapSize.Height * massCoefficient1);
				secondBody->MoveOn(0.0f, -overlapSize.Height * massCoefficient2);
				firstBody->AddCollidedSide(Side::Top);
				secondBody->AddCollidedSide(Side::Bottom);
				if (velocity.Y < 0.0f)
				{
					firstBody->SetVelocity(velocity.X, 0.0f);
				}
				break;
			case Side::Right:
				firstBody->MoveOn(-overlapSize.Width * massCoefficient1, 0.0f);
				secondBody->MoveOn(overlapSize.Width * massCoefficient2, 0.0f);
				firstBody->AddCollidedSide(Side::Right);
				secondBody->AddCollidedSide(Side::Left);
				if (velocity.X > 0.0f)
				{
					firstBody->SetVelocity(0.0f, velocity.Y);
				}
				break;
			case Side::Left:
				firstBody->MoveOn(overlapSize.Width * massCoefficient1, 0.0f);
				secondBody->MoveOn(-overlapSize.Width * massCoefficient2, 0.0f);
				firstBody->AddCollidedSide(Side::Left);
				secondBody->AddCollidedSide(Side::Right);
				if (velocity.X < 0.0f)
				{
					firstBody->SetVelocity(0.0f, velocity.Y);
				}
				break;
			}
		}

		void RectangleSpace::CalculateTouches(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody)
		{
			Corners firstCorenrs = firstBody->GetCorners();
			Corners secondCorenrs = secondBody->GetCorners();
			if (firstCorenrs.Top > secondCorenrs.Bottom)
			{
				firstBody->AddTouchedBody(Side::Top, secondBody);
				secondBody->AddTouchedBody(Side::Bottom, firstBody);
			}
			else if (firstCorenrs.Bottom < secondCorenrs.Top)
			{
				firstBody->AddTouchedBody(Side::Bottom, secondBody);
				secondBody->AddTouchedBody(Side::Top, firstBody);
			}
			if (firstCorenrs.Left > secondCorenrs.Right)
			{
				firstBody->AddTouchedBody(Side::Left, secondBody);
				secondBody->AddTouchedBody(Side::Right, firstBody);
			}
			else if (firstCorenrs.Right < secondCorenrs.Left)
			{
				firstBody->AddTouchedBody(Side::Right, secondBody);
				secondBody->AddTouchedBody(Side::Left, firstBody);
			}
		}

		float RectangleSpace::GetMassCoefficient(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody)
		{
			if (secondBody->GetType() == MovableType::Dynamic)
			{
				float totalWeight = fmax(firstBody->GetWeight() + secondBody->GetWeight(), 1.0f);
				return firstBody->GetWeight() / totalWeight;
			}
			else
			{
				return 1.0f;
			}
		}

		void RectangleSpace::AddBody(std::shared_ptr <RectangleBody> body)
		{
			std::shared_ptr <Entity> entity = body->CreateDebugEntity();

			Add(entity);

			_bodies.push_back(body);
		}
	}
}