﻿#include "pch.h"
#include "RectangleBody.h"

namespace Tringine
{
	namespace Physics
	{
		RectangleBody::RectangleBody(FloatRectangle rect) : RectangleBody(rect, MovableType::Static)
		{
			UpdateCorners();
		}

		RectangleBody::RectangleBody(FloatRectangle rect, MovableType type) : PrimitiveBody(type, rect.Position),
			_size(rect.Size),
			_corners(0.0f, 0.0f, 0.0f, 0.0f),
			_previousCorners(0.0f, 0.0f, 0.0f, 0.0f),
			_solidSides(Side::All)
		{
			UpdateCorners();
			_touchedBodies[Side::Left] = new BodiesList();
			_touchedBodies[Side::Top] = new BodiesList();
			_touchedBodies[Side::Right] = new BodiesList();
			_touchedBodies[Side::Bottom] = new BodiesList();
		}

		bool RectangleBody::IsOverlaps(std::shared_ptr < RectangleBody> anotherRectangle)
		{
			Corners anotherCorners = anotherRectangle->GetCorners();
			Corners corners = this->GetCorners();
			const float eps = 0.999f;
			return	((anotherCorners.Right + eps) >= corners.Left) &&
				((anotherCorners.Left - eps) <= corners.Right) &&
				((anotherCorners.Bottom + eps) >= corners.Top) &&
				((anotherCorners.Top - eps) <= corners.Bottom);
		}

		bool RectangleBody::IsTouches(std::shared_ptr < RectangleBody> anotherRectangle)
		{
			Corners anotherCorners = anotherRectangle->GetCorners();
			Corners corners = this->GetCorners();
			const float eps = 1.999f;
			return	((anotherCorners.Right + eps) >= corners.Left) &&
				((anotherCorners.Left - eps) <= corners.Right) &&
				((anotherCorners.Bottom + eps) >= corners.Top) &&
				((anotherCorners.Top - eps) <= corners.Bottom);
		}

		FloatSize RectangleBody::GetOverlapSize(std::shared_ptr < RectangleBody> anotherRectangle)
		{
			Corners anotherCorners = anotherRectangle->GetCorners();
			Corners corners = this->GetCorners();

			float xOverlap;
			if (corners.Left > anotherCorners.Left)
			{
				xOverlap = anotherCorners.Right - corners.Left + 1;
			}
			else
			{
				xOverlap = corners.Right - anotherCorners.Left + 1;
			}

			float yOverlap;
			if (corners.Top > anotherCorners.Top)
			{
				yOverlap = anotherCorners.Bottom - corners.Top + 1;
			}
			else
			{
				yOverlap = corners.Bottom - anotherCorners.Top + 1;
			}

			return FloatSize(max(0, xOverlap), max(0, yOverlap));
		}
		

		FloatSize RectangleBody::GetSize()
		{
			return _size;
		}

		void RectangleBody::SetSize(FloatSize newSize)
		{
			_size = newSize;
			if (_debugEntity != nullptr)
			{
				_debugEntity->Resize(newSize);
			}
			UpdateCorners();
		}

		Corners RectangleBody::GetCorners()
		{
			return _corners;
		}

		Corners RectangleBody::GetPreviousCorners()
		{
			return _previousCorners;
		}

		void RectangleBody::UpdateCorners()
		{
			FloatPoint position = _position.GetCurrent();
			_corners.Left = position.X;
			_corners.Top = position.Y;
			_corners.Right = position.X + _size.Width;
			_corners.Bottom = position.Y + _size.Height;
		}

		void RectangleBody::MoveTo(FloatPoint newPosition)
		{
			PrimitiveBody::MoveTo(newPosition);
			UpdateCorners();
		}

		std::shared_ptr<Entity> RectangleBody::CreateDebugEntity()
		{
			_debugEntity = std::make_shared<Rectangle>(FloatRectangle(GetCurrentPosition(), GetSize()));
			return _debugEntity;
		}

		void RectangleBody::UpdateDebugEntity()
		{
			if (_debugEntity != nullptr)
			{
				FloatPoint position = GetCurrentPosition();
				_debugEntity->SetPosition(position.X, position.Y, 0.0f);
			}
		}
		bool RectangleBody::IsSideSolid(Side side)
		{
			return (side & _solidSides) == side;
		}

		void RectangleBody::SetSolidSides(int sidesCode)
		{
			SetSolidSides(static_cast<Side>(sidesCode));
		}
		void RectangleBody::SetSolidSides(Side sides)
		{
			_solidSides = sides;
		}

		void RectangleBody::StorePreviousCorners()
		{
			_previousCorners = _corners;
		}

		void RectangleBody::ResetInteractions()
		{
			_collidedSides = Side::None;
			_touchedSides = Side::None;
			for (SideToBodyMap::iterator sideIterator = _touchedBodies.begin(); sideIterator != _touchedBodies.end(); sideIterator++)
			{
				sideIterator->second->clear();
			}
		}

		bool RectangleBody::IsSideCollided(Side side)
		{
			return (_collidedSides & side) == side;
		}

		void RectangleBody::AddCollidedSide(Side side)
		{
			_collidedSides = _collidedSides | side;
			//AddTouchedSide(side);
		}

		bool RectangleBody::IsSideTouched(Side side)
		{
			return (_touchedSides & side) == side;
		}

		BodiesList RectangleBody::GetTouchedBodies(Side side)
		{
			BodiesList list;
			for (SideToBodyMap::iterator sideIterator = _touchedBodies.begin(); sideIterator != _touchedBodies.end(); sideIterator++)
			{
				if ((sideIterator->first & side) > 0)
				{
					BodiesList* sideList = sideIterator->second;
					for (int bodyIndex = 0; bodyIndex < sideList->size(); bodyIndex++)
					{
						list.push_back(sideList->at(bodyIndex));
					}
				}
			}
			return list;
		}

		void RectangleBody::AddTouchedBody(Side side, shared_ptr<RectangleBody> body)
		{
			_touchedSides = _touchedSides | side;
			for (SideToBodyMap::iterator sideIterator = _touchedBodies.begin(); sideIterator != _touchedBodies.end(); sideIterator++)
			{
				if ((sideIterator->first & side) > 0)
				{
					sideIterator->second->push_back(body);
				}
			}
		}
	}
}