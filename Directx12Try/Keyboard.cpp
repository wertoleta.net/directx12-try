﻿#include "pch.h"
#include "Keyboard.h"

namespace Tringine
{
	namespace Input
	{
		Keyboard::Keyboard()
		{
			for (int keyCode = 0; keyCode < KeyCount; keyCode++)
			{
				std::shared_ptr < Key> newKey = std::make_shared<Key>(keyCode);
				_keysByCode[keyCode] = newKey;
				Windows::System::VirtualKey virtualKey = static_cast<Windows::System::VirtualKey>(keyCode);
				std::wstring keyName = std::wstring(virtualKey.ToString()->Data());
				if (_keysByString.count(keyName) == 0)
				{
					_keysByString.insert({ keyName, newKey });
					OutputDebugString((std::to_wstring(keyCode) + L" " + keyName + L"\n").data());
				}
			}
		}

		Keyboard::~Keyboard()
		{

		}

		std::shared_ptr < Key> Keyboard::GetKeyByCode(int code)
		{
			if (code < 0 || code >= KeyCount)
			{
				code = 0;
			}
			return _keysByCode[code];
		}

		std::shared_ptr < Key> Keyboard::GetKeyByName(std::wstring name)
		{
			if (_keysByString.count(name) == 0)
			{
				return _keysByCode[0];
			}
			return _keysByString.at(name);
		}

		void Keyboard::Update()
		{
			for (int keyCode = 0; keyCode < KeyCount; keyCode++)
			{
				_keysByCode[keyCode]->Update();
			}
		}
	}
}