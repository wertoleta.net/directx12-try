﻿#pragma once
#include "Key.h"
#include <map>

namespace Tringine
{
	namespace Input
	{
		typedef std::map<std::wstring, std::shared_ptr < Key>> StringToKeyMap;

		static const int KeyCount = 255;

		class Keyboard
		{
		public:

			Keyboard();
			~Keyboard();
			std::shared_ptr < Key> GetKeyByCode(int code);
			std::shared_ptr < Key> GetKeyByName(std::wstring name);
			void Update();
		private:
			std::shared_ptr < Key> _keysByCode[KeyCount];
			StringToKeyMap _keysByString;
		};
	}
}