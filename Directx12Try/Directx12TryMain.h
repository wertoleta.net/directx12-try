﻿#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "TrinGame.h"
#include "MyStage.h"

// Отрисовывает содержимое Direct3D на экране.
namespace Directx12Try
{
	class Directx12TryMain
	{
	public:
		Directx12TryMain();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();
		void OnKeyDown(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args);
		void OnKeyUp(Windows::UI::Core::CoreWindow^ sender, Windows::UI::Core::KeyEventArgs^ args);

	private:
		std::unique_ptr<TrinGame> _trinGame;
		DX::StepTimer m_timer;
	};
}