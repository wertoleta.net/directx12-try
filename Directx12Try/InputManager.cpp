﻿#include "pch.h"
#include "InputManager.h"

namespace Tringine
{
	namespace Input
	{
		InputManager::InputManager():
			_keyboard(std::make_shared<Keyboard>())
		{

		}

		void InputManager::Update()
		{
			_keyboard->Update();
			for (auto actionsIterator = _actions.begin(); actionsIterator != _actions.end(); actionsIterator++)
			{
				actionsIterator->second->Update();
			}
		}

		std::shared_ptr < Keyboard> InputManager::GetKeyboard()
		{
			return _keyboard;
		}


		void InputManager::AddAction(std::wstring name, std::shared_ptr <Action> action)
		{
			if (_actions.count(name) > 0)
			{
				throw std::exception("Action with same name already exists");
			}
			_actions.insert({ name,  action }); 
		}

		std::shared_ptr < Action> InputManager::GetAction(std::wstring name)
		{
			StringToActionMap::iterator iterator = _actions.find(name);
			if (iterator == _actions.end())
			{
				return nullptr;
			}
			return iterator->second;		
		}
	}
}