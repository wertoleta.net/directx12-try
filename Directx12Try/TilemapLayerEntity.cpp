﻿#include "pch.h"
#include "TilemapLayerEntity.h"
#include "MapLayerGeometry.h"

namespace Tringine
{
	namespace Entities
	{
		TilemapLayerEntity::TilemapLayerEntity(std::shared_ptr < TiledMap> mapData, std::shared_ptr < TiledLayer> layer)
		{
			GeometryPointer = TringineDx::MakeDynamicResource<MapLayerGeometry>(mapData, layer);
			TexturePointer = Texture::Get(layer->GetTilesetTextureName());
			Revive();
		}


		TilemapLayerEntity::~TilemapLayerEntity()
		{
		}

		void TilemapLayerEntity::Update(float timeScale)
		{
			TexturedEntity::Update(timeScale);
		}

		void TilemapLayerEntity::PostUpdate(Drawer& drawer)
		{
			TexturedEntity::PostUpdate(drawer);
		}

		void TilemapLayerEntity::Draw(Drawer& drawer)
		{
			TexturedEntity::Draw(drawer);
		}
	}
}