﻿#pragma once
#include "Sprite.h"
#include "RectangleBody.h"
#include "InputManager.h"

using namespace Tringine::Entities;
using namespace Tringine::Physics;
using namespace Tringine::Input;

namespace Game
{
	class Hero :
		public Sprite
	{
	private:
		const int MaxJumpsCounter = 10;

		std::shared_ptr <RectangleBody> _body;
		int _jumpCounter;
		int _lastPressedDirection = 0;
	public:
		Hero(FloatPoint spawnPosition);
		~Hero();

		void Update(float timeScale) override;
		void Draw(Drawer  &drawer) override;
	};
}
