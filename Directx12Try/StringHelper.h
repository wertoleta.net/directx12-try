﻿#pragma once

#include <sstream>
#include <locale> 
#include <codecvt>

namespace Tringine
{
	namespace Util
	{
		class StringHelper
		{
		public:
			static std::vector<std::wstring> Split(std::wstring string, wchar_t delimeter);
			static std::wstring ToUpperFirstLetterOfEveryWord(std::wstring sentence);
			static std::string ToStdString(std::wstring string);
			static std::wstring ResizeToRealLength(std::wstring);
		};
	}
}

