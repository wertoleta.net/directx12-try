﻿#pragma once
#include "Texture.h"
#include "Common/DeviceResources.h"

using namespace Windows::Foundation;

/*
	Файл содержит описание всех структур которые используются для передачи в шейдеры
*/

namespace TringineDx
{
	//Смешение
	struct Offset 
	{
		float X;
		float Y;
	};

	//Целочисленная точка
	struct IntegerPoint
	{
		int X;
		int Y;

		IntegerPoint() {}
		IntegerPoint(int x, int y)
		{
			X = x;
			Y = y;
		}
	};

	//Точка
	struct FloatPoint
	{
		float X;
		float Y;

		FloatPoint() {}

		FloatPoint(float x, float y) :
			X(x),
			Y(y) {};

		FloatPoint(IntegerPoint p):FloatPoint((float) p.X, (float)p.Y) {}

		FloatPoint operator - (const FloatPoint& p)  const 
		{ 
			return FloatPoint(X - p.X, Y - p.Y); 
		}

		float DistanceTo(FloatPoint p)
		{
			return sqrtf(powf(p.X - X, 2.0f) + powf(p.Y - Y, 2.0f));
		}

		FloatPoint Normalized()
		{
			float length = DistanceTo(FloatPoint(0.0f, 0.0f));
			return FloatPoint(X / length, Y / length);
		}
	};

	//Целочисленный размер
	struct IntegerSize
	{
		int Width;
		int Height;

		IntegerSize() {}
		IntegerSize(int width, int height)
		{
			Width = width;
			Height = height;
		}
	};

	//Отступы
	struct Margin
	{
		float Top;
		float Right;
		float Bottom;
		float Left;
	};

	//Сдвиги между буквами
	struct FontKerning
	{
		//ID символа после которого применяется смещение
		int FromCharId;

		//ID символа перед которым применяется смещение
		int ToCharId; 

		//Величина смещения в значений части Viewport'a 
		float ViewportAmount; 

		//Величина смещения в пикселях
		float PixelAmount;
	};

	//Отдельный символ шрифта
	struct FontChar
	{
		//ID символа
		int id;

		//Расположение на текстуре
		FloatPoint TexturePosition;

		//Размер на текстуре
		Size TextureSize; 

		//Размер на вьюпорте
		Size ViewportSize;

		//Размер в пикселях
		Size PixelSize;

		//Отступ на вьюпорте
		Offset ViewportOffset; 

		//Отступ в пикселях
		Offset PixelOffset;

		//На сколько смещаться вправо для следующего символа на вьюпорте
		float ViewportAdvanceX; 

		//На сколько смещаться вправо для следующего символа в пикселях
		float PixelAdvanceX;

		//Список дополнительных отступов для следующих за этим символов
		FontKerning* KerningsList; 

		//Количество отступов для следующих за этим символов
		int numKernings; 

		/*
			Получение отступа до следующего символа с ID равным nextCharId
			wchar_t nextCharId - ID символа до которого нужно получить отступ
		*/
		float GetKerning(int nextCharId)
		{
			for (int i = 0; i < numKernings; ++i)
			{
				if (KerningsList[i].ToCharId == nextCharId)
					return KerningsList[i].ViewportAmount;
			}
			return 0.0f;
		}
	};

	struct Tile
	{
		int id;
		int row;
		int column;
	};
}