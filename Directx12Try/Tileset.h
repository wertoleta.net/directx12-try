﻿#pragma once

#include "tinyxml.h"
#include "Structures.h"
#include "StringHelper.h"
#include "ResourceDescription.h"

namespace Tringine
{
	class Tileset
	{
	private:
		std::wstring _texturePath;
		std::wstring _textureName;
		std::wstring _path;
		std::wstring _name;
		Size _tileSize;
		int _spacing;
		int _margin;
		int _tileCount;
		int _columns;
		Size _imageSize;

		void LoadFromXml(TiXmlDocument &tilesetDocument);
	public:
		Tileset(std::wstring pathToTilesetFile);
		~Tileset();

		std::wstring GetTexturePath();
		std::wstring GetTextureName();
		Margin GetTileTextureAreaById(int tileId);
	};
}