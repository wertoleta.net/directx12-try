﻿#pragma once

#include "tinyxml.h"
#include "Animation.h"
#include "Frame.h"
#include "types.h"

using namespace Tringine;

typedef std::map<std::wstring, FramesList> StringToFrameListMap;

namespace Tringine
{
	namespace Util
	{
		//Класс для работы с атласной анимацией
		class SpriteSheetHelper
		{
		public:
			//Базовый конструктор
			SpriteSheetHelper();
			//Базовый деструктор
			~SpriteSheetHelper();

			//Получение относительного пути к файлу изображения атласа
			static std::wstring GetImageFilePathFromXml(TiXmlDocument spritesheetXml);

			//Загрузка анимации из атласа
			static void LoadAnimationsFromXml(TiXmlDocument spritesheetXml, std::shared_ptr<Texture> texture);

		private:
			//Получение имени анимации из имени спрайта
			static std::wstring GetAnimationNameFromSpriteName(std::wstring spriteName);

			//Получение списков кадров анимации с уникальными названиями
			static StringToFrameListMap GetAnimationWithUniqueNames(TiXmlHandle atlasHandle);

			//Получение кадра из xml спрайта
			static std::shared_ptr < Frame> GetFrameBySpriteXml(TiXmlElement* spriteXml, TextureSize textureSize);
		};
	}
}