﻿#include "pch.h"
#include "Key.h"

namespace Tringine
{
	namespace Input
	{
		Key::Key(int code) :
			_code(code),
			_state(KeyState::KSUp),
			_nextState(KeyState::KSNone)
		{

		}

		Key::~Key()
		{

		}


		void Key::Update()
		{
			switch (_state)
			{
			case KeyState::KSPressed:
				_state = KeyState::KSDown;
				break;
			case KeyState::KSReleased:
				_state = KeyState::KSUp;
				break;
			}
			if (_nextState != KeyState::KSNone)
			{
				switch (_nextState)
				{
				case KeyState::KSPressed:
					for (int bindedActionIndex = 0; bindedActionIndex < _bindedActions.size(); bindedActionIndex++)
					{
						_bindedActions[bindedActionIndex]->Activate();
					}
					break;
				case KeyState::KSReleased:
					for (int bindedActionIndex = 0; bindedActionIndex < _bindedActions.size(); bindedActionIndex++)
					{
						_bindedActions[bindedActionIndex]->Deactivate();
					}
					break;
				}
				_state = _nextState;
				_nextState = KeyState::KSNone;
			}
		};

		void Key::Press()
		{
			if (IsUp())
			{
				_nextState = KeyState::KSPressed;
			}
		};

		void Key::Release()
		{
			if (IsDown())
			{
				_nextState = KeyState::KSReleased;
			}
		};

		bool Key::IsDown()
		{
			return _state == KeyState::KSDown || _state == KeyState::KSPressed;
		};

		bool Key::IsUp()
		{
			return _state == KeyState::KSUp || _state == KeyState::KSReleased;
		};

		bool Key::IsPressed()
		{
			return _state == KeyState::KSPressed;
		};

		bool Key::IsReleased()
		{
			return _state == KeyState::KSReleased;
		};

		void Key::BindAction(std::shared_ptr<Action> action)
		{
			_bindedActions.push_back(action);
		}

		void Key::UnbindAction(std::shared_ptr < Action> action)
		{
			for (int bindedActionIndex = 0; bindedActionIndex < _bindedActions.size(); bindedActionIndex++)
			{
				if (_bindedActions[bindedActionIndex] == action)
				{
					_bindedActions.erase(_bindedActions.begin() + bindedActionIndex);
					break;
				}
			}
		}
	}
}