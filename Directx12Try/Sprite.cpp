﻿#include "pch.h"
#include "Sprite.h"
#include "PlaneTexturedGeometry.h"

using namespace Tringine::Geometry;

namespace Tringine
{
	namespace Entities
	{
		Sprite::Sprite(std::wstring animationName)
		{
			_pipelinePointer = PipelineStateHelper::Get(PipelineStateHelper::SpritePipelineStateName);
			SwitchAnimation(animationName);
		}


		Sprite::~Sprite()
		{
		}


		void Sprite::Update(float timeScale)
		{
			TexturedEntity::Update(timeScale);
			if (_animation != nullptr)
			{
				size_t framesCount = _animation->GetFramesCount();
				if (!_isReversed)
				{
					_animationProgress += _animationSpeed * timeScale;
					if (_isLooped)
					{
						while (_animationProgress >= framesCount)
						{
							_animationProgress = _animationProgress - framesCount;
						}
					}
					else if (_animationProgress > framesCount)
					{
						_animationProgress = (float)framesCount;
					}
				}
				else
				{
					_animationProgress -= _animationSpeed * timeScale;
					if (_isLooped)
					{
						while (_animationProgress < 0.0f)
						{
							_animationProgress = _animationProgress + framesCount;
						}
					}
					else if (_animationProgress < 0.0f)
					{
						_animationProgress = 0.0f;
					}
				}
				_currentFrame = (int)floor(_animationProgress);
				int frame = (int)floor(fminf(_currentFrame, framesCount - 1));
				GeometryPointer = _animation->GetFrame(frame)->GetGeometry();
			}
		}

		void Sprite::SwitchAnimation(std::wstring animationName, bool isLooped, bool isReversed)
		{
			_isLooped = isLooped;
			_isReversed = isReversed;
			if (_animation != nullptr && _animation->GetName() == animationName)
			{
				return;
			}
			_animation = Animation::Get(animationName);
			if (_animation != nullptr)
			{
				size_t framesCount = _animation->GetFramesCount();
				_animationProgress = isReversed ? framesCount : 0.0f;
				TexturePointer = _animation->GetTexturePointer();
				_currentFrame = (int)floor(_animationProgress);
				int frame = (int)floor(fminf(_currentFrame, framesCount - 1));
				GeometryPointer = _animation->GetFrame(frame)->GetGeometry();
			}
			else
			{
				_animationProgress = 0.0f;
				TexturePointer = nullptr;
				GeometryPointer = nullptr;
			}
		}

		void Sprite::SwitchAnimation(std::wstring animationName, bool isLooped)
		{
			SwitchAnimation(animationName, isLooped, false);
		}

		void Sprite::SwitchAnimation(std::wstring animationName)
		{
			SwitchAnimation(animationName, true);
		}

		bool Sprite::IsAnimationFinished()
		{
			if (_animation == nullptr)
			{
				return true;
			}

			return 
				!_isLooped && 
				(
					(!_isReversed && (_animation->GetFramesCount() - _animationProgress) < 0.00001f) ||
					(_isReversed && _animationProgress < 0.00001f)
				);
		}
	}
}