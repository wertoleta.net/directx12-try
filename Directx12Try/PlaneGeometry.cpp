﻿#include "pch.h"
#include "PlaneGeometry.h"

namespace Tringine
{
	namespace Geometry
	{
		unsigned short PlaneGeometry::Indices[] =
		{
			0, 1, 2,
			3, 0, 2
		};

		PlaneGeometry::PlaneGeometry() :
			_size(1.0f, 1.0f),
			_origin(0.5f, 0.5f)			
		{
			_topology = D3D_PRIMITIVE_TOPOLOGY_LINESTRIP;
		}

		PlaneGeometry::PlaneGeometry(XMFLOAT2 size, XMFLOAT2 origin) :
			_size(size),
			_origin(origin)
		{
			_topology = D3D_PRIMITIVE_TOPOLOGY_LINESTRIP;
		}


		PlaneGeometry::~PlaneGeometry()
		{
		}

		void PlaneGeometry::InitInstanceValues()
		{
			_indexCountPerInstance = 6;
			_instancesCount = 1;
			_startIndexLocation = 0;
			_baseVertexLocation = 0;
			_startInstanceLocation = 0;
		}

		void PlaneGeometry::Initialize(
			ID3D12Device& device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList)
		{
			InitInstanceValues();
			float left = -_origin.x;
			float top = -_origin.y;
			float right = -_origin.x + _size.x;
			float bottom = -_origin.y + _size.y;
			Vertex planeVertices[] =
			{
				{ XMFLOAT3(left, top, 0.0f) },
				{ XMFLOAT3(right, top, 0.0f)},
				{ XMFLOAT3(right, bottom, 0.0f) },
				{ XMFLOAT3(left, bottom, 0.0f) },
			};
			CreateVertexBuffer(device, commandsList, reinterpret_cast<BYTE*>(planeVertices), sizeof(Vertex), sizeof(planeVertices) / sizeof(Vertex));
			CreateIndexBuffer(device, commandsList, Indices, sizeof(Indices) / sizeof(unsigned short));
		}
	}
}