﻿#pragma once
#include "Base.h"
#include "Entity.h"

namespace Tringine
{
	namespace Entities
	{
		/*
			Класс представляет собой игровой объект (слой) который объеденяет другие игровые объекты в группу
		*/
		class Group :
			public Entity
		{
		private:
			//Список указателей на всех детей группы
			std::vector<std::shared_ptr<Base>> _children;
		public:
			//Базовый конструктор
			Group();

			//Базовый деструктор
			virtual ~Group();

			//Основной цикл обновления
			void Update(float timeScale) override;

			/*
				Основной цикл пост-обновления
				Drawer &drawer - объект-отрисовщик
			*/
			void PostUpdate(Drawer& drawer) override;

			/*
				Основной цикл отрисовки
				Drawer &drawer - объект-отрисовщик
			*/
			void Draw(Drawer& drawer) override;

			/*
				Добавление объекта в группу
				Base &childToAdd - объект для добавления
			*/
			void Add(std::shared_ptr < Base> childToAdd);

			/*
				Удаление объекта из группы
				Base &childToAdd - объект для удаления
			*/
			void Remove(std::shared_ptr < Base> childToRemove);

			/*
				Проверка на то содержит-ли группа определенный объект
				Base &childToAdd - объект для проверки нахождения в группе
				return bool - находится ли данный объект в группе
			*/
			bool IsContains(std::shared_ptr < Base> childToFind);

			/*
				Получение "мёртвого" объекта для переиспользования
				return Base* - указатель на "мёртвый" объект группы
			*/
			std::shared_ptr <Base> GetFirstDead();

			/*
				Очистка группы от всех добавленных объектов
				bool isNeedDelete - нужно-ли удалять объекты из памяти (delete) после удаления их из группы.
			*/
			void Clear(bool isNeedDelete);
		};
	}
}