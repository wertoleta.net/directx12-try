﻿#pragma once

#include "tinyxml.h"
#include "TiledMapItem.h"

namespace Tringine
{
	class TiledLayer : public TiledMapItem
	{
	private:
		int _id;
		std::wstring _name;
		std::wstring _textureName;
		int _width;
		int _height;
		std::vector<std::vector<int>*> _tiles;

	public:
		static const std::wstring XmlElementName;

		TiledLayer(TiXmlElement *layerElement);
		~TiledLayer();
		int GetTileIdAtIndex(int index);
		std::wstring GetTilesetTextureName();
		void SetTilesetTextureName(std::wstring textureName);
		IntegerSize GetSize();
	};
}
