﻿#pragma once
#include "TexturedEntity.h"
#include "TiledMap.h"


namespace Tringine
{
	namespace Entities
	{
		class TilemapLayerEntity :
			public TexturedEntity
		{
		public:
			TilemapLayerEntity(std::shared_ptr < TiledMap> mapData, std::shared_ptr < TiledLayer> layer);
			virtual ~TilemapLayerEntity();

			//Основной цикл обновления
			void Update(float timeScale) override;

			/*
				Основной цикл пост-обновления
				Drawer &drawer - объект-отрисовщик
			*/
			void PostUpdate(Drawer& drawer) override;

			/*
				Основной цикл отрисовки
				Drawer &drawer - объект-отрисовщик
			*/
			void Draw(Drawer& drawer) override;
		};
	}
}