#pragma once
#include "Sprite.h"
#include "RectangleBody.h"
#include "InputManager.h"

using namespace Tringine::Entities;
using namespace Tringine::Physics;
using namespace Tringine::Input;

namespace Game
{
	enum AdventurerState
	{
		AS_IDLE,
		AS_RUN,
		AS_FALL,
		AS_JUMP,
		AS_CROUCH,
		AS_SOMERSAULT,
		AS_WALL_SLIDE,
		AS_CORNER_GRAB,
		AS_CORNER_CLIMB_UP,
		AS_CORNER_CLIMB_DOWN,
		AS_SLIDE,
		AS_ATTACK_1,
		AS_ATTACK_2,
		AS_ATTACK_RUN,
		AS_ATTACK_AIR_1,
		AS_ATTACK_AIR_2
	};

	class Adventurer :
		public Sprite
	{
	private:
		const float MaxJumpsCounter = 6;
		const float SomersaultDelay = 20;
		const float MinSlideSideArea = 9;
		const FloatSize IdleSize = FloatSize(11.0f, 25.0f);
		const FloatSize CrouchSize = FloatSize(11.0f, 12.0f);

		std::shared_ptr <RectangleBody> _body;
		AdventurerState _state = AdventurerState::AS_IDLE;
		AdventurerState _nextState = AdventurerState::AS_IDLE;
		float _jumpTimer = 0.0f;
		int _lastPressedDirection = 0;
		float _accelerationTimer = 0.0f;
		float _somersaultTimer = 0.0f;
		Side _slidedSide;
		FloatPoint _bodyPhysicalOffset;
		FloatPoint _bodyAnimationOffset;

		std::shared_ptr < Action> _leftAction;
		std::shared_ptr < Action> _rightAction;
		std::shared_ptr < Action> _upAction;
		std::shared_ptr < Action> _downAction;
		std::shared_ptr < Action> _attackAction;

		void ProcessMovementInput();
		void ProcessMovement(float timeScale);
		void CheckFloor(float timeScale);
		void ProcessJump(float timeScale);
		void ProcessIdle(float timeScale);
		void ProcessFriction(float timeScale);
		void ProcessFall(float timeScale);
		void ProcessRun(float timeScale);
		void ProcessSomersault(float timeScale);
		void ProcessWallSlide(float timeScale);
		void ProcessCornerGrab(float timeScale);
		void ProcessCornerClimbUp(float timeScale);
		void ProcessCornerClimbDown(float timeScale);
		void ProcessSlide(float timeScale);
		void ProcessAttack1(float timeScale);
		void ProcessAttack2(float timeScale);
		void ProcessAttackAir1(float timeScale);
		void ProcessAttackAir2(float timeScale);
		void ProcessAttackRun(float timeScale);
		void CheckAirAttack();
		void CheckJump(float timeScale);
		void DoWallJump(Side wallSide);
		void SetState(AdventurerState newState);
	public:
		Adventurer(FloatPoint spawnPosition);
		~Adventurer();

		void Update(float timeScale) override;
		void Draw(Drawer& drawer) override;
	};
}
