﻿#pragma once

#include "Types.h"
#include "tinyxml.h"
#include "Polyline.h"
#include "XmlHelper.h"
#include "TiledEntry.h"

using namespace Tringine::Util;

namespace Tringine
{
	class TiledObject : public TiledEntry 
	{
	private:
		IntegerRectangle _area;
		std::wstring _type;
		std::shared_ptr < Polyline> _polyline;

	public:
		TiledObject(TiXmlElement *element);
		
		IntegerRectangle GetArea();
		std::wstring GetTypeName();
		std::shared_ptr < Polyline> GetPolyline();
	};
};

