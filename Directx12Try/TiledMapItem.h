﻿#pragma once

#include "Types.h"
#include "tinyxml.h"
#include "XmlHelper.h"
#include "TiledEntry.h"

using namespace Tringine::Util;

namespace Tringine
{
	enum TiledItemType
	{
		TileLayer,
		ObjectGroup
	};

	class TiledMapItem : public TiledEntry
	{
	private:
		TiledItemType _type;
	public: 
		TiledMapItem(TiledItemType type, TiXmlElement* element);
		TiledItemType GetType();

		virtual void MakeThisClassVirtual();
	};
};

