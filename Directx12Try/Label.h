﻿#pragma once

#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"
#include "RootSignatureHelper.h"
#include "Drawer.h" 
#include "Font.h"
#include "Base.h"

using namespace DX;
using namespace DirectX;
using namespace Tringine;
using namespace TringineDx;

namespace Tringine
{
	namespace Entities
	{
		/*
			Класс представляет собой объект для отрисовки текста на GUI
		*/
		class Label : public Base
		{
		private:
			//Символ следующей строки
			static wchar_t NextLineChar;

			//Максимальное количество символов для отрисовки за один кадр
			static int MaxNumCharacters;

			//Тройной буфер для вершин текста
			static Microsoft::WRL::ComPtr<ID3D12Resource> VertexBuffer[DX::c_frameCount];

			//Вью на тройной буффер вершин текста
			static D3D12_VERTEX_BUFFER_VIEW	VertexBufferView[DX::c_frameCount];

			//Адреса тройного буфера в GPU
			static UINT8* VbGpuAddress[DX::c_frameCount];

			//Ссылка на шрифт которым рисовать текст
			std::shared_ptr <Font> _fontPointer;

			//Текст для отрисовки
			std::wstring _text;

			//Положение текста в пространстве вьюпорта (в пикселях)
			XMFLOAT2 _pos;

			//Растяжение текста
			XMFLOAT2 _scale;

			//Отступы между буквами (x) и строками (y)
			XMFLOAT2 _padding;

			//Цвет текста
			XMFLOAT4 _color;
		public:
			/*
				Базовый конструктор
				std::wstring text - текст для отрисовки
				Font *fontPointer = nullptr - ссылка на шрифт которым рисовать текст.
			*/
			Label(std::wstring text, std::shared_ptr<Font> fontPointer = nullptr);

			//Базовый деструктор
			~Label();

			//Номер следующего символа для отрисовки
			static int NextDrawCharacterNumber;

			/*
				Отрисовка текста
				Drawer &drawer - объект-рисовальщик
			*/
			void Draw(Drawer& drawer) override;

			/*
				Установка текста для отрисовки
				std::wstring newText - новый текст для отрисовки
			*/
			void SetText(std::wstring newText);

			/*
				Создание ресурсов зависищах от устройства
				ID3D12Device &device - устройства для которого создавать ресурсы
			*/
			static void CreateDeviceDependentResources(ID3D12Device& device);
		};
	}
}