﻿#include "pch.h"
#include "PipelineStateHelper.h"
#include "ShaderLoadingInfo.h"
#include "Common\DirectXHelper.h"
#include <ppltasks.h>

using namespace TringineDx;

const std::wstring TextureVertexShaderFilePath = L"SpriteVertexShader.cso";
const std::wstring TexturePixelShaderFilePath = L"SpritePixelShader.cso";
const std::wstring TextVertexShaderFilePath = L"TextVertexShader.cso";
const std::wstring TextPixelShaderFilePath = L"TextPixelShader.cso";
const std::wstring GeometryVertexShaderFilePath = L"GeometryVertexShader.cso";
const std::wstring GeometryPixelShaderFilePath = L"GeometryPixelShader.cso";
const std::wstring SpriteVertexShaderFilePath = L"SpriteVertexShader.cso";
const std::wstring SpritePixelShaderFilePath = L"SpritePixelShader.cso";

const std::wstring PipelineStateHelper::MeshTexturedGeomentyPipelineStateName = L"MeshTexturedGeometry";
const std::wstring PipelineStateHelper::MeshGeomentyPipelineStateName = L"MeshGeometry";
const std::wstring PipelineStateHelper::TextPipelineStateName = L"Text";
const std::wstring PipelineStateHelper::SpritePipelineStateName = L"Sprite";

StringToPipelineStateMap PipelineStateHelper::LoadedPipelines;


Concurrency::task< void > PipelineStateHelper::CreateBasePipelineStates(
	std::shared_ptr<DX::DeviceResources> &deviceResources,
	Microsoft::WRL::ComPtr<ID3D12RootSignature>& rootSignature)
{
	Concurrency::task< void > 
		createTextureVSTask, createTexturePSTask, 
		createTextVSTask, createTextPSTask,
		createGeometryVSTask, createGeometryPSTask,
		createSpriteVSTask, createSpritePSTask;

	std::vector<TringineDx::ShaderLoadingInfo> * shaderLoadingInfos = new std::vector<TringineDx::ShaderLoadingInfo>();

	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(TextureVertexShaderFilePath, &_textureVertexShader, &createTextureVSTask)); 
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(TexturePixelShaderFilePath, &_texturePixelShader, &createTexturePSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(TextVertexShaderFilePath, &_textVertexShader, &createTextVSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(TextPixelShaderFilePath, &_textPixelShader, &createTextPSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(GeometryVertexShaderFilePath, &_geometryVertexShader, &createGeometryVSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(GeometryPixelShaderFilePath, &_geometryPixelShader, &createGeometryPSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(SpriteVertexShaderFilePath, &_spriteVertexShader, &createSpriteVSTask));
	shaderLoadingInfos->push_back(TringineDx::ShaderLoadingInfo(SpritePixelShaderFilePath, &_spritePixelShader, &createSpritePSTask));
	
	TringineDx::ShaderLoadingInfo::LoadShadersAsynch(*shaderLoadingInfos);

	ID3D12Device *device = deviceResources->GetD3DDevice();

	auto createTexturePipelineStateTask = (createTextureVSTask && createTexturePSTask).then([
		this,
		&rootSignature,
		&deviceResources,
		device
	]() {
		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		};

		D3D12_BLEND_DESC textureBlendStateDesc = {};
		textureBlendStateDesc.AlphaToCoverageEnable = FALSE;
		textureBlendStateDesc.IndependentBlendEnable = FALSE;
		textureBlendStateDesc.RenderTarget[0].BlendEnable = TRUE;

		textureBlendStateDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		textureBlendStateDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		textureBlendStateDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		
		textureBlendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
		textureBlendStateDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
		textureBlendStateDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;

		textureBlendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		CD3DX12_DEPTH_STENCIL_DESC depthDescription = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		depthDescription.DepthFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
		//depthDescription.DepthEnable = false;

		D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = rootSignature.Get();
		state.VS = CD3DX12_SHADER_BYTECODE(&_textureVertexShader[0], _textureVertexShader.size());
		state.PS = CD3DX12_SHADER_BYTECODE(&_texturePixelShader[0], _texturePixelShader.size());
		state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		state.DepthStencilState = depthDescription;
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 1;
		state.RTVFormats[0] = deviceResources->GetBackBufferFormat();
		state.DSVFormat = deviceResources->GetDepthBufferFormat();
		state.SampleDesc.Count = 1;
		state.BlendState = textureBlendStateDesc;

		Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
		DX::ThrowIfFailed(device->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&pipelineState)));

		_textureVertexShader.clear();
		_texturePixelShader.clear();

		LoadedPipelines.insert({ PipelineStateHelper::MeshTexturedGeomentyPipelineStateName, pipelineState });
	});

	auto createTextPipelineStateTask = (createTextVSTask && createTextPSTask).then(
		[
			this,
			&rootSignature,
			&deviceResources,
			device
	]() {
		static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 }
		};

		D3D12_BLEND_DESC textBlendStateDesc = {};
		textBlendStateDesc.AlphaToCoverageEnable = FALSE;
		textBlendStateDesc.IndependentBlendEnable = FALSE;
		textBlendStateDesc.RenderTarget[0].BlendEnable = TRUE;

		textBlendStateDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		textBlendStateDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
		textBlendStateDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;

		textBlendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
		textBlendStateDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
		textBlendStateDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;

		textBlendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC textDepthStencilDesc = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
		textDepthStencilDesc.DepthEnable = false;

		D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
		state.InputLayout = { inputLayout, _countof(inputLayout) };
		state.pRootSignature = rootSignature.Get();
		state.VS = CD3DX12_SHADER_BYTECODE(&_textVertexShader[0], _textVertexShader.size());
		state.PS = CD3DX12_SHADER_BYTECODE(&_textPixelShader[0], _textPixelShader.size());
		state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		state.SampleMask = UINT_MAX;
		state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		state.NumRenderTargets = 1;
		state.RTVFormats[0] = deviceResources->GetBackBufferFormat();
		state.DSVFormat = deviceResources->GetDepthBufferFormat();
		state.SampleDesc.Count = 1;
		state.DepthStencilState = textDepthStencilDesc;
		state.BlendState = textBlendStateDesc;

		Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
		DX::ThrowIfFailed(device->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&pipelineState)));

		// Данные шейдеров можно удалить после создания состояния конвейера.
		_textVertexShader.clear();
		_textPixelShader.clear();

		LoadedPipelines.insert({ PipelineStateHelper::TextPipelineStateName, pipelineState });
	});

	auto createGeometryPipelineStateTask = (createGeometryVSTask && createGeometryPSTask).then([
		this,
			&rootSignature,
			&deviceResources,
			device
	]() {
			static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			};

			D3D12_BLEND_DESC geometryBlendStateDesc = {};
			geometryBlendStateDesc.AlphaToCoverageEnable = FALSE;
			geometryBlendStateDesc.IndependentBlendEnable = FALSE;
			geometryBlendStateDesc.RenderTarget[0].BlendEnable = TRUE;

			geometryBlendStateDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
			geometryBlendStateDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
			geometryBlendStateDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;

			geometryBlendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
			geometryBlendStateDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
			geometryBlendStateDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;

			geometryBlendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

			CD3DX12_DEPTH_STENCIL_DESC depthDescription = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
			depthDescription.DepthFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;

			D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
			state.InputLayout = { inputLayout, _countof(inputLayout) };
			state.pRootSignature = rootSignature.Get();
			state.VS = CD3DX12_SHADER_BYTECODE(&_geometryVertexShader[0], _geometryVertexShader.size());
			state.PS = CD3DX12_SHADER_BYTECODE(&_geometryPixelShader[0], _geometryPixelShader.size());
			state.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
			state.DepthStencilState = depthDescription;
			state.SampleMask = UINT_MAX;
			state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
			state.NumRenderTargets = 1;
			state.RTVFormats[0] = deviceResources->GetBackBufferFormat();
			state.DSVFormat = deviceResources->GetDepthBufferFormat();
			state.SampleDesc.Count = 1;
			state.BlendState = geometryBlendStateDesc;

			Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
			DX::ThrowIfFailed(device->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&pipelineState)));

			_geometryVertexShader.clear();
			_geometryPixelShader.clear();

			LoadedPipelines.insert({ PipelineStateHelper::MeshGeomentyPipelineStateName, pipelineState });
		});

	auto createSpritePipelineStateTask = (createSpriteVSTask && createSpritePSTask).then([
		this,
			&rootSignature,
			&deviceResources,
			device
	]() {
			static const D3D12_INPUT_ELEMENT_DESC inputLayout[] =
			{
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,	D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
			};

			D3D12_BLEND_DESC spriteBlendStateDesc = {};
			spriteBlendStateDesc.AlphaToCoverageEnable = FALSE;
			spriteBlendStateDesc.IndependentBlendEnable = FALSE;
			spriteBlendStateDesc.RenderTarget[0].BlendEnable = TRUE;

			spriteBlendStateDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
			spriteBlendStateDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
			spriteBlendStateDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;

			spriteBlendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
			spriteBlendStateDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
			spriteBlendStateDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;

			spriteBlendStateDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

			CD3DX12_DEPTH_STENCIL_DESC depthDescription = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
			depthDescription.DepthFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
			//depthDescription.DepthEnable = false;

			CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
			rasterizerDesc.CullMode = D3D12_CULL_MODE::D3D12_CULL_MODE_NONE;

			D3D12_GRAPHICS_PIPELINE_STATE_DESC state = {};
			state.InputLayout = { inputLayout, _countof(inputLayout) };
			state.pRootSignature = rootSignature.Get();
			state.VS = CD3DX12_SHADER_BYTECODE(&_spriteVertexShader[0], _spriteVertexShader.size());
			state.PS = CD3DX12_SHADER_BYTECODE(&_spritePixelShader[0], _spritePixelShader.size());
			state.RasterizerState = rasterizerDesc;
			state.DepthStencilState = depthDescription;
			state.SampleMask = UINT_MAX;
			state.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			state.NumRenderTargets = 1;
			state.RTVFormats[0] = deviceResources->GetBackBufferFormat();
			state.DSVFormat = deviceResources->GetDepthBufferFormat();
			state.SampleDesc.Count = 1;
			state.BlendState = spriteBlendStateDesc;

			Microsoft::WRL::ComPtr<ID3D12PipelineState> pipelineState;
			DX::ThrowIfFailed(device->CreateGraphicsPipelineState(&state, IID_PPV_ARGS(&pipelineState)));

			_spriteVertexShader.clear();
			_spritePixelShader.clear();

			LoadedPipelines.insert({ PipelineStateHelper::SpritePipelineStateName, pipelineState });
		});

	auto cleanupTask = (createTexturePipelineStateTask && createTextPipelineStateTask && 
		createGeometryPipelineStateTask && createSpritePipelineStateTask).then(
		[this,
		shaderLoadingInfos]() 
	{
		delete shaderLoadingInfos;
	});

	return cleanupTask;
}

Microsoft::WRL::ComPtr<ID3D12PipelineState> PipelineStateHelper::Get(std::wstring name)
{
	StringToPipelineStateMap::iterator iterator = LoadedPipelines.find(name);
	if (iterator == LoadedPipelines.end())
	{
		return nullptr;
	}
	return iterator->second;
}