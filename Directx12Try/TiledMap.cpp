﻿#include "pch.h"
#include "TiledMap.h"
#include "XmlHelper.h"

using namespace Tringine::Util;

namespace Tringine
{
	typedef std::map<std::wstring, std::shared_ptr<TiledMap>> StringToTiledMapMap;
	StringToTiledMapMap TiledMap::LoadedTiledMaps;

	TiledMap::TiledMap(TiXmlDocument xml)
	{
		TiXmlElement *rootElement = xml.FirstChildElement();
		if (rootElement == nullptr)
		{
			return;
		}

		TiXmlHandle rootHandle(rootElement);

		std::wstring orientationString = XmlHelper::GetWStringAttributeValue(rootElement, "orientation", L"orthogonal");
		if (orientationString == L"orthogonal")
		{
			_orientation = Orientation::Orthogonal;
		}
		else
		{
			throw new std::exception("Map orientation not supported");
		}

		std::wstring renderOrderString = XmlHelper::GetWStringAttributeValue(rootElement, "renderorder", L"left-up");
		if (renderOrderString == L"left-up")
		{
			_renderOrder = RenderOrder::LeftUp;
		}
		else
		{
			throw new std::exception("Map render order not supported");
		}

		_size.Width = XmlHelper::GetIntAttributeValue(rootElement, "width", 0);
		_size.Height = XmlHelper::GetIntAttributeValue(rootElement, "height", 0);
		_tileSize.Width = XmlHelper::GetIntAttributeValue(rootElement, "tilewidth", 0);
		_tileSize.Height = XmlHelper::GetIntAttributeValue(rootElement, "tileheight", 0);

		std::string path = xml.ValueStr();
		std::wstring wpath(path.begin(), path.end());
		std::wstring directory;
		directory.resize(path.length());
		errno_t error = _wsplitpath_s(wpath.c_str(), NULL, 0, &directory[0], directory.size(), NULL, 0, NULL, 0);
		if (error == EINVAL)
		{
			throw std::exception("Wrong map path value");
		}
		directory = StringHelper::ResizeToRealLength(directory);
		

		TiXmlElement* tilesetElement = rootHandle.FirstChild("tileset").Element();
		for (tilesetElement; tilesetElement; tilesetElement = tilesetElement->NextSiblingElement("tileset"))
		{
			std::wstring tilesetPath = XmlHelper::GetWStringAttributeValue(tilesetElement, "source", L"");
			int tilesetFirstTileId = XmlHelper::GetIntAttributeValue(tilesetElement, "firstgid", 1);
			std::wstring tilesetFullPath = directory + tilesetPath;
			std::shared_ptr < Tileset> tileset = std::make_shared<Tileset>(tilesetFullPath);
			_tilesets.push_back(std::make_shared<TilesetEntry>(tileset, tilesetFirstTileId));
		}

		TiXmlNode *mapItemSibling = rootHandle.FirstChildElement().Element();
		for (mapItemSibling; mapItemSibling; mapItemSibling = mapItemSibling->NextSibling())
		{
			TiXmlElement *element = mapItemSibling->ToElement();
			std::string elementName = mapItemSibling->ValueStr();
			std::wstring wElementName = std::wstring(elementName.begin(), elementName.end());
			std::shared_ptr <TiledMapItem> mapItem = nullptr;
			if (wElementName == TiledLayer::XmlElementName)
			{
				std::shared_ptr <TiledLayer> layer = std::make_shared<TiledLayer>(element);
				IntegerSize size = layer->GetSize();
				int tileId = 0;
				for (int row = 0; row < size.Height; row++)
				{
					for (int column = 0; column < size.Width; column++)
					{
						tileId = layer->GetTileIdAtIndex(row * size.Width + column);
						if (tileId > 0)
						{
							break;
						}
					}
					if (tileId > 0)
					{
						break;
					}
				}
				layer->SetTilesetTextureName(GetTilesetByTileId(tileId)->TilesetItem->GetTextureName());
				mapItem = layer;
			}
			else if (wElementName == TiledObjectsGroup::XmlElementName)
			{
				std::shared_ptr < TiledObjectsGroup> group = std::make_shared<TiledObjectsGroup>(element);
				mapItem = group;
			}
			else
			{
				continue;
			}
			_layers.push_back(mapItem);
		}
	}

	TiledMap::~TiledMap()
	{
	}

	std::vector<std::wstring> TiledMap::GetImagesToLoad()
	{
		std::vector<std::wstring> imagesToLoad;
		for (int tilesetIndex = 0; tilesetIndex < _tilesets.size(); tilesetIndex++)
		{
			std::shared_ptr < Tileset> tileset = _tilesets[tilesetIndex]->TilesetItem;
			imagesToLoad.push_back(tileset->GetTexturePath());
		}
		return imagesToLoad;
	}

	IntegerSize TiledMap::GetSize()
	{
		return _size;
	}

	IntegerSize TiledMap::GetTileSize()
	{
		return _tileSize;
	}

	std::vector< std::shared_ptr<TiledMapItem>> TiledMap::GetItems()
	{
		return _layers;
	}
	
	IntegerPoint TiledMap::GetTilePositionAtIndex(int index)
	{
		switch (_renderOrder)
		{
			case RenderOrder::LeftUp:
				return IntegerPoint(index % _size.Width, index / _size.Width);
			default:
				return IntegerPoint(0, 0);
		}
		return IntegerPoint(0, 0);
	}

	Margin TiledMap::GetTileTextureAreaById(UINT tileId)
	{
		std::shared_ptr < TilesetEntry> tileset = GetTilesetByTileId(tileId);
		return tileset->TilesetItem->GetTileTextureAreaById(tileId - tileset->FirstTileId + 1);
	}

	std::shared_ptr < TilesetEntry> TiledMap::GetTilesetByTileId(UINT tileId)
	{
		std::shared_ptr < TilesetEntry> tileset = _tilesets[0];
		for (int tilesetIndex = 1; tilesetIndex < _tilesets.size(); tilesetIndex++)
		{
			std::shared_ptr < TilesetEntry> nextTileset = _tilesets[tilesetIndex];
			if (nextTileset->FirstTileId > tileId)
			{
				break;
			}
			tileset = nextTileset;
		}
		return tileset;
	}

	void TiledMap::Add(std::wstring name, std::shared_ptr < TiledMap> texture)
	{
		if (LoadedTiledMaps.count(name) > 0)
		{
			throw std::exception("Texture with same name already exists");
		}
		LoadedTiledMaps.insert({ name, texture });
	}

	void TiledMap::Remove(std::wstring name)
	{
		std::shared_ptr < TiledMap> map = Get(name);
		if (map == nullptr)
		{
			throw std::exception("Map doesn't exist");
		}
		LoadedTiledMaps.erase(name);
	}

	std::shared_ptr < TiledMap> TiledMap::Get(std::wstring name)
	{
		StringToTiledMapMap::iterator iterator = LoadedTiledMaps.find(name);
		if (iterator == LoadedTiledMaps.end())
		{
			return nullptr;
		}
		return iterator->second;
	}

	void TiledMap::Delete(std::wstring name)
	{
		StringToTiledMapMap::iterator iterator = LoadedTiledMaps.find(name);
		if (iterator == LoadedTiledMaps.end())
		{
			return;
		}
		LoadedTiledMaps.erase(iterator);
	}
}