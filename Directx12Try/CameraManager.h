﻿#pragma once
#include "Base.h"
#include "Entity.h"
#include "TrinGame.h"
#include "Rule.h"

using namespace Tringine::Entities;

namespace Game
{

	class CameraManager :
		public Base
	{
	private:
		std::shared_ptr<Entity> _target;
		IntegerRectangle _globalBoundaries;
		std::vector<Rule*> _rules;
		Rule* _currentRule;

	protected:
		Rule* GetRule(std::wstring name);
		void SetCurrentRule(Rule* rule);

	public:
		CameraManager();
		~CameraManager();

		void Update(float timeScale) override;
		void SetTarget(std::shared_ptr < Entity> entity);
		void SetGlobalBoundaries(IntegerRectangle& boundaries);
		void AddRule(Rule* rule);
		void SetCurrentRule(std::wstring ruleName);
		void SetInitialSize(FloatSize size);
	};
}

