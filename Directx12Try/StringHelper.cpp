﻿#include "pch.h"
#include "StringHelper.h"

namespace Tringine
{
	namespace Util
	{
		std::vector<std::wstring> StringHelper::Split(std::wstring string, wchar_t delimeter)
		{
			std::vector<std::wstring> parts;
			std::wstringstream wss(string);
			std::wstring temp;
			while (std::getline(wss, temp, delimeter))
			{
				parts.push_back(temp);
			}
			return parts;
		}

		std::wstring StringHelper::ToUpperFirstLetterOfEveryWord(std::wstring sentence)
		{
			std::vector<std::wstring> words = StringHelper::Split(sentence, L'_');
			std::wstring result = L"";
			for (int wordIndex = 0; wordIndex < words.size(); wordIndex++)
			{
				std::wstring word = words[wordIndex];
				for (int letterIndex = 0; letterIndex < word.length(); letterIndex++)
				{
					if (letterIndex == 0)
					{
						result += toupper(word[letterIndex]);
					}
					else
					{
						result += tolower(word[letterIndex]);
					}
				}
			}
			return result;
		}

		std::string StringHelper::ToStdString(std::wstring string)
		{	
			using convert_type = std::codecvt_utf8<wchar_t>;
			std::wstring_convert<convert_type, wchar_t> converter;
			return converter.to_bytes(string);
		}

		std::wstring StringHelper::ResizeToRealLength(std::wstring string) 
		{
			int realLength = 0;
			for (int charIndex = 0; charIndex < string.length(); charIndex++)
			{
				if (string[charIndex] == 0)
				{
					break;
				}
				realLength++;
			}
			string.resize(realLength);
			return string;
		}
	}
}