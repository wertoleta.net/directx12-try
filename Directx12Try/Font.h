﻿#pragma once
#include "Structures.h"
#include "collection.h"

using namespace DX;
using namespace TringineDx;

namespace Tringine
{

	/*
		Класс представляющий собой растровый-шрифт (bitmap-font)
	*/
	class Font
	{
	public:
		/*
			Тип словаря где ключ - строка (имя шрифта), а значение - соответствующий шрифт
		*/
		typedef std::map<std::wstring, std::shared_ptr<Font>> StringToFontMap;

		//Глобальный список всех загруженных шрифтов
		static StringToFontMap LoadedFonts;

		/*
			Добавление шрифта в глобальный список
			std::wstring Name - имя шрифта
			Font &font - шрифт для добавленияы
		*/
		static void Add(std::wstring name, std::shared_ptr<Font> font);

		/*
			Удаление шрифта
			std::wstring Name - имя шрифта
		*/
		static void Remove(std::wstring name);

		/*
			Получение загруженного шрифта по имени
			std::wstring Name - имя шрифта для получения
		*/
		static std::shared_ptr <Font> Get(std::wstring name);

		//Получение первого шрифта из глобального списка
		static std::shared_ptr <Font> GetFirst();

		/*
			Удаление шрифта по имени
			std::wstring Name - имя шрифта для удаления
		*/
		static void Delete(std::wstring name);

		/*
			Пересчёт соотношени япиксельных размеров к размеру вьюпорта для всех шрифтов
			float screenWidth - ширина экрана (вьюпорта) в пикселях
			float screenHeight - высота экрана (вьюпорта) в пикселях
		*/
		static void AllCalculatePixelToViewport(float screenWidth, float screenHeight);

		//Базовый конструктор
		Font();

		//Базовый деструктор
		~Font();

		//Имя шрифта
		std::wstring Name; 

		//Путь к файлу изображения содержащего шрифт
		std::wstring FontImage;

		//Размер шрифта
		int Size; 

		//Высота линии по отношению к вьюпорту
		float ViewportLineHeight; 

		//Базовая высота символов по отношению к вьюпорту
		float ViewportBaseHeight; 

		//Высота линии в пикселях
		float PixelLineHeight;

		//Базовая высота символов в пикселях
		float PixelBaseHeight;

		//Указатель на текстуру содержащую символы шрифта
		std::shared_ptr <Tringine::Texture> TexturePointer;

		//Количество символов в шрифте
		int numCharacters; 

		//Список указателей на символы шрифта 
		FontChar* CharPointerList; 

		//Отступы по отношению к вьюпорту
		Margin ViewportPadding;

		//Отступы в пикселях
		Margin PixelPadding;

		/*
			Получение символа из шрифта по его ID
			int charId - ID символа
		*/
		FontChar* GetChar(int charId);

		/*
			Пересчёт отношения размеров к вьююпорту
			float screenWidth - ширина экрана (вьюпорта) в пикселях
			float screenHeight - ширина экрана (вьюпорта) в пикселях
		*/
		void CalculatePixelToViewport(float screenWidth, float screenHeight);
	};
}

