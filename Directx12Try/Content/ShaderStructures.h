﻿#pragma once

namespace Directx12Try
{
	// Буфер констант, используемый для отправки матриц MVP в шейдер вершин.
	struct ModelViewProjectionConstantBuffer
	{
		DirectX::XMFLOAT4X4 mvpMat;
	};
}