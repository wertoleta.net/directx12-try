Texture2D t1 : register(t0);
SamplerState s1 : register(s0);

// �������� ������ ��������� ������, ����������� ����� ������ ��������.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float2 texCoord: TEXCOORD;
};

// �������� ������� ��� (�����������������) ������ � �����.
float4 main(PixelShaderInput input) : SV_TARGET
{
	return t1.Sample(s1, input.texCoord);
}
