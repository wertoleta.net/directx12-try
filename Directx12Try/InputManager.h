﻿#pragma once

#include "Keyboard.h"
#include "Action.h"
#include <stdlib.h>
#include <map>

using namespace std;

namespace Tringine
{
	namespace Input
	{
		typedef std::map<std::wstring, std::shared_ptr<Action>> StringToActionMap;

		class InputManager
		{
		private:
			std::shared_ptr <Keyboard> _keyboard;
			StringToActionMap _actions;

		public:
			InputManager();

			std::shared_ptr <Keyboard> GetKeyboard();
			void Update();

			void AddAction(std::wstring name, std::shared_ptr < Action> action);
			std::shared_ptr < Action> GetAction(std::wstring name);
		};
	}
}

