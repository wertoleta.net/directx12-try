﻿#include "pch.h"
#include "Drawer.h"
#include "Common\DirectXHelper.h"
#include "Label.h"

using namespace Tringine;
using namespace TringineDx;
using namespace Tringine::Entities;

Drawer::Drawer():
Camera(std::make_shared<Tringine::Camera>())
{
}


Drawer::~Drawer()
{
}

Concurrency::task<void> Drawer::CreateDeviceDependentResources(const std::shared_ptr<DX::DeviceResources> &deviceResources)
{
	DeviceResources = deviceResources;
	DevicePointer = deviceResources->GetD3DDevice(); 
	CommandQueuePointer = DeviceResources->GetCommandQueue();
	
	Label::CreateDeviceDependentResources(*DevicePointer);

	auto baseInitTask = RootSignatureHelper.InitBaseRootSignature(DeviceResources);
	auto createCommandListTask = baseInitTask.then(
			[
				this,
				&deviceResources
			]() {
		DX::ThrowIfFailed(DevicePointer->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, deviceResources->GetCommandAllocator(), PipelineStateHelper::Get(PipelineStateHelper::MeshGeomentyPipelineStateName).Get(), IID_PPV_ARGS(&CommandList)));
		NAME_D3D12_OBJECT(CommandList);
		DX::ThrowIfFailed(CommandList->Close());
	});
	return createCommandListTask;
}
