﻿#include "pch.h"
#include "Label.h"
#include "Structures.h"
#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"
#include "RootSignatureHelper.h"
#include "Drawer.h"

using namespace DX;
using namespace TringineDx;

namespace Tringine
{
	namespace Entities
	{
		wchar_t Label::NextLineChar = L'\n';
		int Label::MaxNumCharacters = 1024;
		int Label::NextDrawCharacterNumber = 0;
		Microsoft::WRL::ComPtr<ID3D12Resource> Label::VertexBuffer[DX::c_frameCount];
		UINT8* Label::VbGpuAddress[DX::c_frameCount]; // this is a pointer to each of the text constant buffers
		D3D12_VERTEX_BUFFER_VIEW Label::VertexBufferView[DX::c_frameCount]; // a view for our text vertex buffer

		Label::Label(std::wstring text, std::shared_ptr<Font> font) :
			_fontPointer(font),
			_text(text),
			_pos(XMFLOAT2(0.0f, 0.0f)),
			_scale(XMFLOAT2(1.0f, 1.0f)),
			_padding(XMFLOAT2(0.0f, 0.0f)),
			_color(XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f))
		{
			if (_fontPointer == nullptr)
			{
				_fontPointer = Font::GetFirst();
			}
			Revive();
		}


		Label::~Label()
		{
		}

		void Label::SetText(std::wstring newText)
		{
			_text = newText;
		}

		void Label::Draw(TringineDx::Drawer& drawer)
		{
			Base::Draw(drawer);
			if (_fontPointer == nullptr)
			{
				return;
			}

			int frameIndex = drawer.DeviceResources->GetCurrentFrameIndex();
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList = drawer.CommandList;
			commandList->ClearDepthStencilView(drawer.RootSignatureHelper.DepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 0.0f, 0, 0, nullptr);
			commandList->SetPipelineState(PipelineStateHelper::Get(PipelineStateHelper::TextPipelineStateName).Get());
			commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
			commandList->IASetVertexBuffers(0, 1, &Label::VertexBufferView[frameIndex]);
			commandList->SetGraphicsRootDescriptorTable(1, _fontPointer->TexturePointer->GetSrvHandle());

			TringineDx::FloatPoint labelScreenPosition;
			labelScreenPosition.X = (_pos.x * 2.0f) - 1.0f;
			labelScreenPosition.Y = ((1.0f - _pos.y) * 2.0f) - 1.0f;

			TringineDx::FloatPoint nextCharPosition;
			nextCharPosition = labelScreenPosition;

			TringineDx::FloatPoint nextCharPadding;
			nextCharPadding.X = (_fontPointer->ViewportPadding.Left + _fontPointer->ViewportPadding.Right) * _padding.x;
			nextCharPadding.Y = (_fontPointer->ViewportPadding.Top + _fontPointer->ViewportPadding.Bottom) * _padding.y;

			TextVertex* vertexes = (TextVertex*)Label::VbGpuAddress[frameIndex];

			FontChar tmp;
			FontChar* lastChar = &tmp;

			for (size_t i = 0; i < _text.size(); ++i)
			{
				wchar_t stringChar = _text[i];
				FontChar* fontChar = _fontPointer->GetChar(stringChar);

				if (fontChar == nullptr)
					continue;

				if (NextDrawCharacterNumber >= Label::MaxNumCharacters)
				{
					break;
				}

				if (stringChar == Label::NextLineChar)
				{
					nextCharPosition.X = labelScreenPosition.X;
					nextCharPosition.Y -= (_fontPointer->ViewportLineHeight + nextCharPadding.Y) * _scale.y;
					continue;
				}

				float kerning = lastChar->GetKerning(stringChar);

				vertexes[NextDrawCharacterNumber] = TextVertex(
					_color.x,
					_color.y,
					_color.z,
					_color.w,
					fontChar->TexturePosition.X,
					fontChar->TexturePosition.Y,
					fontChar->TextureSize.Width,
					fontChar->TextureSize.Height,
					nextCharPosition.X + ((fontChar->ViewportOffset.X + kerning) * _scale.x),
					nextCharPosition.Y - (fontChar->ViewportOffset.Y * _scale.y),
					fontChar->ViewportSize.Width * _scale.x,
					fontChar->ViewportSize.Height * _scale.y);

				NextDrawCharacterNumber++;

				nextCharPosition.X += (fontChar->ViewportAdvanceX - nextCharPadding.X) * _scale.x;

				lastChar = fontChar;
			}

			commandList->DrawInstanced(4, NextDrawCharacterNumber, 0, 0);
		}

		void Label::CreateDeviceDependentResources(ID3D12Device& device)
		{
			CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
			CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
			CD3DX12_RANGE readRange(0, 0);

			for (int frameIndex = 0; frameIndex < DX::c_frameCount; ++frameIndex)
			{
				DX::ThrowIfFailed(device.CreateCommittedResource(
					&uploadHeapProperties,
					D3D12_HEAP_FLAG_NONE,
					&CD3DX12_RESOURCE_DESC::Buffer(Label::MaxNumCharacters * sizeof(TextVertex)),
					D3D12_RESOURCE_STATE_GENERIC_READ,
					nullptr,
					IID_PPV_ARGS(&Label::VertexBuffer[frameIndex])));
				NAME_D3D12_OBJECT(Label::VertexBuffer[frameIndex]);

				DX::ThrowIfFailed(Label::VertexBuffer[frameIndex]->Map(0, &readRange, reinterpret_cast<void**>(&Label::VbGpuAddress[frameIndex])));

				Label::VertexBufferView[frameIndex].BufferLocation = Label::VertexBuffer[frameIndex]->GetGPUVirtualAddress();
				Label::VertexBufferView[frameIndex].StrideInBytes = sizeof(TextVertex);
				Label::VertexBufferView[frameIndex].SizeInBytes = Label::MaxNumCharacters * sizeof(TextVertex);
			}
		}
	}
}