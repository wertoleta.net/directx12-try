﻿#pragma once

#include "Structures.h"
#include "Common/DirectXHelper.h"
#include "Font.h"
#include "Drawer.h"
#include "ResourceDescription.h"

using namespace DX;
using namespace Tringine;

namespace Tringine
{
	/*
		Класс для загрузки ресурсов игры
	*/
	class ResourceManager
	{
	public:

		/*
			Загрузка списка ресурсов
			std::vector<ResourceDescription> - список ресурсов для загрузки
		*/
		void RequestResources(std::vector< std::shared_ptr<ResourceDescription>> resourcesDescriptions);

		static ResourceManager* GetInstance();
	
	private:
		/*
			Конструктор
		*/
		ResourceManager();

		static ResourceManager* Instance;

		//Список загруженных ресурсов
		static std::vector< std::shared_ptr<ResourceDescription>> LoadedResources;

		//Индекс следующего файла для загрузки
		static int NextSpritesheetId;

		/*
			Получить строковое значение атрибута. Пример: Font="Arial", вернёт: `Arial`.
			std::wstring &attribute - атрибут
		*/
		static std::wstring GetAttributeStringValue(std::wstring &attribute);

		/*
			Получить значение атрибута. `Font="Arial"`, вернёт: `"Arial"`. 
			std::wstring &attribute - атрибут
		*/
		static std::wstring GetAttributeValue(std::wstring &attribute);

		/*
			Получить числовое атрибута.
			std::wstring &attribute - атрибут
		*/
		static int GetAttributeIntValue(std::wstring &attribute);

		/*
			Получить значение атрибута под индексом. `Padding=2,3,4,5`, вернёт: `2` для индекса 0, `4` для индекса 2.
			std::wstring &attribute - атрибут
			UINT index - индекс искомого значения
		*/
		static std::wstring GetAttributeValueAtIndex(std::wstring attributeValue, UINT index);

		/*
			Загрузка шрифта
			ResourceDescription description - описание ресурса шрифта
		*/
		static void LoadFont(ResourceDescription &description);

		/*
			Загрузка текстуры из файла
			std::wstring fileName - Имя файла из которого грузить текстуру
			std::wstring textureName - Имя текстуры для обращения в дальнейшем
		*/
		static void LoadTextureFromFile(ResourceDescription &description);

		/*
			Загрузка карты из файла
			ResourceDescription description - описание ресурса шрифта
		*/
		static void LoadMapFromFile(ResourceDescription &description);

		/*
			Загрузка анимаций из атласа
			std::wstring fileName - Имя файла из которого грузить анимации
		*/
		static void LoadAnimationsFromSpriteSheetFile(ResourceDescription &description);

		static void LoadResource(std::shared_ptr < ResourceDescription> description);
		static void RemoveResource(std::shared_ptr < ResourceDescription> description);
	};
}