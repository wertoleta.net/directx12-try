﻿#pragma once

#include "PipelineStateHelper.h"
#include "ConstantBuffer.h"
#include "Structures.h"
#include "Texture.h"
#include "Common\DeviceResources.h"
#include <ppltasks.h>

using namespace Tringine;
using namespace TringineDx;
using namespace Windows::Foundation;

namespace TringineDx
{
	/*
		Класс для работы с ID3D12RootSignature в DirectX12
		Создаёт базовые пайплайны отрисовки с помощью PipelineStateHelper, создаёт базовую кучу дескрипторов, базовый буфер констант и DepthStencilBuffer
	*/
	class RootSignatureHelper
	{
	public:
		//Корневая сигнатура которую инициализирует объект этого класса
		Microsoft::WRL::ComPtr<ID3D12RootSignature> RootSignature;

		//Помощник по работе с пайплайнами отрисовки
		TringineDx::PipelineStateHelper PipelineStateHelper;

		//Куча дескрипторов глубины
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> DepthStencilDescriptorHeap;

		//Буффер глубины
		Microsoft::WRL::ComPtr<ID3D12Resource> DepthStencilBuffer;

		//Буффер констант
		TringineDx::ConstantBuffer ConstantBuffer;

		/*
			Инициализация базовой RootSignature
			std::shared_ptr<DX::DeviceResources> &deviceResources - ресурсы устройства для которого инициализировать корневую сигнатуру
		*/
		Concurrency::task<void> InitBaseRootSignature(std::shared_ptr<DX::DeviceResources> &deviceResources);

		/*
			Инициализация пустой корневой сигнатуры
			ID3D12Device* device - устройство для которого инициализировать пустую сигнатуру
		*/
		void InitEmptyRootSignature(ID3D12Device &device);

		void FillDepthStencil(ID3D12Device& device, Size outputSize);
	private:
		/*
			Создание буффера глубины

			ID3D12Device* device - устройство для которого инициализировать буфер глубины
			Size outputSize - размер области для которой инициализировать буфер глубины
		*/
		void CreateDepthStencilBuffer(ID3D12Device &device, Size outputSize);
	};
}