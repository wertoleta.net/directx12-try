﻿#pragma once

#include "Common\DeviceResources.h"
#include "ShaderStructures.h"
#include "ShaderLoadingInfo.h"
#include "Types.h"
#include <ppltasks.h>

using namespace DX;

namespace TringineDx
{
	/*
		Класс для упрощения работы с ID3D12PipelineState
		Создаёт базовые пайплайны для работы с текстурированной геометрией и текстом
		Загружает базовые шейдеры для текстур и текста
		TODO: Перенести список паплайнов в RootSignatureHelper
	*/
	class PipelineStateHelper
	{
	public:
		static const std::wstring MeshTexturedGeomentyPipelineStateName;
		static const std::wstring MeshGeomentyPipelineStateName;
		static const std::wstring TextPipelineStateName;
		static const std::wstring SpritePipelineStateName;

		/*
			Создание базовых пайплайнов для геометрий и текста
			std::shared_ptr<DX::DeviceResources> &deviceResources - ресурсы устройства вывода
			Microsoft::WRL::ComPtr<ID3D12RootSignature>& rootSignature - корневая сигнатура для которой создавать пайплайны
		*/
		Concurrency::task< void > CreateBasePipelineStates(
			std::shared_ptr<DX::DeviceResources> &deviceResources,
			Microsoft::WRL::ComPtr<ID3D12RootSignature>& rootSignature);

		/*
			Получение пайплайна по имени.
			std::wstring name - имя пайплайна для поиска.
		*/
		static Microsoft::WRL::ComPtr<ID3D12PipelineState> Get(std::wstring name);

	private:
		//Список всех загруженных пайплайнов. Доступ по имени.
		static StringToPipelineStateMap LoadedPipelines;

		//Вертексный шейдр для текста
		std::vector<byte> _textVertexShader;

		//Пиксельный шейдр для текста
		std::vector<byte> _textPixelShader;

		//Вертексный шейдр для текстур
		std::vector<byte> _textureVertexShader;

		//Пиксельный шейдр для текстур
		std::vector<byte> _texturePixelShader;

		//Вертексный шейдр для геометрии
		std::vector<byte> _geometryVertexShader;

		//Пиксельный шейдр для геометрии
		std::vector<byte> _geometryPixelShader;

		//Вертексный шейдр для геометрии
		std::vector<byte> _spriteVertexShader;

		//Пиксельный шейдр для геометрии
		std::vector<byte> _spritePixelShader;
	};
}