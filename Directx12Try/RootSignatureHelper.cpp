﻿#include "pch.h"
#include "RootSignatureHelper.h"
#include "Texture.h"
#include "Structures.h"
#include "Common\DirectXHelper.h"
#include <ppltasks.h>

using namespace Tringine;
using namespace TringineDx;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;

Concurrency::task< void > RootSignatureHelper::InitBaseRootSignature(
	std::shared_ptr<DX::DeviceResources> &deviceResources)
{
	ID3D12Device *device = deviceResources->GetD3DDevice();
	InitEmptyRootSignature(*device);

	Size outputSize = deviceResources->GetOutputSize();
	CreateDepthStencilBuffer(*device, outputSize);
	ConstantBuffer.Init(*device);

	return PipelineStateHelper.CreateBasePipelineStates(deviceResources, RootSignature);
}

void RootSignatureHelper::InitEmptyRootSignature(ID3D12Device &device)
{
	CD3DX12_DESCRIPTOR_RANGE range;
	CD3DX12_ROOT_PARAMETER parameters[2];
	CD3DX12_STATIC_SAMPLER_DESC sampler;

	range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND);

	parameters[0].InitAsConstantBufferView(0, 0, D3D12_SHADER_VISIBILITY_VERTEX);
	parameters[1].InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_PIXEL);

	// create a static sampler
	sampler.Init(0,
		D3D12_FILTER_MIN_MAG_MIP_POINT,
		D3D12_TEXTURE_ADDRESS_MODE_BORDER, D3D12_TEXTURE_ADDRESS_MODE_BORDER, D3D12_TEXTURE_ADDRESS_MODE_BORDER,
		0.0f, 0U,
		D3D12_COMPARISON_FUNC_NEVER,
		D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK,
		0.0f, D3D12_FLOAT32_MAX,
		D3D12_SHADER_VISIBILITY_PIXEL,
		0U);

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS;

	CD3DX12_ROOT_SIGNATURE_DESC descRootSignature;
	descRootSignature.Init(_countof(parameters), parameters, 1, &sampler, rootSignatureFlags);

	ComPtr<ID3DBlob> pSignature;
	ComPtr<ID3DBlob> pError;

	DX::ThrowIfFailed(D3D12SerializeRootSignature(&descRootSignature, D3D_ROOT_SIGNATURE_VERSION_1, pSignature.GetAddressOf(), pError.GetAddressOf()));
	DX::ThrowIfFailed(device.CreateRootSignature(0, pSignature->GetBufferPointer(), pSignature->GetBufferSize(), IID_PPV_ARGS(&RootSignature)));

	//Create description heap
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	heapDesc.NumDescriptors = Texture::MaxTextureDescriptors;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	DX::ThrowIfFailed(device.CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&Texture::DescriptorHeap)));	

	NAME_D3D12_OBJECT(RootSignature);
}

void RootSignatureHelper::CreateDepthStencilBuffer(ID3D12Device &device, Size outputSize)
{
	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	DX::ThrowIfFailed(device.CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&DepthStencilDescriptorHeap)));
}

void RootSignatureHelper::FillDepthStencil(ID3D12Device& device, Size outputSize)
{
	D3D12_CLEAR_VALUE depthOptimizedClearValue = {};
	depthOptimizedClearValue.Format = DXGI_FORMAT_D32_FLOAT;
	depthOptimizedClearValue.DepthStencil.Depth = 0.0f;
	depthOptimizedClearValue.DepthStencil.Stencil = 0;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);

	CD3DX12_RESOURCE_DESC resourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_D32_FLOAT,
		(UINT64)outputSize.Width, (UINT)outputSize.Height,
		1, 0,
		1, 0, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL);

	DX::ThrowIfFailed(device.CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&depthOptimizedClearValue,
		IID_PPV_ARGS(&DepthStencilBuffer)));
	NAME_D3D12_OBJECT(DepthStencilBuffer);

	D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilDesc = {};
	depthStencilDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	depthStencilDesc.Flags = D3D12_DSV_FLAG_NONE;

	device.CreateDepthStencilView(DepthStencilBuffer.Get(), &depthStencilDesc, DepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
}