﻿#include "pch.h"
#include "Rule.h"

namespace Game
{
	Rule::Rule(std::shared_ptr < TiledObject> object):
		_name(object->GetName()),
		_nextName(object->GetProperty(L"next"))
	{
		_endViewportSize.Width = (float)stoi(object->GetProperty(L"endCameraWidth", L"1920"));
		_endViewportSize.Height = (float)stoi(object->GetProperty(L"endCameraHeight", L"1080"));
	}

	std::wstring Rule::GetName()
	{
		return _name;
	}

	std::wstring Rule::GetNextName()
	{
		return _nextName;
	}

	CameraParameters Rule::Process(CameraParameters current, float timeScale)
	{
		return current;
	}

	bool Rule::IsFinished()
	{
		return true;
	}

	void Rule::SetStartViewportSize(FloatSize newSize)
	{
		_startViewportSize = newSize;
	}

	FloatSize Rule::GetEndViewportSize()
	{
		return _endViewportSize;
	}
}