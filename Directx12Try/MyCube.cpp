﻿#include "pch.h"
#include "MyCube.h"
#include "Texture.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace Game
{
	MyCube::MyCube(FloatRectangle area)
	{
		TexturePointer = Texture::Get(L"Box");

		SetPosition(area.Position.X, area.Position.Y);
		GeometryPointer = TringineDx::MakeDynamicResource<BoxGeometry>(XMFLOAT3(area.Size.Width, area.Size.Height, 1.0f), XMFLOAT3(area.Size.Width / 2.0f, area.Size.Height / 2.0f, 0.5f));

		Revive();
	}


	MyCube::~MyCube()
	{
	}

	void MyCube::Update(float timeScale)
	{
		TexturedEntity::Update(timeScale);
	}

	void MyCube::Draw(Drawer &drawer)
	{
		TexturedEntity::Draw(drawer);
	}
}