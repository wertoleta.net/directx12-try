﻿#include "pch.h"
#include "Rails.h"

namespace Game
{

	Rails::Rails(std::shared_ptr < TiledObject> object) : Rule(object)
	{
		_polyline = object->GetPolyline();
		_passedSegmentsProgress = 0.0f;
	};

	bool Rails::IsFinished()
	{
		return _currentSegmentIndex >= _polyline->GetSegmentsCount();
	}

	CameraParameters Rails::Process(CameraParameters current, float timeScale)
	{
		FloatSegment segment = _polyline->GetSegmentAt((int) fminf((float)_currentSegmentIndex, _polyline->GetSegmentsCount() - 1.0f));

		float maxSpeed = timeScale * (4.0f / segment.Length());
		float railsProgress = _passedSegmentsProgress;
		float currentSegmentGlobalProgressPart = segment.Length() / _polyline->GetLength();

		if (!IsFinished())
		{
			float segmentProgress = fmaxf(fminf(segment.ProgressOfNearestPointTo(current.CenterPosition), _currentSegmentProgress + maxSpeed), _currentSegmentProgress);
			_currentSegmentProgress = fminf(segmentProgress, 1.0f);
			railsProgress += currentSegmentGlobalProgressPart * _currentSegmentProgress;
		}
		else
		{
			_currentSegmentProgress = 1.0f;
		}

		current.CenterPosition = segment.PointAtProgress(_currentSegmentProgress);
		current.Size = _startViewportSize + (_endViewportSize - _startViewportSize) * railsProgress;

		if (_currentSegmentProgress >= 0.9999f && !IsFinished())
		{
			_passedSegmentsProgress += currentSegmentGlobalProgressPart;
			_passedSegmentsProgress = fminf(1.0f, _passedSegmentsProgress);

			_currentSegmentIndex++;
			_currentSegmentProgress = 0.0f;
		}

		return current;
	}
}