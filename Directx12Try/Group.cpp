﻿#include "pch.h"
#include "Group.h"
#include "Base.h"

namespace Tringine
{
	namespace Entities
	{
		Group::Group()
		{
			Revive();
		}


		Group::~Group()
		{
			Clear(true);
		}

		void Group::Update(float timeScale)
		{
			Entity::Update(timeScale);
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr < Base> child = _children[childIndex];
				if (child->IsActive())
				{
					_children[childIndex]->Update(timeScale);
				}
			}
		}

		void Group::PostUpdate(Drawer& drawer)
		{
			Entity::PostUpdate(drawer);
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr < Base> child = _children[childIndex];
				if (child->IsActive())
				{
					_children[childIndex]->PostUpdate(drawer);
				}
			}
		}

		void Group::Draw(Drawer& drawer)
		{
			Entity::Draw(drawer);
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr < Base> child = _children[childIndex];
				if (child->IsVisible())
				{
					child->Draw(drawer);
				}
			}
		}

		void Group::Add(std::shared_ptr <Base> childToAdd)
		{
			if (childToAdd->ParentPointer != nullptr)
			{
				Group* currentGroup = dynamic_cast<Group*>(childToAdd->ParentPointer);
				currentGroup->Remove(childToAdd);
			}
			childToAdd->ParentPointer = this;
			_children.push_back(childToAdd);
		}

		void Group::Remove(std::shared_ptr<Base> childToRemove)
		{
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr<Base> child = _children.at(childIndex);
				if (childToRemove == child)
				{
					childToRemove->ParentPointer = nullptr;
					_children.erase(_children.begin() + childIndex);
					break;
				}
			}
		}

		bool Group::IsContains(std::shared_ptr <Base> childToFind)
		{
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr <Base> child = _children.at(childIndex);
				if (childToFind == child)
				{
					return true;
				}
			}
			return false;
		}

		std::shared_ptr <Base> Group::GetFirstDead()
		{
			for (int childIndex = 0; childIndex < _children.size(); childIndex++)
			{
				std::shared_ptr < Base> child = _children.at(childIndex);
				if (!child->IsExists())
				{
					return child;
				}
			}
			return nullptr;
		}

		void Group::Clear(bool isNeedDelete)
		{
			while (_children.size() > 0)
			{
				Remove(_children[0]);
			}
		}
	}
}