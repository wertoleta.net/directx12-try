﻿#pragma once

#include "tinyxml.h"
#include "TiledMapItem.h"
#include "TiledObject.h"

namespace Tringine
{
	typedef std::vector< std::shared_ptr<TiledObject>> TiledObjectsList;

	class TiledObjectsGroup : public TiledMapItem
	{
	private:
		TiledObjectsList _objects;
	public:
		static const std::wstring XmlElementName;

		TiledObjectsGroup(TiXmlElement *element);
		UINT GetObjectsCount();
		std::shared_ptr < TiledObject> GetObjectAt(UINT index);
	};
};

