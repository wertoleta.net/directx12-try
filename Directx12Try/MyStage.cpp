﻿#include "pch.h"
#include "MyStage.h"
#include "MyCube.h"
#include "TrinGame.h"
#include "Hero.h"
#include "Adventurer.h"
#include <string>

using namespace Tringine;

namespace Game
{
	MyStage::MyStage()
	{
		_cameraManager = std::make_shared<CameraManager>();
	}


	MyStage::~MyStage()
	{
	}

	void MyStage::Create()
	{
		Stage::Create();

		FillFrom(TiledMap::Get(L"Caves"));

		_fpsText = std::make_shared<Label>(L"FPS: ");
		Add(_fpsText);

		Add(_cameraManager);
	}

	void MyStage::Update(float timeScale)
	{
		Stage::Update(timeScale);
		_fpsText->SetText(L"FPS: " + std::to_wstring(TrinGame::Instance->RealFps));	

		std::shared_ptr < Keyboard> keyboard = TrinGame::Instance->GetInput()->GetKeyboard();
		if (keyboard->GetKeyByName(L"F1")->IsPressed())
		{
			if (_spacePointer->IsVisible())
			{
				_spacePointer->Hide();
			}
			else
			{
				_spacePointer->Show();
			}
		}
		if (keyboard->GetKeyByName(L"GamepadDPadRight")->IsPressed() || keyboard->GetKeyByName(L"F2")->IsPressed())
		{
			if (_fpsText->IsVisible())
			{
				_fpsText->Hide();
			}
			else
			{
				_fpsText->Show();
			}
		}
		if (keyboard->GetKeyByName(L"F5")->IsPressed())
		{
			TrinGame::Instance->SwitchStage(std::make_shared<MyStage>());
		}
	}

	std::shared_ptr < Base> MyStage::GenerateObject(std::shared_ptr < TiledObject> item)
	{
		if (item->GetTypeName() == L"Hero")
		{
			float x = (float)item->GetArea().Position.X;
			float y = (float)item->GetArea().Position.Y;
			std::shared_ptr <Hero> hero = std::make_shared<Hero>(FloatPoint(x, y));
			_cameraManager->SetTarget(hero);
			return hero;
		}else if (item->GetTypeName() == L"Adventurer")
		{
			float x = (float)item->GetArea().Position.X;
			float y = (float)item->GetArea().Position.Y;
			std::shared_ptr <Adventurer> adventurer = std::make_shared<Adventurer>(FloatPoint(x, y));
			_cameraManager->SetTarget(adventurer);
			return adventurer;
		}
		else if (item->GetTypeName() == L"Terrain")
		{
			std::shared_ptr <RectangleBody> newRectangle = std::make_shared<RectangleBody>(item->GetArea());
			std::wstring sidesParameter = item->GetProperty(L"sides");
			if (sidesParameter.length() > 0)
			{
				newRectangle->SetSolidSides(std::stoi(sidesParameter));
			}
			_spacePointer->AddBody(newRectangle);
		}
		else if (item->GetTypeName() == L"CameraBoundaries")
		{
			_cameraManager->SetGlobalBoundaries(item->GetArea());
		}
		return Stage::GenerateObject(item);
	}

	bool MyStage::ProcessObject(std::shared_ptr < TiledObject> item)
	{
		if (item->GetTypeName() == L"Terrain")
		{
			std::shared_ptr < RectangleBody> newRectangle = std::make_shared<RectangleBody>(item->GetArea());
			std::wstring sidesParameter = item->GetProperty(L"sides");
			if (sidesParameter.length() > 0)
			{
				newRectangle->SetSolidSides(std::stoi(sidesParameter));
			}
			_spacePointer->AddBody(newRectangle);
			return true;
		}
		else if (item->GetTypeName() == L"CameraBoundaries")
		{
			_cameraManager->SetGlobalBoundaries(item->GetArea());
			return true;
		}
		else if (item->GetTypeName() == L"CameraRails")
		{
			Rails* rails = new Rails(item);
			_cameraManager->AddRule(rails);
			return true;
		}
		return Stage::ProcessObject(item);
	}

	void MyStage::ProcessMapItem(std::shared_ptr < TiledMapItem> item)
	{
		if (item->GetName() == L"Settings")
		{
			_cameraManager->SetCurrentRule(item->GetProperty(L"InitialCameraRule", L""));
			_cameraManager->SetInitialSize(
				FloatSize(
					stof(item->GetProperty(L"InitialCameraWidth", L"1920")), 
					stof(item->GetProperty(L"InitialCameraHeight", L"1080"))
				)
			);
		}
		Stage::ProcessMapItem(item);
	}

	std::vector< std::shared_ptr<ResourceDescription>> MyStage::GetRequiredResources()
	{
		std::vector<std::shared_ptr<ResourceDescription>> requiredResources;

		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Font, L"./Assets/font.fnt"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Texture, L"./Assets/box.jpg"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Texture, L"./Assets/pyramid.png"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Texture, L"./Assets/coords.png"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Map, L"./Assets/Maps/testmap.tmx"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_Map, L"./Assets/Maps/caves.tmx"));
		requiredResources.push_back(std::make_shared<ResourceDescription>(ResourceType::RT_SpriteSheet, L"./Assets/Sprites/sprites.xml"));

		return requiredResources;
	}
}
