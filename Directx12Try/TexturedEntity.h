﻿#pragma once
#include "Entity.h"

namespace Tringine
{
	namespace Entities
	{
		class TexturedEntity :
			public Entity
		{
		public:
			TexturedEntity();
			~TexturedEntity();

			//”казатель на текстуру
			std::shared_ptr<Tringine::Texture> TexturePointer;

			void Draw(Drawer& drawer) override;
		};

	}
}