﻿#include "pch.h"
#include "CameraManager.h"

namespace Game
{
	CameraManager::CameraManager():
		_currentRule(nullptr)
	{
		Revive();
	}

	CameraManager::~CameraManager()
	{
		for (int ruleIndex = 0; ruleIndex < _rules.size(); ruleIndex++)
		{
			Rule* rule = _rules[ruleIndex];
			delete rule;
		}
	}

	void CameraManager::Update(float timeScale)
	{
		std::shared_ptr < Camera> camera = TrinGame::Instance->GetCamera();
		CameraParameters newParameters(FloatPoint(camera->Position.x, camera->Position.y), camera->Size);

		if (_target != nullptr)
		{
			XMFLOAT3 targetPosition = _target->GetPosition();
			newParameters.CenterPosition.X = targetPosition.x;
			newParameters.CenterPosition.Y = targetPosition.y;
		}

		if (_currentRule != nullptr)
		{
			newParameters = _currentRule->Process(newParameters, timeScale);
			if (_currentRule->IsFinished())
			{
				Rule* nextRule = GetRule(_currentRule->GetNextName());
				if (nextRule != nullptr)
				{
					nextRule->SetStartViewportSize(_currentRule->GetEndViewportSize());
					SetCurrentRule(nextRule);
				}
			}
		}

		float left = newParameters.CenterPosition.X - camera->Size.Width / 2.0f;
		if (left < _globalBoundaries.Position.X)
		{
			newParameters.CenterPosition.X += _globalBoundaries.Position.X - left;
		}

		float right = newParameters.CenterPosition.X + camera->Size.Width / 2.0f;
		if (right > _globalBoundaries.Position.X + _globalBoundaries.Size.Width)
		{
			newParameters.CenterPosition.X -= right - (_globalBoundaries.Position.X + _globalBoundaries.Size.Width);
		}

		float top = newParameters.CenterPosition.Y - camera->Size.Height / 2.0f;
		if (top < _globalBoundaries.Position.Y)
		{
			newParameters.CenterPosition.Y += _globalBoundaries.Position.Y - top;
		}

		float bottom = newParameters.CenterPosition.Y + camera->Size.Height / 2.0f;
		if (bottom > _globalBoundaries.Position.Y + _globalBoundaries.Size.Height)
		{
			newParameters.CenterPosition.Y -= bottom - (_globalBoundaries.Position.Y + _globalBoundaries.Size.Height);
		}

		newParameters.Size.Width = floorf(newParameters.Size.Width);
		newParameters.Size.Height = floorf(newParameters.Size.Height);
		camera->SetOrthoCenterPosition(newParameters.CenterPosition.X, newParameters.CenterPosition.Y);
		camera->SetOrtho(newParameters.Size);

		//OutputDebugString((std::to_wstring(newParameters.Size.Width) + L"x" + std::to_wstring(newParameters.Size.Height) + L"\n").data());
	}

	void CameraManager::SetTarget(std::shared_ptr <Entity> entity)
	{
		_target = entity;
	}

	void CameraManager::SetGlobalBoundaries(IntegerRectangle& boundaries)
	{
		_globalBoundaries = boundaries;
	}

	void CameraManager::AddRule(Rule* rule)
	{
		_rules.push_back(rule);
	}

	Rule* CameraManager::GetRule(std::wstring name)
	{
		for (int ruleIndex = 0; ruleIndex < _rules.size(); ruleIndex++)
		{
			Rule* rule = _rules[ruleIndex];
			if (rule->GetName() == name)
			{
				return rule;
			}
		}
		return nullptr;
	}

	void CameraManager::SetCurrentRule(std::wstring ruleName)
	{
		Rule* rule = GetRule(ruleName);
		SetCurrentRule(rule);
	}

	void CameraManager::SetCurrentRule(Rule* rule)
	{
		_currentRule = rule;
	}

	void CameraManager::SetInitialSize(FloatSize size)
	{
		if (_currentRule != nullptr)
		{
			_currentRule->SetStartViewportSize(size);
		}
	}
}