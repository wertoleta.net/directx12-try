﻿#include "pch.h"
#include "Directx12TryMain.h"
#include "Common\DirectXHelper.h"
#include <codecvt>
#include <locale>

using namespace Directx12Try;
using namespace Windows::Foundation;
using namespace Windows::System::Threading;
using namespace Windows::UI::Core;
using namespace Windows::UI::Input;
using namespace Concurrency;
using namespace Game;


Directx12TryMain::Directx12TryMain()
{
}

// Создает и инициализирует визуализаторов.
void Directx12TryMain::CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources)
{
	_trinGame = std::unique_ptr<TrinGame>(new TrinGame(deviceResources));

	std::shared_ptr < InputManager> input = _trinGame->GetInput();
	std::shared_ptr < Keyboard> keyboard = input->GetKeyboard();
	std::shared_ptr < Action> action;

	action = std::make_shared<Action>();
	keyboard->GetKeyByName(L"W")->BindAction(action);
	keyboard->GetKeyByName(L"Up")->BindAction(action);
	keyboard->GetKeyByName(L"GamepadX")->BindAction(action);
	input->AddAction(L"Up", action);

	action = std::make_shared<Action>();
	keyboard->GetKeyByName(L"A")->BindAction(action);
	keyboard->GetKeyByName(L"Left")->BindAction(action);
	keyboard->GetKeyByName(L"GamepadLeftThumbstickLeft")->BindAction(action);
	input->AddAction(L"Left", action);

	action = std::make_shared<Action>();
	keyboard->GetKeyByName(L"D")->BindAction(action);
	keyboard->GetKeyByName(L"Right")->BindAction(action);
	keyboard->GetKeyByName(L"GamepadLeftThumbstickRight")->BindAction(action);
	input->AddAction(L"Right", action);

	action = std::make_shared<Action>();
	keyboard->GetKeyByName(L"Down")->BindAction(action);
	keyboard->GetKeyByName(L"S")->BindAction(action);
	keyboard->GetKeyByName(L"GamepadLeftThumbstickDown")->BindAction(action);
	input->AddAction(L"Down", action);

	action = std::make_shared<Action>();
	keyboard->GetKeyByName(L"Control")->BindAction(action);
	keyboard->GetKeyByName(L"X")->BindAction(action);
	keyboard->GetKeyByName(L"GamepadX")->BindAction(action);
	input->AddAction(L"Attack", action);

	std::shared_ptr < MyStage >stage = std::make_shared<MyStage>();
	_trinGame->SwitchStage(stage);
}

void Directx12TryMain::Update()
{
	m_timer.Tick([&]()
	{
#ifdef DEBUG
		try
		{
			_trinGame->Update(m_timer);
		}
		catch (std::exception & exp)
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			std::wstring msg = converter.from_bytes(exp.what());
			OutputDebugString(msg.c_str());
			throw new exception("Unhandled exception");
		}
#else
		_trinGame->Update(m_timer);
#endif 
	});
}

bool Directx12TryMain::Render()
{
	// Не пытаться выполнять какую-либо отрисовку до первого обновления.
	if (m_timer.GetFrameCount() == 0)
	{
		return false;
	}

	return _trinGame->Render();
}

void Directx12TryMain::OnWindowSizeChanged()
{
	_trinGame->CreateWindowSizeDependentResources();
}

void Directx12TryMain::OnSuspending()
{
	_trinGame->SaveState();
}

void Directx12TryMain::OnResuming()
{
	_trinGame->LoadState();
}

void Directx12TryMain::OnDeviceRemoved()
{
	_trinGame->SaveState();
	_trinGame = nullptr;
}

void Directx12TryMain::OnKeyDown(CoreWindow^ sender, KeyEventArgs^ args)
{
	int keyCode = (int)args->VirtualKey;
	_trinGame->GetInput()->GetKeyboard()->GetKeyByCode(keyCode)->Press();
	args->Handled = true;
}

void Directx12TryMain::OnKeyUp(CoreWindow^ sender, KeyEventArgs^ args)
{
	int keyCode = (int)args->VirtualKey;
	_trinGame->GetInput()->GetKeyboard()->GetKeyByCode(keyCode)->Release();
	args->Handled = true;
}
