﻿#pragma once
#include "TexturedEntity.h"
#include "Animation.h"

using namespace Tringine;

namespace Tringine
{
	namespace Entities
	{
		class Sprite :
			public TexturedEntity
		{
		private:
			std::shared_ptr<Animation> _animation;
			float _animationProgress = 0.0f;
			int _currentFrame = 0;
			bool _isLooped;
			bool _isReversed;
		protected:
			float _animationSpeed = 1.0f;
		public:
			Sprite(std::wstring animationName);
			~Sprite();

			void SwitchAnimation(std::wstring animationName);
			void SwitchAnimation(std::wstring animationName, bool isLooped);
			void SwitchAnimation(std::wstring animationName, bool isLooped, bool isReverse);

			//Основной цикл обновления
			void Update(float timeScale) override;
			bool IsAnimationFinished();
		};
	}
}