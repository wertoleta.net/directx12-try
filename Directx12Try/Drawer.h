﻿#pragma once

#include "Common\DeviceResources.h"
#include "ShaderStructures.h"
#include "RootSignatureHelper.h"
#include "Camera.h"
#include <ppltasks.h>

using namespace TringineDx;

namespace TringineDx
{
	/*
		Класс для цикла отрисовки игры
		Содержит объекты которые могут потребоваться для отрисовки - корневую сигнатуру, ресурсы утсройства, само устройство вывода и игровую камеру.
		Также занимается создание устройство-зависимых ресурсов.
	*/
	class Drawer
	{
	public:
		/*
			Базовый конструктор
		*/
		Drawer();

		/*
			Базовый деструктор
		*/
		~Drawer();


		//Корневая сигнатура - содержит всё нужное для отрисовки при помощи DX12
		TringineDx::RootSignatureHelper	RootSignatureHelper;
		
		//Игровая камера
		std::shared_ptr < Tringine::Camera> Camera;

		//Ресурсы утсройства на которое производится вывод
		std::shared_ptr<DX::DeviceResources> DeviceResources;

		//Fence обозначающий отрисовался ли последний кадр
		Microsoft::WRL::ComPtr <ID3D12Fence> LastFrameDrawedFence;

		ID3D12CommandQueue* CommandQueuePointer;

		//Основной список команд 
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> CommandList;

		//Ссылка на устройство вывода
		ID3D12Device *DevicePointer;

		/*
			Создание устройство-зависимых ресурсов.
			const std::shared_ptr<DX::DeviceResources> &deviceResources - ресурсы устройства
		*/
		Concurrency::task<void> CreateDeviceDependentResources(const std::shared_ptr<DX::DeviceResources> &deviceResources);
	};
}
