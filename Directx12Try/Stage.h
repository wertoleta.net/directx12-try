﻿#pragma once

#include "Base.h"
#include "Group.h"
#include "Drawer.h"
#include "TiledMap.h"
#include "TilemapLayerEntity.h"
#include "TiledObject.h"
#include "RectangleSpace.h"
#include "ResourceDescription.h"

using namespace Tringine::Physics;

namespace Tringine
{
	namespace Entities
	{
		/*
			Класс представляет собой одну сцену игры
			Сцена является корневым элементов графа сцены (корневой группой) игры
		*/
		class Stage :
			public Base
		{
		protected:
			//Ссылка на корневую группу
			std::shared_ptr<Group> _mainGroupPointer;

			//Ссылка на группу объектов
			std::shared_ptr<Group> _objectsGroupPointer;

			//Ссылка на физическое пространство
			std::shared_ptr<RectangleSpace> _spacePointer;

		public:
			Stage();
			~Stage();

			/*
				Создание сцены.
				Здесь производится настройка начального состояния сцены
			*/
			void virtual Create();

			//Основной игровой цикл
			void Update(float timeScale) override;

			/*
				Цикл пост-обновления
				Drawer &drawer - рисовальщик
			*/
			void PostUpdate(Drawer& drawer) override;

			/*
				Цикл отрисовки
				Drawer &drawer - рисовальщик
			*/
			void Draw(Drawer& drawer) override;

			/*
				Добавление объекта в основную группу
				Base &childToAdd - объект для добавления
			*/
			void Add(std::shared_ptr < Base> childToAdd);

			/*
				Удаление объекта из основной группы
				Base &childToAdd - объект для удаления
			*/
			void Remove(std::shared_ptr < Base> childToRemove);

			/*
				Проверка на то содержит-ли основная группа определенный объект
				Base &childToAdd - объект для проверки нахождения в группе
				return bool - находится ли данный объект в группе
			*/
			bool IsContains(std::shared_ptr < Base> childToFind);

			/*
				Заполнение уровня объектами из tiled-карты
				TiledMap* map - указатель на карту уровня
			*/
			void FillFrom(std::shared_ptr < TiledMap> map);

			/*
				Генерирование Base объекта из TiledObject
				TiledObject* item - указатель на объект карты уровня
				return Base* - указатель на новый объект уровня игры
			*/
			virtual std::shared_ptr < Base> GenerateObject(std::shared_ptr<TiledObject> item);

			/*
				Обработка объектов из карты уровня для которыйх не требуется создание объектов игры
				TiledObject* item - указатель на объект карты уровня
				return bool - true если объект обработан, false если неизвестный тип объекта
			*/
			virtual bool ProcessObject(std::shared_ptr<TiledObject> item);

			/*
				Обработка слоя из карты уровня
				TiledMapItem* item - указатель на объект слоя карты уровня
			*/
			virtual void ProcessMapItem(std::shared_ptr <TiledMapItem> item);

			/*
				Получение указателя на физическое пространство
			*/
			virtual std::shared_ptr<RectangleSpace> GetPhysicsSpace();

			/*
				Получение списка требуемых в сцене ресурсов
			*/
			virtual std::vector<std::shared_ptr<ResourceDescription>> GetRequiredResources();
		};
	}
}