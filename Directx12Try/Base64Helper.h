﻿#pragma once

#include <string>

namespace Tringine
{
	namespace Util
	{
		class Base64Helper
		{
		public:
			Base64Helper();
			~Base64Helper();

			static std::string Encode(unsigned char const*, unsigned int len);
			static std::string Decode(std::string const& s);
		private:
			static const std::string base64_chars;
			static inline bool IsBase64(unsigned char c);
		};
	}
}

