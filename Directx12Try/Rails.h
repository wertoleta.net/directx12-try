﻿#pragma once
#include "Rule.h"
#include "Polyline.h"

using namespace Tringine::Util;

namespace Game
{
	class Rails :
		public Rule
	{
	private:
		std::shared_ptr < Polyline> _polyline;
		int _currentSegmentIndex;
		float _currentSegmentProgress;
		float _passedSegmentsProgress;

	public:
		Rails(std::shared_ptr < TiledObject> object);

		bool IsFinished() override;

		CameraParameters Process(CameraParameters current, float timeScale) override;
	};
}
