﻿#include "pch.h"
#include "BoxGeometry.h"
#include "Common\DirectXHelper.h"
#include "ShaderStructures.h"

using namespace DirectX;
using namespace TringineDx;
using namespace Tringine::Geometry;

unsigned short BoxGeometry::Indices[] =
{
	//-X Side (Left)
	0, 1, 2,
	2, 1, 3,
	//+X Side (Right)
	4, 5, 6,
	7, 6, 5,
	//-Y Side (Top)
	8, 9, 10,
	11, 10, 9,
	//+Y Side (Bottom)
	12, 13, 14,
	15, 14, 13,
	//-Z Size (Back)
	16, 17, 18,
	19, 16, 18,
	//+Z Side (Front)
	22, 21, 20,
	20, 23, 22
};

BoxGeometry::BoxGeometry() :
	GeometryBase(),
	_size(XMFLOAT3(1.0f, 1.0f,1.0f)),
	_origin(XMFLOAT3(0.5f, 0.5f, 0.5f))
{
};

BoxGeometry::BoxGeometry(
	XMFLOAT3 size,
	XMFLOAT3 origin):
	GeometryBase(),
	_size(size),
	_origin(origin) 
{
}

void BoxGeometry::InitInstanceValues()
{
	_indexCountPerInstance = 36;
	_instancesCount = 1;
	_startIndexLocation = 0;
	_baseVertexLocation = 0;
	_startInstanceLocation = 0;
}

void BoxGeometry::Initialize(
	ID3D12Device &device,
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList)
{
	InitInstanceValues();
	float left = -_origin.x;
	float top = -_origin.y;
	float back = -_origin.z;
	float right = -_origin.x + _size.x;
	float bottom = -_origin.y + _size.y;
	float front = -_origin.z + _size.z;
	TexturedVertex cubeVertices[] =
	{
		//-X Side (Left)
		{ XMFLOAT3(left, top, front), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(left, top, back), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(left, bottom, front), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(left, bottom, back),	XMFLOAT2(0.0f, 1.0f) },
		//+X Side (Right)
		{ XMFLOAT3(right, bottom, front), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(right, bottom, back), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(right,  top, front), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(right,  top, back), XMFLOAT2(0.0f, 0.0f) },
		//-Y Side (Top)
		{ XMFLOAT3(left, top, back), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(left, top, front), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(right, top, back), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(right, top, front), XMFLOAT2(1.0f, 0.0f) },
		//+Y Side (Bottom)
		{ XMFLOAT3(left, bottom, front), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(left, bottom, back), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(right, bottom, front), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(right, bottom, back), XMFLOAT2(1.0f, 0.0f) },
		//-Z Side (Back)
		{ XMFLOAT3(left, top, back),	XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(right, top, back), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(right, bottom, back), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(left, bottom, back), XMFLOAT2(0.0f, 1.0f) },
		//+Z Side (Front)
		{ XMFLOAT3(left, top, front), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(right, top, front), XMFLOAT2(1.0f, 1.0f) },
		{ XMFLOAT3(right, bottom, front), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(left, bottom, front), XMFLOAT2(0.0f, 0.0f) },
	};
	CreateVertexBuffer(device, commandsList, reinterpret_cast<BYTE*>(cubeVertices), sizeof(TexturedVertex), sizeof(cubeVertices) / sizeof(TexturedVertex));
	CreateIndexBuffer(device, commandsList, Indices, sizeof(Indices) / sizeof(unsigned short));
}