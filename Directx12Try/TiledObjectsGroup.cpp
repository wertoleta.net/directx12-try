﻿#include "pch.h"
#include "TiledObjectsGroup.h"

namespace Tringine
{
	const std::wstring TiledObjectsGroup::XmlElementName = L"objectgroup";

	TiledObjectsGroup::TiledObjectsGroup(TiXmlElement *element):TiledMapItem(TiledItemType::ObjectGroup, element)
	{
		TiXmlHandle layerHandle(element);
		TiXmlElement* objectElement = layerHandle.FirstChild("object").Element();
		for (objectElement; objectElement; objectElement = objectElement->NextSiblingElement("object"))
		{
			std::shared_ptr < TiledObject> obj = std::make_shared<TiledObject>(objectElement);
			_objects.push_back(obj);
		}
	}

	UINT TiledObjectsGroup::GetObjectsCount()
	{
		return (UINT) _objects.size();
	}

	std::shared_ptr < TiledObject> TiledObjectsGroup::GetObjectAt(UINT index)
	{
		return _objects[index];
	}
}