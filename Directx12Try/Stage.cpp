﻿#include "pch.h"
#include "Stage.h"

namespace Tringine
{
	namespace Entities
	{
		Stage::Stage() :
			_objectsGroupPointer(nullptr),
			_spacePointer(nullptr),
			_mainGroupPointer(nullptr)
		{
		}


		Stage::~Stage()
		{
		}

		void Stage::Create()
		{
			_objectsGroupPointer = std::make_shared<Group>();

			_spacePointer = std::make_shared<RectangleSpace>();
			_spacePointer->Hide();

			_mainGroupPointer = std::make_shared<Group>();

			_mainGroupPointer->Add(_objectsGroupPointer);
			_mainGroupPointer->Add(_spacePointer);
		}

		void Stage::Update(float timeScale)
		{
			Base::Update(timeScale);
			_mainGroupPointer->Update(timeScale);
		}

		void Stage::PostUpdate(Drawer& drawer)
		{
			Base::PostUpdate(drawer);
			_mainGroupPointer->PostUpdate(drawer);
		}

		void Stage::Draw(Drawer& drawer)
		{
			Base::Draw(drawer);
			_mainGroupPointer->Draw(drawer);
		}

		void Stage::Add(std::shared_ptr <Base> childToAdd)
		{
			_objectsGroupPointer->Add(childToAdd);
		}

		void Stage::Remove(std::shared_ptr <Base> childToRemove)
		{
			_objectsGroupPointer->Remove(childToRemove);
		}

		bool Stage::IsContains(std::shared_ptr < Base> childToFind)
		{
			return _objectsGroupPointer->IsContains(childToFind);
		}

		void Stage::FillFrom(std::shared_ptr <TiledMap> map)
		{
			for (int layerIndex = 0; layerIndex < map->GetItems().size(); layerIndex++)
			{
				std::shared_ptr < TiledMapItem> item = map->GetItems()[layerIndex];
				std::shared_ptr < TiledLayer> layer = nullptr;
				std::shared_ptr < TiledObjectsGroup> tiledGroup = nullptr;
				std::shared_ptr < Base> entity = nullptr;
				switch (item->GetType())
				{
				case TiledItemType::TileLayer:
					layer = dynamic_pointer_cast<TiledLayer>(item);
					entity = std::make_shared<TilemapLayerEntity>(map, layer);
					break;
				case TiledItemType::ObjectGroup:
					tiledGroup = dynamic_pointer_cast<TiledObjectsGroup>(item);
					std::shared_ptr <Group> group = std::make_shared<Group>();

					for (UINT i = 0; i < tiledGroup->GetObjectsCount(); i++)
					{
						std::shared_ptr <TiledObject> objdata = tiledGroup->GetObjectAt(i);
						std::shared_ptr <Base> obj = GenerateObject(objdata);
						if (obj != nullptr)
						{
							group->Add(obj);
						}
						else
						{
							if (!ProcessObject(objdata))
							{
								OutputDebugString((L"Unknow object type: " + objdata->GetTypeName() + L"\n").data());
							}
						}
					}
					entity = group;
					break;
				}

				if (entity != nullptr)
				{
					this->Add(entity);
				}
				ProcessMapItem(item);
			}
		}

		std::shared_ptr < Base> Stage::GenerateObject(std::shared_ptr < TiledObject> item)
		{
			return nullptr;
		}

		bool Stage::ProcessObject(std::shared_ptr < TiledObject> item)
		{
			return false;
		}

		void Stage::ProcessMapItem(std::shared_ptr < TiledMapItem> item)
		{

		}

		std::shared_ptr < RectangleSpace> Stage::GetPhysicsSpace()
		{
			return _spacePointer;
		}

		std::vector<std::shared_ptr<ResourceDescription>> Stage::GetRequiredResources()
		{
			return std::vector<std::shared_ptr<ResourceDescription>>();
		}
	}
}