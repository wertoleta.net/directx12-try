// ����� ��������, � ������� �������� ��� ������� ������� � ������� �� �������� ��� ����������� ���������.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	float4x4 mvpMat;
}; 

struct GeometryVertexShaderInput
{
	float3 pos : POSITION;
}; 

struct GeometryPixelShaderInput
{
	float4 pos : SV_POSITION;
};

GeometryPixelShaderInput main(GeometryVertexShaderInput input)
{
	GeometryPixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);
	pos = mul(pos, mvpMat);
	output.pos = pos;
	return output;
}
