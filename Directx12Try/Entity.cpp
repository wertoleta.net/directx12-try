﻿#include "pch.h"
#include "Entity.h"
#include "ShaderStructures.h"
#include "ConstantBuffer.h"
#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"

using namespace DX;
using namespace TringineDx;

namespace Tringine
{
	namespace Entities
	{
		Entity::Entity() :
			_position(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)),
			_rotation(XMFLOAT3(0.0f, 0.0f, 0.0f)),
			_scale(XMFLOAT3(1.0f, 1.0f, 1.0f))
		{
			ConstantBufferIndex = ConstantBuffer::MainInstancePointer->GetNextConstantIndex();
			XMVECTOR posVec = XMLoadFloat4(&_position);
			XMMATRIX tmpMat = XMMatrixTranslationFromVector(posVec);
			DirectX::XMStoreFloat4x4(&RotationMatrix, XMMatrixIdentity());
			DirectX::XMStoreFloat4x4(&LocalMatrix, tmpMat);
			_pipelinePointer = PipelineStateHelper::Get(PipelineStateHelper::MeshGeomentyPipelineStateName);
		}


		Entity::~Entity()
		{
		}

		void Entity::PostUpdate(Drawer& drawer)
		{
			Base::PostUpdate(drawer);
			XMFLOAT4X4 ModelViewProjectionMatrix;
			drawer.Camera->CalculateConstants(LocalMatrix, ModelViewProjectionMatrix);
			DirectX::XMStoreFloat4x4(&ConstantBuffer.mvpMatrix, XMLoadFloat4x4(&ModelViewProjectionMatrix));
			drawer.RootSignatureHelper.ConstantBuffer.StoreConstants(ConstantBufferIndex, ConstantBuffer);
		}

		void Entity::Draw(Drawer& drawer)
		{
			Base::Draw(drawer);
			if (GeometryPointer != nullptr)
			{
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList = drawer.CommandList;
				commandList->SetGraphicsRootConstantBufferView(0, drawer.RootSignatureHelper.ConstantBuffer.GetIndexedGpuVirtualAddress(ConstantBufferIndex));
				commandList->SetPipelineState(_pipelinePointer.Get()); 
				GeometryPointer->Draw(commandList);
			}
		}

		void Entity::SetPosition(float x, float y, float z)
		{
			_position.x = x;
			_position.y = y;
			_position.z = z;
			UpdateMatrixes();
		}

		void Entity::SetPosition(XMFLOAT3 newPosition)
		{
			_position.x = newPosition.x;
			_position.y = newPosition.y;
			_position.z = newPosition.z;
			UpdateMatrixes();
		}

		XMFLOAT3 Entity::GetPosition()
		{
			return XMFLOAT3(_position.x, _position.y, _position.z);
		}

		void Entity::AddRotation(XMFLOAT3 additionalRotation)
		{
			AddRotation(additionalRotation.x, additionalRotation.y, additionalRotation.z);
		}

		void Entity::AddRotation(float x, float y, float z)
		{
			_rotation.x += x;
			_rotation.y += y;
			_rotation.z += z;
			UpdateMatrixes();
		}

		void Entity::SetRotation(float x, float y, float z)
		{
			_rotation.x = x;
			_rotation.y = y;
			_rotation.z = z;
			UpdateMatrixes();
		}

		void Entity::SetScale(float x, float y, float z)
		{
			_scale.x = x;
			_scale.y = y;
			_scale.z = z;
			UpdateMatrixes();
		}

		XMFLOAT3 Entity::GetScale()
		{
			return _scale;
		}

		void Entity::UpdateMatrixes()
		{
			XMMATRIX scaleMatrix = XMMatrixScalingFromVector(XMLoadFloat3(&_scale));

			// create rotation matrices
			XMMATRIX rotXMat = XMMatrixRotationX(_rotation.x);
			XMMATRIX rotYMat = XMMatrixRotationY(_rotation.y);
			XMMATRIX rotZMat = XMMatrixRotationZ(_rotation.z);

			//add rotation to rotation matrix and store it
			XMMATRIX rotMat = rotXMat * rotYMat * rotZMat;
			DirectX::XMStoreFloat4x4(&RotationMatrix, rotMat);

			// create translation matrix from position vector
			XMMATRIX translationMat = XMMatrixTranslationFromVector(XMLoadFloat4(&_position));

			// create world matrix by first rotating, then positioning
			XMMATRIX worldMat = scaleMatrix * rotMat * translationMat;
			//XMMATRIX worldMat = translationMat * rotMat * scaleMatrix;

			//store world matrix
			DirectX::XMStoreFloat4x4(&LocalMatrix, worldMat);
		}
	}
}