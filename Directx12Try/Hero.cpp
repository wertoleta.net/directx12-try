﻿#include "pch.h"
#include "Hero.h"
#include "TrinGame.h"

namespace Game
{
	Hero::Hero(FloatPoint spawnPosition) : Sprite(L"HeroJump"),
		_jumpCounter(0)
	{			
		Revive();
		SetPosition(spawnPosition.X, spawnPosition.Y);

		FloatRectangle rect = FloatRectangle(spawnPosition, FloatSize(32.0f, 72.0f));
		_body = std::make_shared<RectangleBody>(rect, MovableType::Dynamic);
		_body->SetMaxVelocity(10.0f, 10.0f);
		Tringine::TrinGame::Instance->GetStage()->GetPhysicsSpace()->AddBody(_body);
	}


	Hero::~Hero()
	{
	}

	void Hero::Update(float timeScale)
	{
		Sprite::Update(timeScale);
		std::shared_ptr < InputManager> input = Tringine::TrinGame::Instance->GetInput();

		FloatPoint bodyPosition = _body->GetCurrentPosition();
		FloatSize size = _body->GetSize();
		SetPosition(XMFLOAT3(bodyPosition.X + size.Width / 2.0f, bodyPosition.Y + size.Height, 0.0f));

		if (_body->IsSideCollided(Side::Bottom))
		{
			_jumpCounter = MaxJumpsCounter;
		}

		std::shared_ptr < Action>leftAction = input->GetAction(L"Left");
		std::shared_ptr < Action>rightAction = input->GetAction(L"Right");
		if (!leftAction->IsActive() && !rightAction->IsActive())
		{
			_lastPressedDirection = 0;
		}
		else if (leftAction->IsFired())
		{
			_lastPressedDirection = -1;
		}
		else if (rightAction->IsFired())
		{
			_lastPressedDirection = 1;
		}

		FloatPoint currentVelocity = _body->GetVelocity();
		if (_lastPressedDirection == 0)
		{
			SwitchAnimation(L"HeroIdle");
			if (abs(currentVelocity.X) < 0.1f)
			{
				_body->SetVelocity(0.0f, currentVelocity.Y);
			}
			else
			{
				float step = fmax(abs(currentVelocity.X) / 8.0f, 0.1f);
				_body->AddVelocity(-copysign(step * timeScale, currentVelocity.X), 0.0f);
			}
		}
		else
		{
			SwitchAnimation(L"HeroJump");
			float maxControlledVelocity = (float) copysign(_body->GetMaxVelocity().X / 2.0f, _lastPressedDirection);
			float k = fmax(abs(maxControlledVelocity - currentVelocity.X), 0.0f) / abs(maxControlledVelocity);
			if (!_body->IsSideCollided(Side::Bottom))
			{
				k /= 10.0f;
			}
			_body->AddVelocity(k * _lastPressedDirection * timeScale, 0.0f);
		}

		if (_lastPressedDirection != 0)
		{
			SetScale((float) -_lastPressedDirection, 1.0f, 1.0f);
		}

		if (_jumpCounter > 0)
		{
			if (input->GetAction(L"Up")->IsActive())
			{
				_body->AddVelocity(0.0f, -8.0f * timeScale);
			}
			_jumpCounter = max(0, _jumpCounter--);
		}
	}

	void Hero::Draw(Drawer &drawer)
	{
		Sprite::Draw(drawer);
	}
}