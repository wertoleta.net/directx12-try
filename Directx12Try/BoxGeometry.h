﻿#pragma once

#include "Common\DeviceResources.h"
#include "ShaderStructures.h"
#include "GeometryBase.h"

using namespace DirectX;
using namespace TringineDx;

namespace Tringine
{
	namespace Geometry
	{
		class BoxGeometry : public GeometryBase
		{
		public:
			//Базовый конструктор. Геометрия коробки размером 1х1х1 с точкой вращения в центре
			BoxGeometry();

			/*
				Конструктор для геометрии коробки с произвольным размером
				XMFLOAT3 size - размеры коробки
				XMFLOAT3 origin - центр вращения
			*/
			BoxGeometry(
				XMFLOAT3 size,
				XMFLOAT3 origin);

			/*
				Инициализация ресурсов геометрии коробки
				ID3D12Device &device - устройство для которого инициализируется ресурс
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавляются команды на инициализацию ресурса
			*/
			void Initialize(
				ID3D12Device& device,
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList) override;
		protected:
			void InitInstanceValues() override;
		private:
			//Инексы вершин коробки
			static unsigned short Indices[];

			//Размер коробки
			XMFLOAT3 _size;
			//Центр вращения коробки
			XMFLOAT3 _origin;
		};
	}
}