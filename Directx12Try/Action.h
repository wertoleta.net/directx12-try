﻿#pragma once

#include <map>

namespace Tringine
{
	namespace Input
	{

		enum ActionState
		{
			ASNone,
			ASFired,
			ASActive,
			ASEnds
		};

		class Action
		{
			typedef std::map<std::wstring, std::shared_ptr<Action>> StringToActionMap;

		public:
			Action();

			void Update();
			void Activate();
			void Deactivate();

			bool IsFired();
			bool IsActive();
			bool IsEnds();

		private:
			std::wstring _name;
			int _activationsCount;;
			ActionState _state;
			ActionState _nextState;
		};
	}
}

