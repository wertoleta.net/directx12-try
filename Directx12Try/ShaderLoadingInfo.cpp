﻿#include "pch.h"
#include "ShaderLoadingInfo.h"

namespace TringineDx
{
	ShaderLoadingInfo::ShaderLoadingInfo(
		std::wstring path,
		std::vector<byte> *dataPointer,
		Concurrency::task< void > *taskPointer)
	{
		this->DataPointer = dataPointer;
		_path = path;
		_taskPointer = taskPointer;
	}

	std::wstring& ShaderLoadingInfo::GetPath()
	{
		return _path;
	}

	void ShaderLoadingInfo::SetTask(Concurrency::task<void> &task)
	{
		*_taskPointer = task;
	}

	void ShaderLoadingInfo::LoadShadersAsynch(
		std::vector<TringineDx::ShaderLoadingInfo>& shadersInfo)
	{
		for (size_t i = 0; i < shadersInfo.size(); i++)
		{
			TringineDx::ShaderLoadingInfo info = shadersInfo[i];
			Concurrency::task<void> task = DX::ReadDataAsync(info.GetPath())
				.then([info](std::vector<byte>& fileData)
			{
				std::vector<byte>* currentData = info.DataPointer;
				*currentData = fileData;
			});
			info.SetTask(task);
		}
	}
}