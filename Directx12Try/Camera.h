﻿#pragma once

#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"
#include "RootSignatureHelper.h"

using namespace DX;
using namespace DirectX;
using namespace TringineDx;

namespace Tringine
{
	/*
		Класс игровой камеры
	*/
	class Camera
	{
	private:
		//Указывает на то является ли проекция ортогональной или нет
		bool _isOrtho;

		FloatSize _viewportSize;
		FloatSize _minSize;
		FloatSize _maxSize;

		//Матрица положения в мировых координатах
		XMFLOAT4 _worldPosition;

		//Матрица цели - куда смотреть в мировых координатах
		XMFLOAT4 _worldTarget;
	public:
		//Базовый конструктор
		Camera();

		//Базовый деструктор
		~Camera();

		//Зона обрезки вьюпорта
		D3D12_RECT ScissorRect;

		//Матрица проекции
		XMFLOAT4X4 ProjectionMatrix;

		//Матрица вида
		XMFLOAT4X4 CameraViewMatrix;

		//Положение в мировых координатах
		XMFLOAT4 Position;

		//Цель куда смотреть в мировых координатах
		XMFLOAT4 Target;

		//Куда направлен верх камеры
		XMFLOAT4 Up;

		//Размер вьюпорта
		FloatSize Size;

		//Установить вид проекций
		void SetProjection(float aspectRatio, float fovY);

		//Установить ортогональный вид
		void SetOrtho(float width, float height);

		//Установить ортогональный вид
		void SetOrtho(FloatSize size);

		//Установка вьюпорта
		void SetViewport(D3D12_VIEWPORT &viewport);

		//Рассчитать значения сущности для буфера констант
		void CalculateConstants(const XMFLOAT4X4 &entityLocalMat, XMFLOAT4X4 &entityWorldMat);

		//Сброс
		void Reset();

		//Установка расположения
		void SetPosition(float x, float y, float z);

		//Установка расположения
		void SetPosition(float x, float y);

		//Установка расположения
		void SetOrthoCenterPosition(float x, float y);
	};
}

