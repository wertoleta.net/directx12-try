﻿#include "pch.h"
#include "ConstantBuffer.h"
#include "Common\DirectXHelper.h"

using namespace TringineDx;

ConstantBuffer *ConstantBuffer::MainInstancePointer = nullptr;

ConstantBuffer::~ConstantBuffer()
{
	for (int frameIndex = 0; frameIndex < DX::c_frameCount; frameIndex++) 
	{
		_buffer[frameIndex]->Unmap(0, nullptr);
		_mappedBuffer[frameIndex] = nullptr;
	}
	_nextConstantIndex = 0;
}

void ConstantBuffer::Init(ID3D12Device &device)
{
	//Настройки по-умолчанию для буфера констант
	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	CD3DX12_RESOURCE_DESC constantBufferDescription = CD3DX12_RESOURCE_DESC::Buffer(TotalSize);
	CD3DX12_RANGE readRange(0, 0);
	ModelViewProjectionConstantBuffer emptyTempBuffer;
	ZeroMemory(&emptyTempBuffer, sizeof(emptyTempBuffer));

	//Создание буфера констант для каждого кадра тройного буфера
	for (int frameIndex = 0; frameIndex < DX::c_frameCount; frameIndex++) {
		DX::ThrowIfFailed(device.CreateCommittedResource(
			&uploadHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&constantBufferDescription,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&_buffer[frameIndex])));

		NAME_D3D12_OBJECT(_buffer[frameIndex]);

		DX::ThrowIfFailed(_buffer[frameIndex]->Map(0, &readRange, reinterpret_cast<void**>(&_mappedBuffer[frameIndex])));

		ZeroMemory(_mappedBuffer[frameIndex], TotalSize);

		for (UINT shift = 0; shift < TotalSize; shift += AlignedSize)
		{
			memcpy(_mappedBuffer[frameIndex] + shift, &emptyTempBuffer, sizeof(emptyTempBuffer));
		}
	}

	if (ConstantBuffer::MainInstancePointer == nullptr)
	{
		ConstantBuffer::MainInstancePointer = this;
	}
}

void ConstantBuffer::StoreConstants(int entityIndex, ModelViewProjectionConstantBuffer &mvpBuffer)
{
	memcpy(_currentFrameMapped + AlignedSize * entityIndex, &mvpBuffer, sizeof(mvpBuffer));
}

UINT ConstantBuffer::GetNextConstantIndex()
{
	UINT toReturn = _nextConstantIndex;
	if (toReturn * AlignedSize >= TotalSize)
	{
		throw std::exception("Oversized constant amount");
	}
	_nextConstantIndex++;
	return toReturn;
}

void ConstantBuffer::ResetConstantIndexes()
{
	_nextConstantIndex = 0;
}

void ConstantBuffer::SetCurrentFrameIndex(int frameIndex)
{
	_currentFrameGpuVirtualAddress = _buffer[frameIndex]->GetGPUVirtualAddress();
	_currentFrameMapped = _mappedBuffer[frameIndex];
}

D3D12_GPU_VIRTUAL_ADDRESS ConstantBuffer::GetIndexedGpuVirtualAddress(int entityIndex)
{
	return _currentFrameGpuVirtualAddress + AlignedSize * entityIndex;
}