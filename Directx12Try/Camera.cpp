﻿#include "pch.h"
#include "Camera.h"
#include "Structures.h"
#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"
#include "RootSignatureHelper.h"

using namespace DX;
using namespace TringineDx;

namespace Tringine
{
	Camera::Camera() :
		Position(XMFLOAT4(0.0f, 0.0f, 500.0f, 0.0f)),
		Target(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)),
		Up(XMFLOAT4(0.0f, -1.0f, 0.0f, 0.0f)),
		_isOrtho(false),
		_minSize(100.0f, 100.0f),
		_maxSize(3840.0f, 2160.0f)
	{
		SetProjection(16.0f / 9.0f, 45.0f);
	}


	Camera::~Camera()
	{
	}

	void Camera::SetProjection(float aspectRatio, float fovY)
	{
		_isOrtho = false;
		fovY = fovY * XM_PI / 180.0f;
		XMMATRIX tmpMat = XMMatrixPerspectiveFovLH(fovY, aspectRatio, 0.1f, 1000.0f);

		DirectX::XMStoreFloat4x4(&ProjectionMatrix, tmpMat);

		Size.Width = (float)(ScissorRect.right - ScissorRect.left);
		Size.Height = (float)(ScissorRect.bottom - ScissorRect.top);
	}

	void Camera::SetOrtho(float width, float height)
	{
		_isOrtho = true;

		Size.Width = fmax(fmin(_maxSize.Width, width), _minSize.Width);
		Size.Height = fmax(fmin(_maxSize.Height, height), _minSize.Height);

		XMMATRIX tmpMat = XMMatrixOrthographicLH(Size.Width, Size.Height, 0.1f, 1000.0f);
		DirectX::XMStoreFloat4x4(&ProjectionMatrix, tmpMat);
	}

	void Camera::SetOrtho(FloatSize size)
	{
		SetOrtho(size.Width, size.Height);
	}

	void Camera::SetViewport(D3D12_VIEWPORT &viewport)
	{
		_viewportSize.Width = viewport.Width;
		_viewportSize.Height = viewport.Height;
		ScissorRect = { 0, 0, static_cast<LONG>(viewport.Width), static_cast<LONG>(viewport.Height) };
	}

	void Camera::Reset()
	{
		_worldPosition = Position;
		_worldPosition.x += Size.Width / 2.0f;
		_worldPosition.y += Size.Height / 2.0f;
		_worldTarget = Target;
		if (_isOrtho)
		{
			_worldTarget.x = _worldPosition.x;
			_worldTarget.y = _worldPosition.y;
		}
		XMVECTOR cPos = XMLoadFloat4(&_worldPosition);
		XMVECTOR cTarg = XMLoadFloat4(&_worldTarget);
		XMVECTOR cUp = XMLoadFloat4(&Up);
		XMMATRIX tmpMat = XMMatrixLookAtLH(cPos, cTarg, cUp);
		DirectX::XMStoreFloat4x4(&CameraViewMatrix, tmpMat);
	}

	void Camera::CalculateConstants(const XMFLOAT4X4 &entityLocalMat, XMFLOAT4X4 &entityWorldMat)
	{
		XMMATRIX viewMat = XMLoadFloat4x4(&CameraViewMatrix); // load view matrix
		XMMATRIX projMat = XMLoadFloat4x4(&ProjectionMatrix); // load projection matrix
		XMMATRIX wvpMat = XMLoadFloat4x4(&entityLocalMat) * viewMat * projMat; // create wvp matrix
		XMMATRIX transposed = XMMatrixTranspose(wvpMat); // must transpose wvp matrix for the gpu
		DirectX::XMStoreFloat4x4(&entityWorldMat, transposed); // store transposed wvp matrix in constant buffer
	}

	void Camera::SetPosition(float x, float y, float z)
	{
		Position.x = x;
		Position.y = y;
		Position.z = z;
	}

	void Camera::SetPosition(float x, float y)
	{
		SetPosition(x, y, Position.z);
	}

	void Camera::SetOrthoCenterPosition(float x, float y)
	{
		SetPosition(x - Size.Width / 2.0f, y - Size.Height / 2.0f, Position.z);
	}
}