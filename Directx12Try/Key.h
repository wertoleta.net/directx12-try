﻿#pragma once

#include <map>
#include "Action.h"

namespace Tringine
{
	namespace Input
	{
		enum KeyState
		{
			KSNone,
			KSUp,
			KSPressed,
			KSDown,
			KSReleased
		};

		class Key
		{
		public:
			Key(int code);
			~Key();

			void Update();
			void Press();
			void Release();
			bool IsDown();
			bool IsUp();
			bool IsPressed();
			bool IsReleased();

			void BindAction(std::shared_ptr < Action> action);
			void UnbindAction(std::shared_ptr < Action> action);
		private:
			int _code;
			KeyState _state;
			KeyState _nextState;
			std::vector< std::shared_ptr<Action>> _bindedActions;
		};
	}
}

