﻿#pragma once

#include "tinyxml.h"

namespace Tringine
{
	namespace Util
	{
		class XmlHelper
		{
		public:
			XmlHelper();
			~XmlHelper();

			static std::string GetStringAttributeValue(TiXmlElement* xml, char* attributeName, std::string defaultValue);
			static std::wstring GetWStringAttributeValue(TiXmlElement* xml, char* attributeName, std::wstring defaultValue);
			static int GetIntAttributeValue(TiXmlElement* xml, char* attributeName, int defaultValue);
			static float GetFloatAttributeValue(TiXmlElement* xml, char* attributeName, float defaultValue);
		};
	}
}

