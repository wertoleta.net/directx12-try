﻿#include "pch.h"
#include "TiledMapItem.h"

namespace Tringine
{
	TiledMapItem::TiledMapItem(TiledItemType type, TiXmlElement* element): TiledEntry(element),
		_type(type)
	{
	}

	TiledItemType TiledMapItem::GetType()
	{
		return _type;
	}

	void TiledMapItem::MakeThisClassVirtual() {};
}