﻿#pragma once

#include "Common/DirectXHelper.h"
#include <map>

using namespace DX;

//Тип соответствия очереди на очистку - фенсу

namespace TringineDx
{
	class DynamicResource;
	typedef std::vector<std::shared_ptr<DynamicResource>> DynamicResourceList;

	class DynamicResource
	{
	public:
		//Базовый конструктор
		DynamicResource();

		//Базовый деструктор
		~DynamicResource();

		/*
			Инициализация ресурса
			ID3D12Device &device - устройство для которого инициализируется ресурс
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавляются команды на инициализацию ресурса
		*/
		virtual void Initialize(
			ID3D12Device &device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList);

		//Очистка ресурса от временных и/или вспомогательных элементов не нужных после инициализации
		virtual void Cleanup();

		bool IsUsed();
		bool IsCanBeDeleted();

		void SetLastDrawFence(Microsoft::WRL::ComPtr < ID3D12Fence> fence);

		/*
			Инициализация всех ресурсов игры созданых на этом шаге игры
			ID3D12Device &device - устройство для которого инициализируются ресурсы
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавляются команды на инициализацию ресурсов
		*/
		static Microsoft::WRL::ComPtr < ID3D12Fence> InitializeAllNew(
			ID3D12Device& device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList);

		static void GarbageCollect(Microsoft::WRL::ComPtr < ID3D12Fence> fence);

		//Очистка всех ресурсов игры инициализированных на этом шаге игры от временных и/или вспомогательных элементов не нужных после инициализации
		static void CleanupAllInitialized();

		void ResetUse();

		static void AddCreatedResource(std::shared_ptr<DynamicResource> resource);
	protected:
		void Use();
	private:
		typedef std::map< Microsoft::WRL::ComPtr<ID3D12Fence>, DynamicResourceList*> CleanupQueueToFence;

		//Очередь на инициализацию из ресурсов созданных на этом шаге игры
		static DynamicResourceList CreateQueue;

		//Очередь на очистку из ресурсов инициализированных на этом шаге игры
		static CleanupQueueToFence CleanupQueue;

		static DynamicResourceList AllResources;

		bool _isUsed;

		Microsoft::WRL::ComPtr < ID3D12Fence> _lastDrawFence;
	};


	template <class T, class... _Types>
	std::shared_ptr<T> MakeDynamicResource(_Types&&... _Args) {
		//if (!std::is_base_of<DynamicResource, T>::value)
		//{
		//}
		//std::shared_ptr<DynamicResource> dr = std::make_shared<T>(_Args...);
		//std::shared_ptr<DynamicResource> dr = std::make_shared<T>(_Args...);
 		std::shared_ptr<T> _Ret = std::make_shared<T>(_Args...);
		std::shared_ptr<DynamicResource> dr = std::dynamic_pointer_cast<DynamicResource>(_Ret);
		if (dr == nullptr)
		{
			throw std::exception("Wrong class for MakeDynamicResource");
		}
		DynamicResource::AddCreatedResource(dr);
		return _Ret;
	}
}
