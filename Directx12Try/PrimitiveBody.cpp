﻿#include "pch.h"
#include "PrimitiveBody.h"

const FloatPoint DefaultSpawnPosition(0.0f, 0.0f);
const FloatPoint DefaultVelocity(0.0f, 0.0f);
const FloatPoint DefaultMaxVelocity(10.0f, 10.0f);
const float DefaultWeight = 0.0f;

namespace Tringine
{
	namespace Physics
	{
		PrimitiveBody::PrimitiveBody() :
			_type(MovableType::Static),
			_position(DefaultSpawnPosition),
			_weight(DefaultWeight),
			_velocity(DefaultVelocity),
			_maxVelocity(DefaultMaxVelocity)
		{

		}

		PrimitiveBody::PrimitiveBody(MovableType type) :
			_type(type),
			_position(DefaultSpawnPosition),
			_weight(DefaultWeight),
			_velocity(DefaultVelocity),
			_maxVelocity(DefaultMaxVelocity)
		{

		}

		PrimitiveBody::PrimitiveBody(MovableType type, FloatPoint spawnPoint) :
			_type(type),
			_position(spawnPoint),
			_weight(DefaultWeight),
			_velocity(DefaultVelocity),
			_maxVelocity(DefaultMaxVelocity)
		{

		}

		MovableType PrimitiveBody::GetType()
		{
			return _type;
		}

		float PrimitiveBody::GetWeight()
		{
			return _weight;
		}

		FloatPoint PrimitiveBody::GetCurrentPosition()
		{
			return _position.GetCurrent();
		}

		FloatPoint PrimitiveBody::GetPreviousPosition()
		{
			return _position.GetPrevious();
		}

		void PrimitiveBody::MoveTo(FloatPoint newPosition)
		{
			_position.MoveTo(newPosition);
		}

		void PrimitiveBody::MoveOn(FloatPoint additionalShift)
		{
			this->MoveOn(additionalShift.X, additionalShift.Y);
		}

		void PrimitiveBody::MoveOn(float additionalX, float additionalY)
		{
			FloatPoint current = _position.GetCurrent();
			this->MoveTo(FloatPoint(current.X + additionalX, current.Y + additionalY));
		}

		bool PrimitiveBody::IsCollide(std::shared_ptr < PrimitiveBody> anotherBody)
		{ 
			return false; 
		};

		FloatPoint PrimitiveBody::GetVelocity()
		{
			return _velocity;
		}

		void PrimitiveBody::SetVelocity(float x, float y)
		{
			_velocity.X = copysignf(fminf(abs(x), _maxVelocity.X), x);
			_velocity.Y = copysignf(fminf(abs(y), _maxVelocity.Y), y);
		}

		void PrimitiveBody::AddVelocity(float x, float y)
		{
			SetVelocity(_velocity.X + x, _velocity.Y + y);
		}

		FloatPoint PrimitiveBody::GetMaxVelocity()
		{
			return _maxVelocity;
		}

		void PrimitiveBody::SetMaxVelocity(float x, float y)
		{
			_maxVelocity.X = abs(x);
			_maxVelocity.Y = abs(y);
		}

		void PrimitiveBody::ApplyVelocity(float timeScale)
		{
			MoveOn(_velocity.X * timeScale, _velocity.Y * timeScale);
		}
	}
}