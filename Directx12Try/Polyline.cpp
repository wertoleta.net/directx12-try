﻿#include "pch.h"
#include "Polyline.h"

namespace Tringine
{
	namespace Util
	{
		Polyline::Polyline(TiXmlElement* element)
		{
			std::wstring pointsString = XmlHelper::GetWStringAttributeValue(element, "points", L"");
			std::vector<std::wstring> parts = StringHelper::Split(pointsString, L' ');
			for (int i = 0; i < parts.size(); i++)
			{
				std::wstring part = parts[i];
				std::vector<std::wstring> numbers = StringHelper::Split(part, L',');
				FloatPoint newPoint(std::stof(numbers[0]), std::stof(numbers[1]));
				_points.push_back(newPoint);
				if (i > 0)
				{
					_totalLength += FloatSegment(_points[i - 1], _points[i]).Length();
				}
			}
		}

		Polyline::Polyline(TiXmlElement* element, FloatPoint origin):Polyline(element)
		{
			for (int pointIndex = 0; pointIndex < _points.size(); pointIndex++)
			{
				_points[pointIndex].X += origin.X;
				_points[pointIndex].Y += origin.Y;
			}
		}

		int Polyline::GetPointsCount()
		{
			return (int) _points.size();
		}

		int Polyline::GetSegmentsCount() 
		{
			return (int) fmax(_points.size() - 1, 0);
		}

		float Polyline::GetLength()
		{
			return _totalLength;
		}

		FloatPoint Polyline::GetPointAt(int index)
		{
			if (index < 0 || index >= _points.size())
			{
				throw exception("Argument out of bounds");
			}
			return _points[index];
		}

		FloatSegment Polyline::GetSegmentAt(int index)
		{
			if (index < 0 || index >= _points.size() - 1)
			{
				throw exception("Argument out of bounds");
			}
			return FloatSegment(_points[index], _points[index + 1]);
		}
	}
}