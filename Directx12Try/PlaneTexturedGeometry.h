﻿#pragma once
#include "GeometryBase.h"

namespace Tringine
{
	namespace Geometry
	{
		class PlaneTexturedGeometry :
			public GeometryBase
		{
		public:
			PlaneTexturedGeometry();
			PlaneTexturedGeometry(XMFLOAT2 size, XMFLOAT2 origin);
			PlaneTexturedGeometry(XMFLOAT2 size, XMFLOAT2 origin, XMFLOAT4 textureSides);
			~PlaneTexturedGeometry();

			/*
				»нициализаци¤ ресурсов геометрии плоскости
				ID3D12Device &device - устройство дл¤ которого инициализируетс¤ ресурс
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавл¤ютс¤ команды на инициализацию ресурса
			*/
			void Initialize(
				ID3D12Device& device,
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList) override;
		protected:
			void InitInstanceValues() override;
		private:
			//»нексы вершин коробки
			static unsigned short Indices[];

			//–азмер коробки
			XMFLOAT2 _size;
			//÷ентр вращени¤ коробки
			XMFLOAT2 _origin;
			//√раницы на текстуре
			XMFLOAT4 _textureSides;
		};
	}
}