﻿#pragma once

#include "Types.h"
#include "tinyxml.h"
#include "XmlHelper.h"

using namespace Tringine::Util;

namespace Tringine
{
	class TiledEntry
	{
	private:
		std::wstring _name;
		StringToStringMap _properties;

	public:
		TiledEntry(TiXmlElement* element);

		std::wstring GetName();
		std::wstring GetProperty(std::wstring name);
		std::wstring GetProperty(std::wstring name, std::wstring defaultValue);
	};
}

