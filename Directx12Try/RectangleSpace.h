﻿#pragma once

#include "Base.h"
#include "RectangleBody.h"
#include "Entity.h"
#include "Group.h"
#include "GeometryBase.h"
#include "PlaneGeometry.h"

using namespace Tringine::Entities;
using namespace Tringine::Entities::Shapes;
using namespace Tringine::Geometry;

namespace Tringine
{
	namespace Physics
	{
		typedef std::vector< std::shared_ptr<RectangleBody>> BodyList;

		class RectangleSpace: public Group
		{
		private:
			BodyList _bodies;

			void ResetBodiesStatuses(float timeScale);
			void ApplyGravity(float timeScale);
			void CalculateFullSolidCollisions(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody);
			void CalculatePartitialSolidCollisions(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody);
			void CalculateTouches(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody);
			float GetMassCoefficient(std::shared_ptr < RectangleBody> firstBody, std::shared_ptr < RectangleBody> secondBody);

		public:
			RectangleSpace();
			~RectangleSpace();

			void Update(float timeScale) override;
			void AddBody(std::shared_ptr < RectangleBody> body);
			BodyList GetAllOverlaped(std::shared_ptr<RectangleBody> body);
		};
	}
}
