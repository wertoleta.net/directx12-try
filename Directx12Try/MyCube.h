﻿#pragma once
#include "TexturedEntity.h"
#include "Types.h"

using namespace Tringine::Entities;

namespace Game
{
	class MyCube :
		public TexturedEntity
	{
	private:
		XMFLOAT3 _rotationVelocity;
	public:
		MyCube(FloatRectangle area);
		~MyCube();

		void Update(float timeScale) override;
		void Draw(Drawer  &drawer) override;
	};
}
