﻿#pragma once

#include "Structures.h"

using namespace std;
using namespace Tringine;
using namespace TringineDx;

namespace Tringine
{
	struct IntegerRectangle
	{
		IntegerPoint Position;
		IntegerSize Size;
	};

	//Дробный размер
	struct FloatSize
	{
		float Width;
		float Height;

		FloatSize() {}
		FloatSize(float width, float height)
		{
			Width = width;
			Height = height;
		}

		float GetArea()
		{
			return abs(Width) * abs(Height);
		}

		FloatSize operator+(FloatSize const& right)
		{
			return FloatSize(Width + right.Width, Height + right.Height);
		}

		FloatSize operator-(const FloatSize& right)
		{
			return FloatSize(Width - right.Width, Height - right.Height);
		}

		FloatSize operator*(const float& right)
		{
			return FloatSize(Width * right, Height * right);
		}
	};

	struct FloatSegment
	{
		FloatPoint Start;
		FloatPoint End;

		FloatSegment() {}
		FloatSegment(FloatPoint start, FloatPoint end):
		Start(start),
		End(end)
		{
		}

		float DistanceToPoint(FloatPoint p)
		{
			float top = (End.Y - Start.Y) * p.X - (End.X - Start.X) * p.Y + End.X * Start.Y - End.Y * Start.X;
			float bot = Length();
			return top / bot;
		}

		float Length()
		{
			return sqrtf(powf(End.X - Start.X, 2.0) + powf(End.Y - Start.Y, 2.0));
		}

		FloatPoint NearestPointTo(FloatPoint p)
		{
			float StartToEndProgress = ProgressOfNearestPointTo(p);
			return PointAtProgress(StartToEndProgress);
		}

		FloatPoint PointAtProgress(float progress)
		{
			FloatPoint StartToEnd = FloatPoint(End.X - Start.X, End.Y - Start.Y);
			return FloatPoint(Start.X + StartToEnd.X * progress, Start.Y + StartToEnd.Y * progress);
		}

		float ProgressOfNearestPointTo(FloatPoint p)
		{
			FloatPoint StartToP = FloatPoint(p.X - Start.X, p.Y - Start.Y);
			FloatPoint StartToEnd = FloatPoint(End.X - Start.X, End.Y - Start.Y);
			float StartToEndDistanceSquared = powf(StartToEnd.X, 2.0f) + powf(StartToEnd.Y, 2.0f);
			float StartToPointDotStartToEnd = StartToP.X * StartToEnd.X + StartToP.Y * StartToEnd.Y;
			return StartToPointDotStartToEnd / StartToEndDistanceSquared;
		}
	};

	struct FloatRectangle
	{
		FloatPoint Position;
		FloatSize Size;

		FloatRectangle() :
			Position(0.0f, 0.0f),
			Size(1.0f, 1.0f)
		{
		}

		FloatRectangle(FloatPoint position, FloatSize size) :
			Position(position),
			Size(size)
		{
		}

		FloatRectangle(IntegerRectangle integerRectangle) :
			Position((float) integerRectangle.Position.X, (float) integerRectangle.Position.Y),
			Size((float) integerRectangle.Size.Width, (float) integerRectangle.Size.Height)
		{
		}
	};

	//Размер чего-либо в пикселях
	typedef IntegerSize PixelSize;

	//Размер текстуры
	typedef IntegerSize TextureSize;

	//Положение относительно текстуры
	typedef TringineDx::FloatPoint TextureRelativePosition;

	//Размер относительно текстуры
	typedef Size TextureRelativeSize;

	//Тип соответствия пайплайна его имени
	typedef std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D12PipelineState>> StringToPipelineStateMap;

	//Тип соответствия строки строке
	typedef std::map<std::wstring, std::wstring> StringToStringMap;
}