﻿#pragma once
#include "Stage.h"
#include "Label.h"
#include "ResourceManager.h"
#include "RectangleBody.h"
#include "Rectangle.h"
#include "CameraManager.h"
#include "Rails.h"
#include "ResourceDescription.h"

using namespace Tringine::Entities;
using namespace Tringine::Entities::Shapes;
using namespace Tringine::Physics;

namespace Game
{
	class MyStage :
		public Stage
	{
	public:
		MyStage();
		~MyStage();

		void Create() override;
		void Update(float timeScale) override;
		std::shared_ptr < Base> GenerateObject(std::shared_ptr < TiledObject> item) override;
		bool ProcessObject(std::shared_ptr < TiledObject> item) override;
		void ProcessMapItem(std::shared_ptr < TiledMapItem> item) override;
		std::vector< std::shared_ptr<ResourceDescription>> GetRequiredResources() override;

	private:
		std::shared_ptr < Label>_fpsText;
		std::shared_ptr < CameraManager> _cameraManager;
	};
}
