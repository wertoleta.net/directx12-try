﻿#include "pch.h"
#include "Base.h"


namespace Tringine
{
	namespace Entities
	{
		Base::Base() :
			_isExists(false),
			_isActive(false),
			_isVisible(true),
			ParentPointer(nullptr)
		{
		}


		Base::~Base()
		{
		}

		void Base::Update(float timeScale) {}
		void Base::PostUpdate(Drawer& drawer) {}
		void Base::Draw(Drawer& drawer) {}

		//Getters and Setters
		bool Base::IsActive()
		{
			return _isActive && _isExists;
		}

		bool Base::IsExists() {
			return _isExists;
		}

		bool Base::IsVisible() {
			return _isVisible && _isExists;
		}

		Base* Base::GetParentPointer() {
			return ParentPointer;
		}

		void Base::Enable() {
			_isActive = true;
		}

		void Base::Disable() {
			_isActive = false;
		}

		void Base::Hide()
		{
			_isVisible = false;
		}

		void Base::Show()
		{
			_isVisible = true;
		}

		void Base::Kill() {
			Disable();
			_isExists = false;
		}

		void Base::Revive() {
			Enable();
			_isExists = true;
		}

		void Base::SetParent(Base* parentPointer) {
			ParentPointer = parentPointer;
		}
	}
}