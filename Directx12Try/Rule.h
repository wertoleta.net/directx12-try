﻿#pragma once
#include "Types.h"
#include "TiledObject.h"

namespace Game
{
	struct CameraParameters
	{
		FloatPoint CenterPosition;
		FloatSize Size;

		CameraParameters(FloatPoint position, FloatSize size):
			CenterPosition(position),
			Size(size)
		{

		}

		CameraParameters() :CameraParameters(FloatPoint(0.0f, 0.0f), FloatSize(1920.0f, 1920.0f))
		{

		}
	};

	class Rule
	{
	private:
		std::wstring _name;
		std::wstring _nextName;

	protected:
		FloatSize _startViewportSize;
		FloatSize _endViewportSize;
	
	public:
		Rule(std::shared_ptr < TiledObject> object);

		std::wstring GetName();
		std::wstring GetNextName();
		
		virtual bool IsFinished();
		virtual CameraParameters Process(CameraParameters current, float timeScale);
		void SetStartViewportSize(FloatSize newSize);
		FloatSize GetEndViewportSize();
	};
}

