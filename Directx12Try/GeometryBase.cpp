﻿#include "pch.h"
#include "GeometryBase.h"
#include "Common\DirectXHelper.h"

using namespace DirectX;
using namespace Tringine::Geometry;

GeometryBase::GeometryBase() :
	_topology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
{
};

GeometryBase::~GeometryBase() 
{
	_vertexBuffer.Reset();
	_indexBuffer.Reset();
	_vertexUploadBuffer.Reset();
	_indexUploadBuffer.Reset();
};

void GeometryBase::Draw(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandList) 
{
	Use();
	commandList->IASetPrimitiveTopology(_topology);
	commandList->IASetVertexBuffers(0, 1, &VertexBufferView);
	commandList->IASetIndexBuffer(&IndexBufferView);
	commandList->DrawIndexedInstanced(_indexCountPerInstance, _instancesCount, _startIndexLocation, _baseVertexLocation, _startInstanceLocation);
}

void GeometryBase::CreateVertexBuffer(
	ID3D12Device &device,
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList,
	BYTE* cubeVerticesPointer,
	UINT verticeSize,
	UINT verticesCount)
{
	const UINT64 vertexBufferSize = (UINT64)  verticeSize * verticesCount;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(vertexBufferSize);
	DX::ThrowIfFailed(device.CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&_vertexBuffer)));

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(device.CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&vertexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_vertexUploadBuffer)));

	NAME_D3D12_OBJECT(_vertexBuffer);

	D3D12_SUBRESOURCE_DATA _vertexBufferData;
	_vertexBufferData.pData = cubeVerticesPointer;
	_vertexBufferData.RowPitch = vertexBufferSize;
	_vertexBufferData.SlicePitch = _vertexBufferData.RowPitch;

	UpdateSubresources(commandsList.Get(), _vertexBuffer.Get(), _vertexUploadBuffer.Get(), 0, 0, 1, &_vertexBufferData);

	CD3DX12_RESOURCE_BARRIER vertexBufferBarier;
	vertexBufferBarier =
		CD3DX12_RESOURCE_BARRIER::Transition(_vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	commandsList->ResourceBarrier(1, &vertexBufferBarier);

	VertexBufferView.BufferLocation = _vertexBuffer->GetGPUVirtualAddress();
	VertexBufferView.StrideInBytes = verticeSize;
	VertexBufferView.SizeInBytes = (UINT)(verticeSize * verticesCount);
}

void GeometryBase::CreateIndexBuffer(
	ID3D12Device &device,
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList,
	unsigned short indexes[],
	UINT indexesCount)
{
	const UINT indexBufferSize = sizeof(unsigned short) * indexesCount;

	CD3DX12_HEAP_PROPERTIES defaultHeapProperties(D3D12_HEAP_TYPE_DEFAULT);
	CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(indexBufferSize);
	DX::ThrowIfFailed(device.CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&_indexBuffer)));

	CD3DX12_HEAP_PROPERTIES uploadHeapProperties(D3D12_HEAP_TYPE_UPLOAD);
	DX::ThrowIfFailed(device.CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&indexBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&_indexUploadBuffer)));

	NAME_D3D12_OBJECT(_indexBuffer);

	D3D12_SUBRESOURCE_DATA _indexBufferData;
	_indexBufferData.pData = reinterpret_cast<BYTE*>(indexes);
	_indexBufferData.RowPitch = indexBufferSize;
	_indexBufferData.SlicePitch = _indexBufferData.RowPitch;

	UpdateSubresources(commandsList.Get(), _indexBuffer.Get(), _indexUploadBuffer.Get(), 0, 0, 1, &_indexBufferData);

	CD3DX12_RESOURCE_BARRIER indexBufferBarier;
	indexBufferBarier =
		CD3DX12_RESOURCE_BARRIER::Transition(_indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER);
	commandsList->ResourceBarrier(1, &indexBufferBarier);

	IndexBufferView.BufferLocation = _indexBuffer->GetGPUVirtualAddress();
	IndexBufferView.SizeInBytes = indexBufferSize;
	IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
}

void GeometryBase::Cleanup()
{
	if (_vertexUploadBuffer)
	{
		_vertexUploadBuffer.Reset();
	}
	if (_indexUploadBuffer)
	{
		_indexUploadBuffer.Reset();
	}
}