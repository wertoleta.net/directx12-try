﻿#include "pch.h"
#include "XmlHelper.h"
#include <codecvt>
#include <locale>

namespace Tringine
{
	namespace Util
	{
		XmlHelper::XmlHelper()
		{
		}


		XmlHelper::~XmlHelper()
		{
		}

		std::wstring XmlHelper::GetWStringAttributeValue(TiXmlElement* xml, char* attributeName, std::wstring defaultValue)
		{
			if (xml == nullptr)
			{
				return defaultValue;
			}
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			const char* attributeValue = xml->Attribute(attributeName);
			if (attributeValue != nullptr)
			{
				return converter.from_bytes(attributeValue);
			}
			return defaultValue;
		}

		std::string XmlHelper::GetStringAttributeValue(TiXmlElement* xml, char* attributeName, std::string defaultValue)
		{
			if (xml == nullptr)
			{
				return defaultValue;
			}
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
			const char* attributeValue = xml->Attribute(attributeName);
			if (attributeValue != nullptr)
			{
				return std::string(attributeValue);
			}
			return defaultValue;
		}

		int XmlHelper::GetIntAttributeValue(TiXmlElement* xml, char* attributeName, int defaultValue)
		{
			if (xml == nullptr)
			{
				return defaultValue;
			}
			const char* attributeValue = xml->Attribute(attributeName);
			if (attributeValue != nullptr)
			{
				return atoi(attributeValue);
			}
			return defaultValue;
		}

		float XmlHelper::GetFloatAttributeValue(TiXmlElement* xml, char* attributeName, float defaultValue)
		{
			if (xml == nullptr)
			{
				return defaultValue;
			}
			const char* attributeValue = xml->Attribute(attributeName);
			if (attributeValue != nullptr)
			{
				return atof(attributeValue);
			}
			return defaultValue;
		}
	}
}