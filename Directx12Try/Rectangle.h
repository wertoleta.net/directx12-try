﻿#pragma once
#include "Entity.h"
#include "TexturedEntity.h"
#include "PlaneTexturedGeometry.h"
#include "PlaneGeometry.h"
#include "Types.h"
#include "Texture.h"

using namespace Tringine::Geometry;
using namespace Tringine;

namespace Tringine
{
	namespace Entities
	{
		namespace Shapes
		{
			class Rectangle :
				public Entity
			{
			public:
				Rectangle(FloatRectangle rectangleData);

				void Draw(Drawer& drawer) override;
				void Resize(FloatSize newSize);
			};
		}

	}
}
