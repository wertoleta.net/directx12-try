#include "pch.h"
#include "Adventurer.h"
#include "TrinGame.h"

namespace Game
{
	const bool IsDebugStates = true;

	Adventurer::Adventurer(FloatPoint spawnPosition) : Sprite(L"adventurer-stand"),
		_jumpTimer(0),
		_bodyPhysicalOffset(0.0f, 0.0f),
		_slidedSide(Side::None)
	{
		Revive();
		SetPosition(spawnPosition.X, spawnPosition.Y);

		FloatRectangle rect = FloatRectangle(spawnPosition, IdleSize);
		_body = std::make_shared<RectangleBody>(rect, MovableType::Dynamic);
		_body->SetMaxVelocity(10.0f, 10.0f);
		Tringine::TrinGame::Instance->GetStage()->GetPhysicsSpace()->AddBody(_body);

		std::shared_ptr < InputManager> input = Tringine::TrinGame::Instance->GetInput();
		_upAction = input->GetAction(L"Up");
		_leftAction = input->GetAction(L"Left");
		_rightAction = input->GetAction(L"Right");
		_downAction = input->GetAction(L"Down");
		_attackAction = input->GetAction(L"Attack");

		SetState(AdventurerState::AS_IDLE);
		//_nextState = AdventurerState::AS_IDLE;
	}


	Adventurer::~Adventurer()
	{
	}

	void Adventurer::Update(float timeScale)
	{
		Sprite::Update(timeScale);

		_somersaultTimer = fmaxf(_somersaultTimer - timeScale, 0);
		ProcessMovementInput();
		switch (_state)
		{
		case AdventurerState::AS_IDLE:
			ProcessIdle(timeScale);
			break;
		case AdventurerState::AS_RUN:
			ProcessRun(timeScale);
			break;
		case AdventurerState::AS_JUMP:
			ProcessJump(timeScale);
			break;
		case AdventurerState::AS_FALL:
			ProcessFall(timeScale);
			break;
		case AdventurerState::AS_CROUCH:
			ProcessJump(timeScale);
			break;
		case AdventurerState::AS_SOMERSAULT:
			ProcessSomersault(timeScale);
			break;
		case AdventurerState::AS_WALL_SLIDE:
			ProcessWallSlide(timeScale);
			break;
		case AdventurerState::AS_CORNER_GRAB:
			ProcessCornerGrab(timeScale);
			break;
		case AdventurerState::AS_CORNER_CLIMB_UP:
			ProcessCornerClimbUp(timeScale);
			break;
		case AdventurerState::AS_CORNER_CLIMB_DOWN:
			ProcessCornerClimbDown(timeScale);
			break;
		case AdventurerState::AS_SLIDE:
			ProcessSlide(timeScale);
			break;
		case AdventurerState::AS_ATTACK_1:
			ProcessAttack1(timeScale);
			break;
		case AdventurerState::AS_ATTACK_2:
			ProcessAttack2(timeScale);
			break;
		case AdventurerState::AS_ATTACK_RUN:
			ProcessAttackRun(timeScale);
			break;
		case AdventurerState::AS_ATTACK_AIR_1:
			ProcessAttackAir1(timeScale);
			break;
		case AdventurerState::AS_ATTACK_AIR_2:
			ProcessAttackAir2(timeScale);
			break;
		}

		SetState(_nextState);

		FloatPoint bodyPosition = _body->GetCurrentPosition();
		SetPosition(XMFLOAT3(
			bodyPosition.X + _bodyPhysicalOffset.X + _bodyAnimationOffset.X,
			bodyPosition.Y + _bodyPhysicalOffset.Y + _bodyAnimationOffset.Y, 0.0f));
	}

	void Adventurer::ProcessAttack1(float timeScale)
	{
		if (IsAnimationFinished())
		{
			if (_attackAction->IsActive())
			{
				if (_lastPressedDirection == 0)
				{
					_nextState = AdventurerState::AS_ATTACK_2;
				}
				else
				{
					_nextState = AdventurerState::AS_ATTACK_RUN;
				}
			}
			else
			{
				_nextState = AdventurerState::AS_IDLE;
			}
		}
		ProcessFriction(timeScale);
	}

	void Adventurer::ProcessAttack2(float timeScale)
	{
		if (IsAnimationFinished())
		{
			if (_attackAction->IsActive())
			{
				if (_lastPressedDirection == 0)
				{
					_nextState = AdventurerState::AS_ATTACK_1;
				}
				else
				{
					_nextState = AdventurerState::AS_ATTACK_RUN;
				}
			}
			else
			{
				_nextState = AdventurerState::AS_IDLE;
			}
		}
		ProcessFriction(timeScale);
	}

	void Adventurer::ProcessAttackAir1(float timeScale)
	{
		if (_body->GetVelocity().Y > 0.0f)
		{
			ProcessFall(timeScale);
		}
		else
		{
			ProcessJump(timeScale);
		}
		if (_nextState == AdventurerState::AS_FALL || _nextState == AdventurerState::AS_JUMP || _nextState == AdventurerState::AS_ATTACK_AIR_1)
		{
			if (IsAnimationFinished())
			{
				if (_attackAction->IsActive())
				{
					_nextState = AdventurerState::AS_ATTACK_AIR_2;
				}
				else
				{
					_nextState = AdventurerState::AS_JUMP;
				}
			}
			else
			{
				_nextState = AdventurerState::AS_ATTACK_AIR_1;
			}
		}
	}

	void Adventurer::ProcessAttackAir2(float timeScale)
	{
		if (_body->GetVelocity().Y > 0.0f)
		{
			ProcessFall(timeScale);
		}
		else
		{
			ProcessJump(timeScale);
		}
		if (_nextState == AdventurerState::AS_FALL || _nextState == AdventurerState::AS_JUMP || _nextState == AdventurerState::AS_ATTACK_AIR_1)
		{
			if (IsAnimationFinished())
			{
				if (_attackAction->IsActive())
				{
					_nextState = AdventurerState::AS_ATTACK_AIR_1;
				}
				else
				{
					_nextState = AdventurerState::AS_JUMP;
				}
			}
			else
			{
				_nextState = AdventurerState::AS_ATTACK_AIR_2;
			}
		}
	}

	void Adventurer::ProcessAttackRun(float timeScale)
	{
		if (IsAnimationFinished())
		{
			if (_attackAction->IsActive())
			{
				if (_lastPressedDirection == 0)
				{
					_nextState = AdventurerState::AS_ATTACK_1;
				}
				else
				{
					_nextState = AdventurerState::AS_RUN;
				}
			}
			else
			{
				_nextState = AdventurerState::AS_IDLE;
			}
		}
		ProcessFriction(timeScale / 3.0f);
	}

	void Adventurer::ProcessIdle(float timeScale)
	{
		if (_downAction->IsFired())
		{
			BodyList downBodies = _body->GetTouchedBodies(Side::Bottom);
			Corners corners = _body->GetCorners();
			_slidedSide = Side::None;
			std::shared_ptr<RectangleBody> downBody;
			for (size_t index = 0; index < downBodies.size(); index++)
			{
				Corners downCorners = downBodies[index]->GetCorners();
				if (corners.IsBetweenLeftAndRight(downCorners.Left))
				{
					_slidedSide = Side::Right;
					downBody = downBodies[index];
					break;
				}
				else if (corners.IsBetweenLeftAndRight(downCorners.Right))
				{
					_slidedSide = Side::Left;
					downBody = downBodies[index];
					break;
				}
			}
			if (_slidedSide != Side::None)
			{
				Corners downBodyCorners = downBody->GetCorners();
				FloatPoint tmpPosition(0.0f, downBodyCorners.Top);
				if (_slidedSide == Side::Right)
				{
					tmpPosition.X = downBodyCorners.Left - IdleSize.Width - 1;
				}
				else
				{
					tmpPosition.X = downBodyCorners.Right + 1;
				}
				std::shared_ptr<RectangleBody> tmpBody = make_shared<RectangleBody>(FloatRectangle(tmpPosition, IdleSize), MovableType::Static);
				BodyList overlappedBodies = TrinGame::Instance->GetStage()->GetPhysicsSpace()->GetAllOverlaped(tmpBody);
				if (overlappedBodies.size() == 0)
				{
					_body->MoveTo(tmpPosition);
					_nextState = AdventurerState::AS_CORNER_CLIMB_DOWN;
				}
			}
		}
		if (_nextState != AdventurerState::AS_CORNER_CLIMB_DOWN)
		{
			ProcessMovement(timeScale);
			ProcessJump(timeScale);
		}
		if (_nextState == AdventurerState::AS_IDLE && _attackAction->IsActive())
		{
			_nextState = AdventurerState::AS_ATTACK_2;
		}
	}

	void Adventurer::ProcessRun(float timeScale)
	{
		if (_downAction->IsActive())
		{
			_nextState = AdventurerState::AS_SLIDE;
		}
		else
		{
			ProcessJump(timeScale);
		}
		if (_nextState == AdventurerState::AS_RUN && _attackAction->IsActive())
		{
			_nextState = AdventurerState::AS_ATTACK_RUN;
		}
	}

	void Adventurer::ProcessCornerClimbUp(float timeScale)
	{
		_body->SetVelocity(0.0f, 0.0f);
		if (IsAnimationFinished())
		{
			FloatSize size = _body->GetSize();
			_body->MoveOn(copysignf(size.Width / 2.0f, GetScale().x), -size.Height - 1);
			_nextState = AdventurerState::AS_IDLE;
		}
	}

	void Adventurer::ProcessCornerClimbDown(float timeScale)
	{
		_body->SetVelocity(0.0f, 0.0f);
		if (IsAnimationFinished())
		{
			FloatSize size = _body->GetSize();
			//_body->MoveOn(copysignf(size.Width / 2.0f, GetScale().x), -size.Height - 1);
			_nextState = AdventurerState::AS_CORNER_GRAB;
		}
	}

	void Adventurer::ProcessCornerGrab(float timeScale)
	{
		_body->SetVelocity(0.0f, 0.0f);
		if (_downAction->IsActive())
		{
			_nextState = AdventurerState::AS_WALL_SLIDE;
			_body->MoveOn(0.0f, 6.0f);
		}
		else if (_upAction->IsActive())
		{
			if (_rightAction->IsActive() && _body->IsSideTouched(Side::Left))
			{
				DoWallJump(Side::Left);
			}
			else if (_leftAction->IsActive() && _body->IsSideTouched(Side::Right))
			{
				DoWallJump(Side::Right);
			}
			else
			{
				_nextState = AdventurerState::AS_CORNER_CLIMB_UP;
			}
		}
	}

	void Adventurer::ProcessWallSlide(float timeScale)
	{
		_body->SetVelocity(0.0f, 1.0f);
		if (_body->IsSideCollided(Side::Bottom))
		{
			_nextState = AdventurerState::AS_IDLE;
		}
		bool isSlideEnded = true;
		BodiesList touchedBodies = _body->GetTouchedBodies(_slidedSide);
		float top = _body->GetCorners().Top;
		for (const shared_ptr<RectangleBody> body : touchedBodies)
		{
			float otherBottom = body->GetCorners().Bottom;
			float gap = otherBottom - top;
			if (gap >= MinSlideSideArea)
			{
				isSlideEnded = false;
				break;
			}
		}
		if (isSlideEnded || _downAction->IsFired())
		{
			_nextState = AdventurerState::AS_FALL;
			XMFLOAT3 scale = GetScale();
			SetScale(_slidedSide == Side::Left ? abs(scale.x) : -abs(scale.x), scale.y, scale.z);
		}
		else
		{
			if (_upAction->IsActive())
			{
				DoWallJump(_slidedSide);
			}
		}
		CheckFloor(timeScale);
	}

	void Adventurer::DoWallJump(Side wallSide)
	{
		if (wallSide == Side::Left)
		{
			_body->SetVelocity(1.0f, -3.0f);
			_jumpTimer = MaxJumpsCounter / 2.0f;
			_nextState = AdventurerState::AS_JUMP;
		}
		else if (wallSide == Side::Right)
		{
			_body->SetVelocity(-1.0f, -3.0f);
			_jumpTimer = MaxJumpsCounter / 2.0f;
			_nextState = AdventurerState::AS_JUMP;
		}
	}

	void Adventurer::ProcessSomersault(float timeScale)
	{
		ProcessFriction(timeScale);
		float somerSaultSpeed = abs(_body->GetVelocity().X);
		_animationSpeed = 0.3f * (somerSaultSpeed / (_body->GetMaxVelocity().X / 2));
		if (somerSaultSpeed <= 0.5)
		{
			_somersaultTimer = SomersaultDelay;
			_nextState = AdventurerState::AS_CROUCH;
		}
		CheckFloor(timeScale);
	}

	void Adventurer::ProcessFall(float timeScale)
	{
		FloatSize size = _body->GetSize();
		bool isSlideStarted = false;
		bool isCornerGrabbed = false;
		shared_ptr<RectangleBody> grabbedCorner;
		_slidedSide = Side::None;
		if (_body->IsSideCollided(Side::Left))
		{
			_slidedSide = Side::Left;
		}
		else if (_body->IsSideCollided(Side::Right))
		{
			_slidedSide = Side::Right;
		}
		if (_slidedSide != Side::None)
		{
			BodiesList touchedBodies = _body->GetTouchedBodies(_slidedSide);
			float top = _body->GetCorners().Top;
			for (const shared_ptr<RectangleBody> body : touchedBodies)
			{
				Corners otherCorners = body->GetCorners();
				float otherTop = otherCorners.Top;
				float otherBottom = otherCorners.Bottom;
				float topGap = top - otherTop;
				float bottomGap = otherBottom - top;
				if (topGap >= 1)
				{
					if (bottomGap >= MinSlideSideArea)
					{
						isSlideStarted = true;
					}
				}
				else if (topGap >= -4)
				{
					isCornerGrabbed = true;
					grabbedCorner = body;
				}
			}
		}
		if (!isSlideStarted && !isCornerGrabbed)
		{
			ProcessJump(timeScale);
		}
		else
		{
			if (isCornerGrabbed)
			{
				Corners c = grabbedCorner->GetCorners();
				if (_slidedSide == Side::Left)
				{
					_body->MoveTo(FloatPoint(c.Right + 1.0f, c.Top));
				}
				else
				{
					_body->MoveTo(FloatPoint(c.Left - size.Width, c.Top));
				}
				_nextState = AdventurerState::AS_CORNER_GRAB;
			}
			else
			{
				_nextState = AdventurerState::AS_WALL_SLIDE;
			}
		}
	}

	void Adventurer::CheckFloor(float timeScale)
	{
		if (!_body->IsSideTouched(Side::Bottom)) 
		{
			if (_body->GetVelocity().Y >= 0)
			{
				if (_state == AdventurerState::AS_SOMERSAULT)
				{
					_body->AddVelocity(-_body->GetVelocity().X / 1.5f, 0.0f);
				}
				if (_state == AdventurerState::AS_WALL_SLIDE)
				{
					_body->AddVelocity(0.0f, -0.3f);
				}
				else
				{
					_nextState = AdventurerState::AS_FALL;
				}
			}
			else if (_body->GetVelocity().Y < 0.0f)
			{
				_nextState = AdventurerState::AS_JUMP;
			}
		}
		else if (_state != AdventurerState::AS_JUMP)
		{
			_jumpTimer = MaxJumpsCounter;
		}
		else if (_nextState == AdventurerState::AS_JUMP)
		{
			_nextState = AdventurerState::AS_IDLE;
		}
	}

	void Adventurer::ProcessSlide(float timeScale)
	{
		FloatPoint velocity = _body->GetVelocity();
		if (fabsf(velocity.X) < 0.5f)
		{
			if (_downAction->IsActive())
			{
				_nextState = AdventurerState::AS_CROUCH;
			}
			else
			{
				_nextState = AdventurerState::AS_RUN;
			}
		}
		ProcessFriction(timeScale / 3.0);
		CheckFloor(timeScale);
	}

	void Adventurer::CheckJump(float timeScale)
	{

	}

	void Adventurer::ProcessJump(float timeScale)
	{
		ProcessMovement(timeScale);
		if (_jumpTimer > 0)
		{
			if (_upAction->IsActive())
			{
				float verticalVelocityK = -1.6f * timeScale * fmin(1.0f, _jumpTimer);
				if (MaxJumpsCounter - _jumpTimer < 0.0001f)
				{
					_body->SetVelocity(_body->GetVelocity().X, verticalVelocityK);
				}
				else
				{
					_body->AddVelocity(0.0f, verticalVelocityK);
				}
			}
			else
			{
				_jumpTimer = 0;
			}
			_jumpTimer = fmaxf(0.0f, _jumpTimer - timeScale);
		}
		CheckFloor(timeScale);
		CheckAirAttack();
	}

	void Adventurer::CheckAirAttack()
	{
		if ((_nextState == AdventurerState::AS_JUMP || _nextState == AdventurerState::AS_FALL) && _attackAction->IsActive())
		{
			_nextState = AdventurerState::AS_ATTACK_AIR_1;
		}
	}

	void Adventurer::SetState(AdventurerState newState)
	{
		if (_state == newState)
		{
			return;
		}
		_state = newState;
		FloatPoint velocity = _body->GetVelocity();
		XMFLOAT3 scale = GetScale();
		std::wstring newStateString = L"";
		FloatPoint initialOffset = _bodyPhysicalOffset;
		bool _isNeedCompensateOffset = true;
		float scaleXK = _slidedSide == Side::Left ? -1 : 1;
		switch (_state)
		{
		case AdventurerState::AS_FALL:
			_animationSpeed = 0.1f;
			SwitchAnimation(L"adventurer-fall");
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Fall\n";
			break;
		case AdventurerState::AS_IDLE:
			_animationSpeed = 0.1f;
			SwitchAnimation(L"adventurer-idle");
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Idle\n";
			break;
		case AdventurerState::AS_JUMP:
			_animationSpeed = 0.1f;
			SwitchAnimation(L"adventurer-jump");
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Jump\n";
			break;
		case AdventurerState::AS_RUN:
			_animationSpeed = 0.1f;
			SwitchAnimation(L"adventurer-run");
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Run\n";
			break;
		case AdventurerState::AS_CROUCH:
			_animationSpeed = 0.1f;
			SwitchAnimation(L"adventurer-crouch");
			_body->SetSize(CrouchSize);
			_bodyPhysicalOffset.X = CrouchSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = CrouchSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Crouch\n";
			break;
		case AdventurerState::AS_SOMERSAULT:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-smrslt");
			_body->SetSize(CrouchSize);
			_bodyPhysicalOffset.X = CrouchSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = CrouchSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Somersault\n";
			break;
		case AdventurerState::AS_WALL_SLIDE:
			_animationSpeed = 0.1f;
			_body->SetVelocity(velocity.X, fminf(velocity.Y, 0.5f));
			SwitchAnimation(L"adventurer-wall-slide");
			newStateString = L"WallSlide\n";
			SetScale(_slidedSide == Side::Left ? -abs(scale.x) : abs(scale.x), scale.y, scale.z);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			break;
		case AdventurerState::AS_CORNER_GRAB:
			_animationSpeed = 0.1f;
			_body->SetVelocity(0.0f, 0.0f);
			SwitchAnimation(L"adventurer-crnr-grb");
			newStateString = L"CornerGrab\n";

			SetScale(scaleXK * abs(scale.x), scale.y, scale.z);
			_body->SetSize(IdleSize);
			_bodyAnimationOffset.X = scaleXK * 1.4 * IdleSize.Width / 2.0f;
			_bodyAnimationOffset.Y = -IdleSize.Height;
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			break;
		case AdventurerState::AS_CORNER_CLIMB_UP:
			_animationSpeed = 0.2f;
			SwitchAnimation(L"adventurer-crnr-clmb", false);
			newStateString = L"CornerClimb\n";

			_bodyAnimationOffset.X = copysignf(1.4 * IdleSize.Width / 2.0f, scale.x);
			_bodyAnimationOffset.Y = -IdleSize.Height;
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_body->SetSize(IdleSize);
			break;
		case AdventurerState::AS_CORNER_CLIMB_DOWN:
			_animationSpeed = 0.2f;
			SwitchAnimation(L"adventurer-crnr-clmb", false, true);
			newStateString = L"CornerClimbDown\n";

			SetScale(scaleXK * abs(scale.x), scale.y, scale.z);
			_bodyAnimationOffset.X = scaleXK * 1.4 * IdleSize.Width / 2.0f;
			_bodyAnimationOffset.Y = -IdleSize.Height;
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_body->SetSize(IdleSize);
			_body->SetVelocity(0.0f, 0.0f);
			break;
		case AdventurerState::AS_SLIDE:
			_animationSpeed = 0.3;
			SwitchAnimation(L"adventurer-slide");
			newStateString = L"Slide\n";

			_bodyPhysicalOffset.X = CrouchSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = CrouchSize.Height;
			_bodyAnimationOffset = FloatPoint(copysignf(10.0f, scale.x), 0.0f);
			_body->SetSize(CrouchSize);
			break;
		case AdventurerState::AS_ATTACK_1:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-attack1", false);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Attack1\n";
			break;
		case AdventurerState::AS_ATTACK_2:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-attack2", false);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Attack2\n";
			break;
		case AdventurerState::AS_ATTACK_RUN:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-attack3", false);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"AttackRun\n";
			_body->AddVelocity(copysignf(fmaxf(0.0f, 1.5f - fabsf(velocity.X)), scale.x), 0.0f);
			break;
		case AdventurerState::AS_ATTACK_AIR_1:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-air-attack1", false);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Air attack 1\n";
			if (velocity.Y > 0.0f)
			{
				_body->AddVelocity(-velocity.X / 2.0f, -4.0f);
			}
			break;
		case AdventurerState::AS_ATTACK_AIR_2:
			_animationSpeed = 0.3f;
			SwitchAnimation(L"adventurer-air-attack2", false);
			_body->SetSize(IdleSize);
			_bodyPhysicalOffset.X = IdleSize.Width / 2.0f;
			_bodyPhysicalOffset.Y = IdleSize.Height;
			_bodyAnimationOffset = FloatPoint(0.0f, 0.0f);
			newStateString = L"Air attack 2\n";
			if (velocity.Y > 0.0f)
			{
				_body->AddVelocity(-velocity.X / 2.0f, -4.0f);
			}
			break;
		}
		if (IsDebugStates)
		{
			OutputDebugStringW(newStateString.data());
		}
		FloatPoint offsetDifference = initialOffset - _bodyPhysicalOffset;
		_body->MoveOn(offsetDifference);
	}

	void Adventurer::ProcessFriction(float timeScale)
	{
		FloatPoint currentVelocity = _body->GetVelocity();
		if (abs(currentVelocity.X) < 0.1f)
		{
			_body->SetVelocity(0.0f, currentVelocity.Y);
		}
		else
		{
			float step = fmax(abs(currentVelocity.X) / 8.0f, 0.1f);
			_body->AddVelocity(-copysign(step * timeScale, currentVelocity.X), 0.0f);
		}
	}

	void Adventurer::ProcessMovementInput()
	{
		if (!_leftAction->IsActive() && !_rightAction->IsActive())
		{
			_lastPressedDirection = 0;
			_accelerationTimer = 0;
		}
		else if (_leftAction->IsFired())
		{
			_lastPressedDirection = -1;
		}
		else if (_rightAction->IsFired())
		{
			_lastPressedDirection = 1;
		}
	}

	void Adventurer::ProcessMovement(float timeScale)
	{
		FloatPoint currentVelocity = _body->GetVelocity();
		if (_lastPressedDirection == 0)
		{
			if (_state != AdventurerState::AS_JUMP)
			{
				if (_downAction->IsActive())
				{
					_nextState = AdventurerState::AS_CROUCH;
				}
				else
				{
					_nextState = AdventurerState::AS_IDLE;
				}
			}
			ProcessFriction(timeScale);
		}
		else
		{
			float currentAcceleration = 0.0f;
			if (!_body->IsSideTouched(Side::Bottom) || _state == AdventurerState::AS_JUMP)
			{
				_accelerationTimer = timeScale;
				currentAcceleration = 1.0f;
			}
			else
			{
				_accelerationTimer += timeScale;
				currentAcceleration = fmin(_accelerationTimer / 10.0f, 1.0f);
			}
			if (_state == AdventurerState::AS_CROUCH && _downAction->IsActive())
			{
				ProcessFriction(timeScale);
				if (_somersaultTimer == 0)
				{
					float maxControlledVelocity = (float)copysign(_body->GetMaxVelocity().X / 2.0f, _lastPressedDirection);
					float k = fmax(abs(maxControlledVelocity) * 2, 0.0f);
					_body->AddVelocity(k * _lastPressedDirection * timeScale, 0.0f);
					_nextState = AdventurerState::AS_SOMERSAULT;
				}
			}
			else if (_state != AdventurerState::AS_SOMERSAULT)
			{
				float maxControlledVelocity = (float)copysign(_body->GetMaxVelocity().X / 2.0f, _lastPressedDirection);
				float k = fmax(abs(maxControlledVelocity - currentVelocity.X), 0.0f) / abs(maxControlledVelocity);
				if (!_body->IsSideTouched(Side::Bottom))
				{
					k /= 10.0f;
				}
				_body->AddVelocity(k * _lastPressedDirection * currentAcceleration * timeScale, 0.0f);
				if (_state != AdventurerState::AS_JUMP)
				{
					_nextState = AdventurerState::AS_RUN;
				}
			}
		}

		if (_lastPressedDirection != 0)
		{
			SetScale((float)_lastPressedDirection, 1.0f, 1.0f);
		}
	}

	void Adventurer::Draw(Drawer& drawer)
	{
		Sprite::Draw(drawer);
	}
}