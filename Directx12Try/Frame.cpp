﻿#include "pch.h"
#include "Frame.h"
#include "DynamicResource.h"

namespace Tringine
{
	Frame::Frame()
	{
		_geometry = nullptr;
	}


	Frame::~Frame()
	{
	}

	void Frame::SetPositionOnTexture(int x, int y, TextureSize textureSize)
	{
		_texturePosition.X = x;
		_texturePosition.Y = y;
		_positionOnTexture.X = 1.0f * x / textureSize.Width;
		_positionOnTexture.Y = 1.0f * y / textureSize.Height;
		this->GenerateGeometry();
	}

	void Frame::SetSizeOnTexture(int width, int height, TextureSize textureSize)
	{
		_pixelSize.Width = width;
		_pixelSize.Height = height;
		_sizeOnTexture.Width = 1.0f * width / textureSize.Width;
		_sizeOnTexture.Height = 1.0f * height / textureSize.Height;
		this->GenerateGeometry();
	}

	void Frame::SetOrigin(float x, float y)
	{
		_origin.X = x;
		_origin.Y = y;
		this->GenerateGeometry();
	}

	void Frame::GenerateGeometry()
	{
		
		_geometry = TringineDx::MakeDynamicResource<PlaneTexturedGeometry>(
			XMFLOAT2((float) this->_pixelSize.Width, (float) this->_pixelSize.Height),
			XMFLOAT2(this->_origin.X * this->_pixelSize.Width, this->_origin.Y * this->_pixelSize.Height),
			XMFLOAT4(this->_positionOnTexture.X, this->_positionOnTexture.Y, this->_positionOnTexture.X + this->_sizeOnTexture.Width, this->_positionOnTexture.Y + this->_sizeOnTexture.Height));
	}

	PixelSize Frame::GetPixelSize()
	{
		return _pixelSize;
	}

	std::shared_ptr<PlaneTexturedGeometry> Frame::GetGeometry()
	{
		return _geometry;
	}
}