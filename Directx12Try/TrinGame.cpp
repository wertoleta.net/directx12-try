﻿#include "pch.h"

#include "TrinGame.h"
#include "Structures.h"
#include "BoxGeometry.h"
#include "ResourceManager.h"
#include "Label.h"
#include "..\Common\DirectXHelper.h"
#include <ppltasks.h>
#include <synchapi.h>

using namespace Directx12Try;
using namespace TringineDx;
using namespace Tringine::Geometry;
using namespace Tringine;

using namespace Concurrency;
using namespace DirectX;
using namespace Microsoft::WRL;
using namespace Windows::Foundation;
using namespace Windows::Storage;

TrinGame *TrinGame::Instance = nullptr;

// Загружает шейдеры вершин и пикселей из файлов и создает геометрию куба.
TrinGame::TrinGame(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
	_isLoadingComplete(false),
	_isNeedCreateWindowSizeDependedResources(false),
	_input(new InputManager())
{
	LoadState();

	srand((unsigned int) time(NULL));

	CreateDeviceDependentResources(deviceResources);

	TrinGame::Instance = this;
}

TrinGame::~TrinGame()
{
}

void TrinGame::CreateDeviceDependentResources(const std::shared_ptr<DX::DeviceResources> &deviceResources)
{
	auto createDeviceDependResourcesTask = _drawer.CreateDeviceDependentResources(deviceResources);

	createDeviceDependResourcesTask.then([this]() {
		CreateWindowSizeDependentResources(true);
		_isLoadingComplete = true;
	});
}

// Инициализирует параметры представления при изменении размера окна.
void TrinGame::CreateWindowSizeDependentResources(bool isForced)
{
	if (!_isLoadingComplete && !isForced)
	{
		return;
	}
	Size outputSize = _drawer.DeviceResources->GetOutputSize();

	_drawer.Camera->SetOrtho(outputSize.Width, outputSize.Height);
	
	D3D12_VIEWPORT viewport = _drawer.DeviceResources->GetScreenViewport();
	_drawer.Camera->SetViewport(viewport);

	Font::AllCalculatePixelToViewport(outputSize.Width, outputSize.Height);

	_isNeedCreateWindowSizeDependedResources = true;
}

void TrinGame::Update(DX::StepTimer const& timer)
{
	RealFps = timer.GetFramesPerSecond();

	if (!_isLoadingComplete)
	{
		return;
	}

	std::shared_ptr < Stage> stageSwitchTo = _nextStage;
	if (stageSwitchTo != nullptr)
	{
		if (_currentStage != nullptr)
		{
			ID3D12Fence* f = _drawer.LastFrameDrawedFence.Get();
			if (f != nullptr)
			{
				UINT64 val = f->GetCompletedValue();
				if (val == 0)
				{
					return;
				}
			}
		}
		std::shared_ptr < Stage> prevStage =_currentStage;
		_currentStage = stageSwitchTo;
		_nextStage = nullptr;

		ResourceManager::GetInstance()->RequestResources(stageSwitchTo->GetRequiredResources());

		_drawer.RootSignatureHelper.ConstantBuffer.ResetConstantIndexes();

		stageSwitchTo->Create();
		CreateWindowSizeDependentResources();
	}

	_input->Update();
	if (_currentStage != nullptr)
	{		
		_currentStage->Update((float)(timer.GetElapsedSeconds() / 0.016666));
	}
}

// Сохраняет текущее состояние визуализатора.
void TrinGame::SaveState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;
}

// Восстанавливает предыдущее состояние визуализатора.
void TrinGame::LoadState()
{
	auto state = ApplicationData::Current->LocalSettings->Values;
}

void TrinGame::SwitchStage(std::shared_ptr <Stage> stage)
{
	_nextStage = stage;
}

std::shared_ptr < Stage> TrinGame::GetStage()
{
	return _currentStage;
}

std::shared_ptr < InputManager> TrinGame::GetInput()
{
	return _input;
}

//Получение основной камеры
std::shared_ptr < Camera> TrinGame::GetCamera()
{
	return _drawer.Camera;
}

// Отрисовывает один кадр с помощью шейдеров вершин и пикселей.
bool TrinGame::Render()
{
	// Загрузка является асинхронной. Геометрию можно рисовать только после ее загрузки.
	if (!_isLoadingComplete || _nextStage != nullptr)
	{
		return false;
	}

	DX::ThrowIfFailed(_drawer.DeviceResources->GetCommandAllocator()->Reset());
	DX::ThrowIfFailed(_drawer.CommandList->Reset(_drawer.DeviceResources->GetCommandAllocator(), PipelineStateHelper::Get(PipelineStateHelper::MeshGeomentyPipelineStateName).Get()));

	_drawer.Camera->Reset();

	_drawer.RootSignatureHelper.ConstantBuffer.SetCurrentFrameIndex(_drawer.DeviceResources->GetCurrentFrameIndex());
	Label::NextDrawCharacterNumber = 0;

	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList = _drawer.CommandList;

	if (_currentStage != nullptr)
	{
		_currentStage->PostUpdate(_drawer);
	}

	Microsoft::WRL::ComPtr<ID3D12Fence> createResourcesFence = nullptr;
	if (_isNeedCreateWindowSizeDependedResources)
	{
		_drawer.RootSignatureHelper.FillDepthStencil(*_drawer.DevicePointer, _drawer.DeviceResources->GetOutputSize());
		_isNeedCreateWindowSizeDependedResources = false;
	}
	else
	{
		PIXBeginEvent(commandList.Get(), 0, L"Main resource creating cycle");
		{
			DynamicResource::CleanupAllInitialized();
			createResourcesFence = DynamicResource::InitializeAllNew(*_drawer.DevicePointer, commandList);
		}
		PIXEndEvent(commandList.Get());

		PIXBeginEvent(commandList.Get(), 0, L"Main draw cycle");
		{
			// Установление использования этим кадром корневой подписи графики и куч дескрипторов.
			commandList->SetGraphicsRootSignature(_drawer.RootSignatureHelper.RootSignature.Get());

			// set the descriptor heap
			ID3D12DescriptorHeap* ppHeaps[] = { Texture::DescriptorHeap.Get() };
			commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

			// Установление окна просмотра и прямоугольника отсечения.
			commandList->RSSetViewports(1, &_drawer.DeviceResources->GetScreenViewport());
			commandList->RSSetScissorRects(1, &_drawer.Camera->ScissorRect);

			// Указание на то, что этот ресурс будет использоваться как целевой объект отрисовки.
			commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(_drawer.DeviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

			// Записать команды рисования.
			D3D12_CPU_DESCRIPTOR_HANDLE* renderTargetView = &_drawer.DeviceResources->GetRenderTargetView();
			D3D12_CPU_DESCRIPTOR_HANDLE* depthStencilView = &_drawer.RootSignatureHelper.DepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

			commandList->OMSetRenderTargets(1, renderTargetView, false, depthStencilView);

			commandList->ClearRenderTargetView(*renderTargetView, DirectX::Colors::CornflowerBlue, 0, nullptr);
			commandList->ClearDepthStencilView(_drawer.RootSignatureHelper.DepthStencilDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), D3D12_CLEAR_FLAG_DEPTH, 0.0f, 0, 0, nullptr);

			if (_currentStage != nullptr)
			{
				_currentStage->Draw(_drawer);
			}

			// Указание на то, что целевой объект отрисовки теперь будет использоваться, чтобы обозначать окончание выполнения списка команд.
			commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(_drawer.DeviceResources->GetRenderTarget(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
		}
		PIXEndEvent(commandList.Get());
	}
	DX::ThrowIfFailed(commandList->Close());
	
	ID3D12CommandList* ppCommandLists[] = { commandList.Get() };
	_drawer.CommandQueuePointer->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
	if (createResourcesFence != nullptr)
	{
		_drawer.CommandQueuePointer->Signal(createResourcesFence.Get(), 1);
	}
	Microsoft::WRL::ComPtr<ID3D12Fence> lastFrameFence;
	ThrowIfFailed(_drawer.DevicePointer->CreateFence(0, D3D12_FENCE_FLAGS::D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&lastFrameFence)));
	_drawer.CommandQueuePointer->Signal(lastFrameFence.Get(), 1);
	_drawer.LastFrameDrawedFence = lastFrameFence;

	DynamicResource::GarbageCollect(lastFrameFence);

	return true;
}