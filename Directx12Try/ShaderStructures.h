﻿#pragma once
#include "Common/DeviceResources.h"

/*
	Файл содержит описание всех структур которые используются для передачи в шейдеры
*/

namespace TringineDx
{
	//Буфер констант, используемый для отправки матриц MVP в шейдер вершин.
	struct ModelViewProjectionConstantBuffer
	{
		//MVP матрица
		DirectX::XMFLOAT4X4 mvpMatrix;
	};

	//Вершина меша
	struct Vertex
	{
		//Позиция в мире
		DirectX::XMFLOAT3 pos;
	};

	//Вершина текстурированного меша
	struct TexturedVertex
	{
		//Позиция в мире
		DirectX::XMFLOAT3 pos;
		//Позиция на текстуре
		DirectX::XMFLOAT2 texCoord;
	};

	//Текстовый меш
	struct TextVertex
	{
		//Базовый конструктор
		TextVertex(float r, float g, float b, float a, float u, float v, float tw, float th, float x, float y, float w, float h) : color(r, g, b, a), texCoord(u, v, tw, th), pos(x, y, w, h) {}
		
		//Позиция в мире
		DirectX::XMFLOAT4 pos;

		//Позиция на текстуре
		DirectX::XMFLOAT4 texCoord;

		//Цвет
		DirectX::XMFLOAT4 color;
	};
}