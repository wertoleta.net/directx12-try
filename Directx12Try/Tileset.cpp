﻿#include "pch.h"
#include "Tileset.h"
#include "XmlHelper.h"
#include <codecvt>
#include <locale>

using namespace Tringine::Util;

namespace Tringine
{
	Tileset::Tileset(std::wstring pathToTilesetFile)
	{
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		_path = pathToTilesetFile;
		if (_path.length() == 0)
		{
			throw "No path to tileset";
		}
		TiXmlDocument tilesetXml(StringHelper::ToStdString(_path));
		if (tilesetXml.LoadFile())
		{
			LoadFromXml(tilesetXml);
		}
		else
		{
			throw std::exception("Can't read tileset from file");
		}
	}

	void Tileset::LoadFromXml(TiXmlDocument &tilesetDocument)
	{
		TiXmlElement *tilesetElement = tilesetDocument.FirstChildElement();
		if (tilesetElement == nullptr)
		{
			return;
		}
		TiXmlHandle tilesetHandle(tilesetElement);
		_name = XmlHelper::GetWStringAttributeValue(tilesetElement, "name", std::wstring(L"Unknown name"));
		_tileSize.Width = (float)XmlHelper::GetIntAttributeValue(tilesetElement, "tilewidth", 32);
		_tileSize.Height = (float)XmlHelper::GetIntAttributeValue(tilesetElement, "tileheight", 32);
		_spacing = XmlHelper::GetIntAttributeValue(tilesetElement, "spacing", 0);
		_margin = XmlHelper::GetIntAttributeValue(tilesetElement, "margin", 0);
		_tileCount = XmlHelper::GetIntAttributeValue(tilesetElement, "tilecount", 0);
		_columns = XmlHelper::GetIntAttributeValue(tilesetElement, "columns", 0);

		TiXmlElement *imageElement = tilesetHandle.FirstChild("image").Element();
		bool imageProcessed = false;
		for (imageElement; imageElement; imageElement = imageElement->NextSiblingElement("image"))
		{
			if (imageProcessed)
			{
				throw std::exception("Multiple textures for map layers not supported");
			}
			_texturePath = XmlHelper::GetWStringAttributeValue(imageElement, "source", std::wstring(L""));

			_imageSize.Width = (float)XmlHelper::GetIntAttributeValue(imageElement, "width", 1);
			_imageSize.Height = (float)XmlHelper::GetIntAttributeValue(imageElement, "height", 1);
			imageProcessed = true;
		}
	}


	Tileset::~Tileset()
	{
	}

	std::wstring Tileset::GetTexturePath()
	{
		std::wstring directory;
		directory.resize(_path.length());
		errno_t error = _wsplitpath_s(_path.c_str(), NULL, 0, &directory[0], directory.size(), NULL, 0, NULL, 0);
		if (error == EINVAL)
		{
			throw std::exception("Wrong tileset path value");
		}
		directory = StringHelper::ResizeToRealLength(directory);
		return directory + _texturePath;
	}

	std::wstring Tileset::GetTextureName()
	{
		ResourceDescription description(ResourceType::RT_Texture, _texturePath);
		return description.GetName();
	}

	Margin Tileset::GetTileTextureAreaById(int tileId)
	{
		tileId--;
		int column = tileId % _columns;
		int row = tileId / _columns;

		float x = column * (_tileSize.Width + _spacing) + _margin;
		float y = row * (_tileSize.Height + _spacing) + _margin;

		Margin textureMargin;
		textureMargin.Left = x / _imageSize.Width;
		textureMargin.Right = (x + _tileSize.Width) / _imageSize.Width;
		textureMargin.Top = y / _imageSize.Height;
		textureMargin.Bottom = (y + _tileSize.Height) / _imageSize.Height;
		return textureMargin;
	}
}