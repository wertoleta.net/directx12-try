﻿#include "pch.h"
#include "TiledLayer.h"
#include "XmlHelper.h"
#include "Base64Helper.h"

using namespace Tringine::Util;

namespace Tringine
{
	const std::wstring TiledLayer::XmlElementName = L"layer";

	TiledLayer::TiledLayer(TiXmlElement *layerElement):TiledMapItem(TiledItemType::TileLayer, layerElement)
	{
		_id = XmlHelper::GetIntAttributeValue(layerElement, "id", -1);
		_name = XmlHelper::GetWStringAttributeValue(layerElement, "name", L"Unknown");
		_width = XmlHelper::GetIntAttributeValue(layerElement, "width", 0);
		_height = XmlHelper::GetIntAttributeValue(layerElement, "height", 0);

		TiXmlHandle layerHandle(layerElement);
		TiXmlElement *dataElement = layerHandle.FirstChild("data").Element();
		if (dataElement == nullptr)
		{
			throw new std::exception("Corrupted map layer. No data.");
		}

		std::string data = Base64Helper::Decode(std::string(dataElement->GetText()));
		for (int rowIndex = 0; rowIndex < _height; rowIndex++)
		{
			std::vector<int> *row = new std::vector<int>;
			for (int columnIndex = 0; columnIndex < _width; columnIndex++)
			{
				int shift = (rowIndex * _width + columnIndex) * 4;
				int tileIndex = 0;

				for (int byteIndex = 0; byteIndex < 4; byteIndex++)
				{
					uint8 val = data[shift + byteIndex];
					tileIndex +=  val << (byteIndex * 8);
				}
				row->push_back(tileIndex);
			}
			_tiles.push_back(row);
		}
	}

	TiledLayer::~TiledLayer()
	{
	}

	int TiledLayer::GetTileIdAtIndex(int index)
	{
		int row = index / _width;
		int column = index % _width;
		return _tiles.at(row)->at(column);
	}

	std::wstring TiledLayer::GetTilesetTextureName()
	{
		return _textureName;
	}

	void TiledLayer::SetTilesetTextureName(std::wstring textureName)
	{
		_textureName = textureName;
	}

	IntegerSize TiledLayer::GetSize()
	{
		return IntegerSize(_width, _height);
	}
}