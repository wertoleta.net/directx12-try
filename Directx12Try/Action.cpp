﻿#include "pch.h"
#include "Action.h"

namespace Tringine
{
	namespace Input
	{
		Action::Action():
			_activationsCount(0),
			_state(ActionState::ASNone),
			_nextState(ActionState::ASNone)
		{
		
		}

		void Action::Update() 
		{
			switch (_state)
			{
			case ActionState::ASFired:
				_state = ActionState::ASActive;
				break;
			case ActionState::ASEnds:
				_state = ActionState::ASNone;
				break;
			}
			if (_nextState != ActionState::ASNone)
			{
				_state = _nextState;
				_nextState = ActionState::ASNone;
			}
		}
		
		void Action::Activate()
		{
			if (_activationsCount == 0)
			{
				_nextState = ActionState::ASFired;
			}
			_activationsCount++;
		}

		void Action::Deactivate() 
		{
			_activationsCount--;
			if (_activationsCount == 0)
			{
				_nextState = ActionState::ASEnds;
			}
		}

		bool Action::IsFired()
		{
			return _state == ActionState::ASFired;
		}

		bool Action::IsActive()
		{
			return _state != ActionState::ASNone;
		}

		bool Action::IsEnds()
		{
			return _state == ActionState::ASEnds;
		}
	}
}