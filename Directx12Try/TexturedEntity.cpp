﻿#include "pch.h"
#include "TexturedEntity.h"


namespace Tringine
{
	namespace Entities
	{
		TexturedEntity::TexturedEntity()
		{
			_pipelinePointer = PipelineStateHelper::Get(PipelineStateHelper::MeshTexturedGeomentyPipelineStateName);
		}

		TexturedEntity::~TexturedEntity()
		{
		}

		void TexturedEntity::Draw(Drawer& drawer)
		{
			if (TexturePointer != nullptr)
			{
				drawer.CommandList->SetGraphicsRootDescriptorTable(1, TexturePointer->GetSrvHandle());
			}
			Entity::Draw(drawer);
		}
	}
}