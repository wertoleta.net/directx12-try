﻿#include "pch.h"
#include "TiledEntry.h"

namespace Tringine
{
	TiledEntry::TiledEntry(TiXmlElement* element)
	{
		_name = XmlHelper::GetWStringAttributeValue(element, "name", L"unknown");

		TiXmlHandle handle(element);
		TiXmlElement* propertiesElement = handle.FirstChild("properties").Element();
		if (propertiesElement != nullptr)
		{
			TiXmlHandle propertiesHandle(propertiesElement);
			TiXmlElement* propertyElement = propertiesHandle.FirstChild("property").Element();
			for (propertyElement; propertyElement; propertyElement = propertyElement->NextSiblingElement("property"))
			{
				std::wstring name = XmlHelper::GetWStringAttributeValue(propertyElement, "name", L"unknown");
				std::wstring value = XmlHelper::GetWStringAttributeValue(propertyElement, "value", L"unknown");
				_properties.insert({ name, value });
			}
		}
	}

	std::wstring TiledEntry::GetName()
	{
		return _name;
	}

	std::wstring TiledEntry::GetProperty(std::wstring name)
	{
		return GetProperty(name, L"");
	}

	std::wstring TiledEntry::GetProperty(std::wstring name, std::wstring defaultValue)
	{
		StringToStringMap::iterator iterator = _properties.find(name);
		if (iterator == _properties.end())
		{
			return defaultValue;
		}
		return iterator->second;
	}
}