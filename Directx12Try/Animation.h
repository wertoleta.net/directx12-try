﻿#pragma once

#include "Texture.h"
#include "Frame.h"
#include "Structures.h"
#include "Types.h"

namespace Tringine
{
	// ласс дл¤ работы с 2D анимаци¤ми
	class Animation
	{
	public:
		//“ип соответстви¤ анимации еЄ имени
		typedef std::map<std::wstring, std::shared_ptr<Animation>> StringToAnimationMap;
		
		//—писок всех загруженных анимаций. ƒоступ по имени.
		static StringToAnimationMap LoadedAnimations;

		/*
			Ѕазовый конструктор
			Texture &texture - текстура дл¤ анимации
			std::wstring &animationName - название анимации
		*/
		Animation(std::shared_ptr<Texture> texture, std::wstring &animationName);

		//Ѕазовый деструктор
		~Animation();

		/*
			ƒобавить кадр в анимацию
			Frame *frame - кадр дл¤ добавлени¤
		*/
		void AddFrame(std::shared_ptr < Frame>frame);

		/*
			ƒобавить серию кадров в анимацию
			FramesList &frameList - список кадров дл¤ добавлени¤
		*/
		void AddFrames(FramesList &frameList);

		//ѕолучить название анимации
		std::wstring GetName();

		//ѕолучить текстуру используемую анимацией
		std::shared_ptr<Texture> GetTexturePointer();

		/*
			ѕолучить кадр анимаици
			int frameIndex - номер кадра дл¤ получени¤
		*/
		std::shared_ptr < Frame> GetFrame(int frameIndex);

		//ѕолучение общего количества кадров в анимации
		size_t GetFramesCount();

		/*
			ƒобавление анимации в общий список
			std::wstring name - им¤ анимации дл¤ добавлени¤
			Texture *animation - указатель на анимацию
		*/
		static void Add(std::wstring name, std::shared_ptr < Animation> animation);

		/*
			”даление анимации 
			std::wstring name - им¤ анимации дл¤ добавлени¤
		*/
		static void Remove(std::wstring name);
		/*
			ƒобавление анимации в общий список
			Texture *animation - указатель на анимацию
		*/
		static void Add(std::shared_ptr < Animation> animation);

		/*
			ѕолучение анимации по имени.
			std::wstring name - им¤ анимации дл¤ поиска.
		*/
		static std::shared_ptr < Animation> Get(std::wstring name);

		/*
			”даление анимации по имени.
			std::wstring name - им¤ анимации дл¤ удалени¤.
		*/
		static void Delete(std::wstring name);

	private:
		//Ќазвание анимации
		std::wstring _name;

		//”казатель на текстуру
		std::shared_ptr<Texture> _texturePointer;

		// адры анимации
		FramesList _frames;

		//ћинимальный размер кадра
		PixelSize _minimalFrameSize = PixelSize(INT_MAX, INT_MAX);

		//ћаксимальный размер кадра
		PixelSize _maximalFrameSize = PixelSize(0, 0);
	};
}

