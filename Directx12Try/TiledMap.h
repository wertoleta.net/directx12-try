﻿#pragma once

#include "tinyxml.h"
#include "Tileset.h"
#include "TiledMapItem.h"
#include "TiledLayer.h"
#include "TiledObjectsGroup.h"
#include <stdlib.h>
#include <string>

namespace Tringine
{
	struct TilesetEntry
	{
		std::shared_ptr < Tileset> TilesetItem;
		UINT FirstTileId;
		TilesetEntry() {}
		TilesetEntry(std::shared_ptr < Tileset> tileset, UINT firstTileId)
		{
			TilesetItem = tileset;
			FirstTileId = firstTileId;
		}
	};

	class TiledMap
	{
	private:
		//“ип соответстви¤ карты еЄ имени
		typedef std::map<std::wstring, std::shared_ptr<TiledMap>> StringToTiledMapMap;

		//—писок всех загруженных текстур. ƒоступ по имени.
		static StringToTiledMapMap LoadedTiledMaps;

		enum Orientation { Orthogonal };
		enum RenderOrder { LeftUp };

		std::vector< std::shared_ptr<TilesetEntry>> _tilesets;
		std::vector< std::shared_ptr<TiledMapItem>> _layers;
		Orientation _orientation;
		RenderOrder _renderOrder;
		IntegerSize _size;
		IntegerSize _tileSize;
	public:
		TiledMap(TiXmlDocument xml);
		~TiledMap();

		std::vector<std::wstring> GetImagesToLoad();
		IntegerSize GetSize();
		IntegerSize GetTileSize();
		std::vector< std::shared_ptr<TiledMapItem>> GetItems();
		IntegerPoint GetTilePositionAtIndex(int index);
		Margin GetTileTextureAreaById(UINT tileId);
		std::shared_ptr < TilesetEntry> GetTilesetByTileId(UINT tileId);

		/*
			ƒобавление карты в общий список
			std::wstring name - им¤ текстуры дл¤ добавлени¤
			Texture *texture - ссылка на текстуру
		*/
		static void Add(std::wstring name, std::shared_ptr < TiledMap> texture);

		/*
			”даление карты
			std::wstring name - им¤ текстуры дл¤ добавлени¤
		*/
		static void Remove(std::wstring name);

		/*
			ѕолучение текстуры по имени.
			std::wstring name - им¤ текстуры дл¤ поиска.
		*/
		static std::shared_ptr < TiledMap> Get(std::wstring name);

		/*
			”даление текстуры по имени.
			std::wstring name - им¤ текстуры дл¤ удалени¤.
		*/
		static void Delete(std::wstring name);
	};
}
