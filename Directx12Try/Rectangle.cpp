﻿#include "pch.h"
#include "Rectangle.h"

namespace Tringine
{
	namespace Entities
	{
		namespace Shapes
		{
			Rectangle::Rectangle(FloatRectangle rectangleData) 
			{
				Resize(rectangleData.Size);
				SetPosition(rectangleData.Position.X, rectangleData.Position.Y);
				
				//TexturePointer = Texture::Get(L"Box");

				Revive();
			};

			void Rectangle::Draw(Drawer& drawer)
			{
				Entity::Draw(drawer);
			}

			void Rectangle::Resize(FloatSize newSize)
			{
				GeometryPointer = TringineDx::MakeDynamicResource<PlaneGeometry>(XMFLOAT2(newSize.Width, newSize.Height), XMFLOAT2(0.0f, 0.0f));
			}
		}
	}
}