﻿#pragma once
#include "Structures.h"
#include "Types.h"
#include "PlaneTexturedGeometry.h"

using namespace Tringine::Geometry;

namespace Tringine
{
	// ласс представл¤ет собой кадр 2D анимации
	class Frame
	{
	public:
		//Базовый конструктор
		Frame();

		//Базовый деструктор
		~Frame();

		/*
			Установка положени¤ кадра на текстуре
			int x - x координата (в пиксел¤х) левого верхнего угла кадра на текстуре
			int y - y координата (в пиксел¤х) левого верхнего угла кадра на текстуре
			TextureSize textureSize - размер текстуры
		*/
		void SetPositionOnTexture(int x, int y, TextureSize textureSize);

		/*
			Установка размера кадра на текстуре
			int width - ширина (в пиксел¤х) кадра на текстуре
			int height - высота (в пиксел¤х) кадра на текстуре
			TextureSize textureSize - размер текстуры
		*/
		void SetSizeOnTexture(int width, int height, TextureSize textureSize);

		/*
			Установка центра кадра
			float x - центр кадра по горизонтали от 0.0 до 1.0, где 0.0 - лева¤ граница кадра, а 1.0 - права¤ граница кадра
			float y - центр кадра по вертикали от 0.0 до 1.0, где 0.0 - верхн¤¤ граница кадра, а 1.0 - нижн¤¤ граница кадра
		*/
		void SetOrigin(float x, float y);

		//Получение размера кадра в пиксел¤х
		PixelSize GetPixelSize();

		//Получение геометрии кадра
		std::shared_ptr<PlaneTexturedGeometry> GetGeometry();
	private:
		//Положение на текстуре
		IntegerPoint _texturePosition;

		//Размер в пиксел¤х
		PixelSize _pixelSize;

		//Относительное положение на текстуре
		TextureRelativePosition _positionOnTexture;

		//Относительный размер на текстуре
		TextureRelativeSize _sizeOnTexture;

		//Центр кадра
		Offset _origin;

		//Геометри¤ кадра
		std::shared_ptr<PlaneTexturedGeometry> _geometry;

		//Генераци¤ соответствующей геометрии
		void GenerateGeometry();
	};
}

//Список кадров
typedef std::vector< std::shared_ptr < Frame>> FramesList;
