﻿#pragma once
#include <string>
#include <stdlib.h>
#include "StringHelper.h"

using namespace Tringine::Util;

namespace Tringine
{
	enum ResourceScope
	{
		RS_Game,
		RS_Stage
	};

	enum ResourceType
	{
		RT_Texture,
		RT_Map,
		RT_Font,
		RT_SpriteSheet
	};

	class ResourceDescription
	{
	private:
		ResourceType _type;
		std::wstring _path;
		ResourceScope _scope;

	public:
		ResourceDescription(ResourceType type, std::wstring path);
		ResourceDescription(ResourceType type, ResourceScope scope, std::wstring path);

		ResourceType GetType();
		std::wstring GetPath();
		std::wstring GetName();
		ResourceScope GetScope();
	};
}
