﻿#pragma once

#include "Common/DeviceResources.h"
#include "Common/DirectXHelper.h"
#include "RootSignatureHelper.h"
#include "GeometryBase.h"
#include "BoxGeometry.h"
#include "ConstantBuffer.h"
#include "ShaderStructures.h"
#include "Base.h"

using namespace DX;
using namespace DirectX;
using namespace Tringine;
using namespace Tringine::Entities;
using namespace TringineDx;
using namespace Tringine::Geometry;

namespace Tringine
{
	namespace Entities
	{
		/*
			Класс представляяеющий объкты с графическим представлением в мире игры
			TODO: добавить матрицу трансформаций не только относительно родителя, но и относительно мира
		*/
		class Entity : public Base
		{
		private:
			//Обновление матриц трансформации
			void UpdateMatrixes();
			
			//Положение в мире
			XMFLOAT4 _position;

			//Поворот в мире
			XMFLOAT3 _rotation;

			//Масштаб
			XMFLOAT3 _scale;

		protected:
			//Указатель на пайплайн отрисовки (шейдер)
			Microsoft::WRL::ComPtr<ID3D12PipelineState> _pipelinePointer;

		public:
			//Указатель на геометрию
			std::shared_ptr<Tringine::Geometry::GeometryBase> GeometryPointer;

			//Матрица трансформаций относительно родителя
			XMFLOAT4X4 LocalMatrix;

			//Матрица поворота
			XMFLOAT4X4 RotationMatrix;

			//Матрица масштаба
			XMFLOAT4X4 ScaleMatrix;

			//Индекс в буфере констант
			int ConstantBufferIndex;

			//Буффер констрант где будет храниться матрица трансформаций относительно мира
			ModelViewProjectionConstantBuffer ConstantBuffer;

			//Базовый конструктор
			Entity();

			//Базовый деструктор
			virtual ~Entity();

			/*
				Функция пост-обновления. Высчитывает матрицу проекций для камер и заносит её в буфер констант
				Drawer &drawer - объект-отрисовщик
			*/
			void PostUpdate(Drawer& drawer) override;

			/*
				Отрисовка объекта
				Drawer &drawer - объект-отрисовщик
			*/
			void Draw(Drawer& drawer) override;

			/*
				Установка расположения объекта относительно родителя
				float x - целевая x координата объекта
				float y - целевая y координата объекта
				float z - целевая z координата объекта
			*/
			void SetPosition(float x = 0, float y = 0, float z = 0);

			/*
				Установка расположения объекта относительно родителя
				XMFLOAT3 newPosition - новое положение
			*/
			void SetPosition(XMFLOAT3 newPosition);

			//Получение своего положения в пространстве
			XMFLOAT3 GetPosition();

			/*
				Добавление вращения объекту
				XMFLOAT3 additionalRotation - дополнительный поворот по трём осям для добавления (Радианы)
			*/
			void AddRotation(XMFLOAT3 additionalRotation);

			/*
				Добавление вращения объекту
				float x - дополнительное вращение вокруг x оси (Радианы)
				float y - дополнительное вращение вокруг y оси (Радианы)
				float z - дополнительное вращение вокруг z оси (Радианы)
			*/
			void AddRotation(float x, float y, float z);

			/*
				Устанавливает вращение объекту
				float x - вращение вокруг x оси (Радианы)
				float y - вращение вокруг y оси (Радианы)
				float z - вращение вокруг z оси (Радианы)
			*/
			void SetRotation(float x, float y, float z);

			/*
				Устанавливает масштаб объекта
				float x - размер вдоль оси x
				float y - размер вдоль оси y
				float z - размер вдоль оси z
			*/
			void SetScale(float x, float y, float z);

			/*
				Получение масштаба сущности
			*/
			XMFLOAT3 GetScale();
		};
	}
}