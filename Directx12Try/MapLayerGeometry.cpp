﻿#include "pch.h"
#include "MapLayerGeometry.h"
#include "ShaderStructures.h"
#include "Structures.h"

using namespace TringineDx;

namespace Tringine
{
	namespace Geometry
	{
		MapLayerGeometry::MapLayerGeometry(std::shared_ptr < TiledMap> mapData, std::shared_ptr < TiledLayer> layer) :
			GeometryBase(),
			_layer(layer),
			_mapData(mapData)
		{
		}


		MapLayerGeometry::~MapLayerGeometry()
		{
		}

		void MapLayerGeometry::InitInstanceValues()
		{
			_indexCountPerInstance = 0;
			_instancesCount = 1;
			_startIndexLocation = 0;
			_baseVertexLocation = 0;
			_startInstanceLocation = 0;
		}

		void MapLayerGeometry::Initialize(
			ID3D12Device& device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList)
		{
			InitInstanceValues();
			std::vector<TexturedVertex> vertices;
			std::vector<unsigned short> indexes;
			IntegerSize size = _mapData->GetSize();
			IntegerSize tileSize = _mapData->GetTileSize();

			for (int tileIndex = 0; tileIndex < size.Height * size.Width; tileIndex++)
			{
				int tileId = _layer->GetTileIdAtIndex(tileIndex);
				if (tileId > 0)
				{
					IntegerPoint tilePosition = _mapData->GetTilePositionAtIndex(tileIndex);
					float left = float(tilePosition.X * tileSize.Width);
					float top = float(tilePosition.Y * tileSize.Height);
					float right = float(left + tileSize.Width);
					float bottom = float(top + tileSize.Height);
					float front = 0.0f;

					Margin textureArea = _mapData->GetTileTextureAreaById(tileId);

					vertices.push_back({ XMFLOAT3(left, top, front), XMFLOAT2(textureArea.Left, textureArea.Top) });
					vertices.push_back({ XMFLOAT3(right, top, front), XMFLOAT2(textureArea.Right, textureArea.Top) });
					vertices.push_back({ XMFLOAT3(right, bottom, front), XMFLOAT2(textureArea.Right, textureArea.Bottom) });
					vertices.push_back({ XMFLOAT3(left, bottom, front), XMFLOAT2(textureArea.Left, textureArea.Bottom) });

					unsigned short firstIndex = (unsigned short)vertices.size() - 4;
					indexes.push_back(firstIndex);
					indexes.push_back(firstIndex + 1);
					indexes.push_back(firstIndex + 2);
					indexes.push_back(firstIndex + 3);
					indexes.push_back(firstIndex);
					indexes.push_back(firstIndex + 2);
					_indexCountPerInstance += 6;
				}
			}

			TexturedVertex* vertexPointer = &vertices[0];
			CreateVertexBuffer(device, commandsList, reinterpret_cast<BYTE*>(vertexPointer), sizeof(TexturedVertex), (UINT) vertices.size());

			unsigned short* indexPointer = &indexes[0];
			CreateIndexBuffer(device, commandsList, indexPointer, (UINT) indexes.size());
		}
	}
}