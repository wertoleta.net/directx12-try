﻿#pragma once

#include "Common/DirectXHelper.h"
#include "Common/DeviceResources.h"
#include "collection.h"
#include "DynamicResource.h"

using namespace TringineDx;


namespace Tringine
{
	class Texture : public DynamicResource
	{
	private:
		//Тип соответствия текстуры её имени
		typedef std::map<std::wstring, std::shared_ptr<Texture>> StringToTextureMap;

		//Список всех загруженных текстур. Доступ по имени.
		static StringToTextureMap LoadedTextures;

		//WindowsImagingComponent для загрузки изображений
		static IWICImagingFactory* WicFactory;

		/*
			Возвращает количество бит на пиксель для заданного формата
			DXGI_FORMAT& dxgiFormat - формат для которого нужно получить бит на пиксель.
		*/
		static int GetDXGIFormatBitsPerPixel(DXGI_FORMAT& dxgiFormat);

		/*
			Получение DXGI совместимого формата из WIC формата
		*/
		static WICPixelFormatGUID GetConvertToWICFormat(WICPixelFormatGUID& wicFormatGUID);

		/*
			Получение соответствующего DXGI формата из WIC формата
		*/
		static DXGI_FORMAT GetDXGIFormatFromWICFormat(WICPixelFormatGUID& wicFormatGUID);

		/*
			Image data
		*/
		BYTE* _data = nullptr;

		/*
			Total image size (in bytes)
		*/
		int _dataSize = 0;

		/*
			Image bytes per row
		*/
		int _bytesPerRow = 0;

		/*
			Image description
		*/
		D3D12_RESOURCE_DESC _description = {};

		//Вью на ресурс
		D3D12_GPU_DESCRIPTOR_HANDLE SrvHandle;

	public:
		/*
			Создание текстуры из данных.

			BYTE* data - Указатель на первый байт изображения в памяти.
			int size - Общий размер изображения в байтах.
			int imageBytesPerRow - Количество байт описывающих одну строку изображения.
			D3D12_RESOURCE_DESC& textureDescription - Описание формата загруженного файла.
		*/
		Texture(BYTE* data, int size, int imageBytesPerRow, D3D12_RESOURCE_DESC& textureDescription);

		//Максимальное количество текстур
		static const int MaxTextureDescriptors = 256;

		//Куча дескрипторов текстур
		static Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> DescriptorHeap;

		//Указатель на конец кучи дескрипторов текстур
		static int DescriptorHeapEndPointer;

		//Буфер данных текстуры
		Microsoft::WRL::ComPtr<ID3D12Resource> DataBuffer;

		//Буфер выгрузки текстуры в кучу
		Microsoft::WRL::ComPtr<ID3D12Resource> BufferUploadHeap;

		//Удалене буферов выгрузки текстуры
		void Cleanup();

		/*
			Инициализация ресурса
			ID3D12Device &device - устройство для которого инициализируется ресурс
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавляются команды на инициализацию ресурса
		*/
		void Initialize(
			ID3D12Device& device,
			Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList);

		D3D12_GPU_DESCRIPTOR_HANDLE GetSrvHandle();

		/*
			Добавление текстуры в общий список
			std::wstring name - имя текстуры для добавления
			Texture *texture - ссылка на текстуру
		*/
		static void Add(std::wstring name, std::shared_ptr<Texture> texture);

		/*
			Удаление текстуры
			std::wstring name - имя текстуры для удаления
		*/
		static void Remove(std::wstring name);

		/*
			Получение текстуры по имени.
			std::wstring name - имя текстуры для поиска.
		*/
		static std::shared_ptr<Texture> Get(std::wstring name);

		/*
			Удаление текстуры по имени.
			std::wstring name - имя текстуры для удаления.
		*/
		static void Delete(std::wstring name);

		/*
			Загрузка изображения из файла.
			LPCWSTR filename - путь к файлу изображения
		*/
		static std::shared_ptr<Texture> LoadFromFile(LPCWSTR filename);
	};
}