﻿#pragma once

#include "Common\DeviceResources.h"
#include "Content\ShaderStructures.h"
#include "Common\StepTimer.h"
#include "RootSignatureHelper.h"
#include "PipelineStateHelper.h"
#include "BoxGeometry.h"
#include "ResourceManager.h"
#include "Camera.h"
#include "Entity.h"
#include "Group.h"
#include "Drawer.h"
#include "MyCube.h"
#include "Stage.h"
#include "InputManager.h"

using namespace DX;
using namespace DirectX;
using namespace Tringine::Entities;
using namespace Tringine::Input;
using namespace Game;

namespace Tringine
{
	// Этот пример визуализатора создает экземпляр базового конвейера отрисовки.
	class TrinGame
	{
	public:
		//Ссылка на глобальный объект игры
		static TrinGame *Instance;

		//Актуальный ФПС
		unsigned int RealFps;

		/*
			Конструктор
			const std::shared_ptr<DX::DeviceResources>& deviceResources - ресурсы устройства на который будет выводится рендер
		*/
		TrinGame(const std::shared_ptr<DX::DeviceResources>& deviceResources);

		//Деструктор
		~TrinGame();

		/*
			(Пере)Создание ресурсов которые зависят от устройства
			const std::shared_ptr<DX::DeviceResources>& deviceResources - ресурсы устройства на который будет выводится рендер
		*/
		void CreateDeviceDependentResources(const std::shared_ptr<DX::DeviceResources> &deviceResources);

		/*
			(Пере)Создание ресурсов которые зависят от размера экрана
			bool isForced - пересоздавать ли ресурсы не зависимо от того, завершилась загрузка или нет.
		*/
		void CreateWindowSizeDependentResources(bool isForced = true);

		/*
			Основной цикл обновления
			DX::StepTimer const& timer - таймер в котором указано сколько времени прошло с прошлого обновления
		*/
		void Update(DX::StepTimer const& timer);

		/*
			Основной цикл отрисовки
		*/
		bool Render();

		//Сохранить текущее состояние игры
		void SaveState();

		//Загрузить текущее состояние игры
		void LoadState();

		//Переключить текущую сцену игры
		void SwitchStage(std::shared_ptr <Stage> stage);

		//Получить текущую сцену
		std::shared_ptr < Stage> GetStage();

		//Получение обработчика ввода
		std::shared_ptr < InputManager> GetInput();

		//Получение основной камеры
		std::shared_ptr < Camera> GetCamera();

	private:
		//Флаг указывающий на то завершилась загрузка ресурсов или нет.
		bool	_isLoadingComplete;

		bool	_isNeedCreateWindowSizeDependedResources;

		//Отрисовшик - занимается отрисовкой каждого объекта в игре
		TringineDx::Drawer _drawer;

		//Указатель на текущую сену игры
		std::shared_ptr < Stage> _currentStage;

		//Указатель на сцену игры на которую должно произойти переключение в следующем кадре
		std::shared_ptr < Stage> _nextStage;

		//Контроллер ввода
		std::shared_ptr < InputManager> _input;
	};
}

