﻿#pragma once
#include "GeometryBase.h"
#include "TiledMap.h"
#include "TiledLayer.h"

using namespace Tringine;

namespace Tringine
{
	namespace Geometry
	{
		class MapLayerGeometry :
			public GeometryBase
		{
		public:
			MapLayerGeometry(std::shared_ptr < TiledMap> mapData, std::shared_ptr < TiledLayer> layer);
			~MapLayerGeometry();

			/*
				Инициализация ресурсов геометрии карты
				ID3D12Device &device - устройство для которого инициализируется ресурс
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> &commandsList - список команд в который добавляются команды на инициализацию ресурса
			*/
			void Initialize(
				ID3D12Device& device,
				Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& commandsList);
		protected:
			void InitInstanceValues() override;
		private:
			std::shared_ptr < TiledMap> _mapData;
			std::shared_ptr < TiledLayer> _layer;
		};
	}
}
