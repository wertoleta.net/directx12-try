﻿#include "pch.h"
#include "SpriteSheetHelper.h"
#include "XmlHelper.h"
#include "Structures.h"

namespace Tringine
{
	namespace Util
	{
		typedef std::map<std::wstring, FramesList> StringToFrameListMap;

		SpriteSheetHelper::SpriteSheetHelper()
		{
		}


		SpriteSheetHelper::~SpriteSheetHelper()
		{
		}


		std::wstring SpriteSheetHelper::GetImageFilePathFromXml(TiXmlDocument spritesheetXml)
		{
			TiXmlElement* rootElement = spritesheetXml.FirstChildElement();
			if (rootElement == nullptr)
			{
				throw new std::exception("Can't load file path from xml. Invalid xml argument.");
			}

			TiXmlHandle rootHandle(rootElement);

			return XmlHelper::GetWStringAttributeValue(rootElement, "imagePath", L"");
		}

		void SpriteSheetHelper::LoadAnimationsFromXml(TiXmlDocument spriteSheetXml, std::shared_ptr<Texture> spritesheetTexture)
		{
			TiXmlElement* rootElement = spriteSheetXml.FirstChildElement();
			if (rootElement == nullptr)
			{
				throw new std::exception("Can't load animations from xml. Invalid xml argument.");
			}

			TiXmlHandle rootHandle(rootElement);

			std::wstring imagePath = XmlHelper::GetWStringAttributeValue(rootElement, "imagePath", L"");

			StringToFrameListMap framesByAnimationName = GetAnimationWithUniqueNames(rootHandle);
			for (StringToFrameListMap::iterator it = framesByAnimationName.begin(); it != framesByAnimationName.end(); ++it)
			{
				std::wstring animationName = it->first;
				FramesList* animationFrames = &it->second;
				std::shared_ptr < Animation> newAnimation = std::make_shared<Animation>(spritesheetTexture, animationName);
				newAnimation->AddFrames(*animationFrames);
				Animation::Add(newAnimation);
			}
		}

		StringToFrameListMap SpriteSheetHelper::GetAnimationWithUniqueNames(TiXmlHandle atlasHandle)
		{
			PixelSize imageSize;
			imageSize.Width = XmlHelper::GetIntAttributeValue(atlasHandle.Element(), "width", 0);
			imageSize.Height = XmlHelper::GetIntAttributeValue(atlasHandle.Element(), "height", 0);

			StringToFrameListMap framesByAnimationName;
			TiXmlElement* spriteElement = atlasHandle.FirstChild("sprite").Element();
			for (spriteElement; spriteElement; spriteElement = spriteElement->NextSiblingElement("sprite"))
			{
				std::wstring spriteName = XmlHelper::GetWStringAttributeValue(spriteElement, "n", L"");
				std::wstring animationName = GetAnimationNameFromSpriteName(spriteName);
				bool animationAlreadyExist = false;
				for (StringToFrameListMap::iterator it = framesByAnimationName.begin(); it != framesByAnimationName.end(); ++it) {
					if (it->first == animationName)
					{
						animationAlreadyExist = true;
						break;
					}
				}
				if (!animationAlreadyExist)
				{
					framesByAnimationName[animationName] = FramesList();
				}
				FramesList *frames = &framesByAnimationName[animationName];
				std::shared_ptr < Frame> newFrame = GetFrameBySpriteXml(spriteElement, imageSize);
				frames->push_back(newFrame);
			}
			return framesByAnimationName;
		}

		std::shared_ptr < Frame> SpriteSheetHelper::GetFrameBySpriteXml(TiXmlElement* spriteElement, TextureSize textureSize)
		{
			int x = XmlHelper::GetIntAttributeValue(spriteElement, "x", 0);
			int y = XmlHelper::GetIntAttributeValue(spriteElement, "y", 0);
			int width = XmlHelper::GetIntAttributeValue(spriteElement, "w", 0);
			int height = XmlHelper::GetIntAttributeValue(spriteElement, "h", 0);
			int originalWidth = XmlHelper::GetIntAttributeValue(spriteElement, "oW", 0);
			int originalHeight = XmlHelper::GetIntAttributeValue(spriteElement, "oH", 0);
			int croppedX = XmlHelper::GetIntAttributeValue(spriteElement, "oX", 0);
			int croppedY = XmlHelper::GetIntAttributeValue(spriteElement, "oY", 0);
			float originalOriginx = XmlHelper::GetFloatAttributeValue(spriteElement, "pX", 0.0f);
			float originalOriginy = XmlHelper::GetFloatAttributeValue(spriteElement, "pY", 0.0f);
			float originX = ((originalWidth * originalOriginx) - croppedX) / width;
			float originY = ((originalHeight * originalOriginy) - croppedY) / height;

			std::shared_ptr < Frame> newFrame = std::make_shared<Frame>();
			newFrame->SetPositionOnTexture(x, y, textureSize);
			newFrame->SetSizeOnTexture(width, height, textureSize);
			newFrame->SetOrigin(originX, originY);
			return newFrame;
		}

		std::wstring SpriteSheetHelper::GetAnimationNameFromSpriteName(std::wstring spriteName)
		{
			size_t animationNameStart = spriteName.find_last_of('/') + 1;
			size_t animationNameEnd = (int) fminf((float) spriteName.find_last_of('_'), (float) spriteName.find_last_of('-'));
			if (animationNameEnd == -1)
			{
				animationNameEnd = spriteName.length();
			}
			if (animationNameStart > animationNameEnd)
			{
				animationNameStart = animationNameEnd;
			}
			return spriteName.substr(animationNameStart, animationNameEnd - animationNameStart);
		}
	}
}