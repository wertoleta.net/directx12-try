﻿#include "pch.h"
#include "Animation.h"

using namespace Tringine;

namespace Tringine
{
	Animation::StringToAnimationMap Animation::LoadedAnimations;

	Animation::Animation(std::shared_ptr<Texture> texture, std::wstring &animationName)
	{
		_texturePointer = texture;
		_name = animationName;
	}

	Animation::~Animation()
	{
	}

	void Animation::AddFrame(std::shared_ptr < Frame> frame)
	{
		//Определение минимального/максимального размера кадра во всей анимации
		PixelSize frameSize = frame->GetPixelSize();
		if (frameSize.Width < _minimalFrameSize.Width)
		{
			_minimalFrameSize.Width = frameSize.Width;
		}
		if (frameSize.Height < _minimalFrameSize.Height)
		{
			_minimalFrameSize.Height = frameSize.Height;
		}
		if (frameSize.Width > _maximalFrameSize.Width)
		{
			_maximalFrameSize.Width = frameSize.Width;
		}
		if (frameSize.Height > _maximalFrameSize.Height)
		{
			_maximalFrameSize.Height = frameSize.Height;
		}

		//Добавление анимации в список
		_frames.push_back(frame);
	}

	void Animation::AddFrames(FramesList &frames)
	{
		for each (std::shared_ptr<Frame> frame in frames)
		{
			AddFrame(frame);
		}
	}

	std::wstring Animation::GetName()
	{
		return _name;
	}

	void Animation::Add(std::wstring name, std::shared_ptr < Animation> animation)
	{
		if (LoadedAnimations.count(name) > 0)
		{
			throw new std::exception("Animation with same name already exists");
		}
		LoadedAnimations.insert({ name, animation });
	}

	void Animation::Remove(std::wstring name)
	{
		std::shared_ptr < Animation> animation = Get(name);
		if (animation == nullptr)
		{
			throw std::exception("Animation for remove doesn't exist");
		}
		LoadedAnimations.erase(name);
	}

	void Animation::Add(std::shared_ptr < Animation> animation)
	{
		Add(animation->GetName(), animation);
	}

	std::shared_ptr<Texture> Animation::GetTexturePointer()
	{
		return _texturePointer;
	}

	std::shared_ptr < Frame> Animation::GetFrame(int frameIndex)
	{
		return this->_frames[frameIndex];
	}

	size_t Animation::GetFramesCount()
	{
		return this->_frames.size();
	}

	std::shared_ptr <Animation> Animation::Get(std::wstring name)
	{
		StringToAnimationMap::iterator iterator = LoadedAnimations.find(name);
		if (iterator == LoadedAnimations.end())
		{
			return nullptr;
		}
		return iterator->second;
	}

	void Animation::Delete(std::wstring name)
	{
		StringToAnimationMap::iterator iterator = LoadedAnimations.find(name);
		if (iterator == LoadedAnimations.end())
		{
			return;
		}
		LoadedAnimations.erase(iterator);
	}
}
