﻿#pragma once
#include "PrimitiveBody.h"
#include "Types.h"
#include "Entity.h"
#include "PlaneGeometry.h"
#include "Rectangle.h"

using namespace Tringine::Entities::Shapes;

namespace Tringine
{
	namespace Physics
	{
		struct Corners
		{
			float Left;
			float Right;
			float Top;
			float Bottom;

			Corners(float left, float top, float right, float bottom):
				Left(left),
				Top(top),
				Right(right),
				Bottom(bottom) {};

			bool IsBetweenTopAndBottom(float yCoordinate)
			{
				return (yCoordinate >= Top && yCoordinate <= Bottom);
			}

			bool IsBetweenLeftAndRight(float xCoordinate)
			{
				return (xCoordinate >= Left && xCoordinate <= Right);
			}
		};

		enum Side
		{
			None			= 0b0000,
			Left			= 0b0001,
			Top				= 0b0010,
			LeftTop			= 0b0011,
			Right			= 0b0100,
			Horizontal		= 0b0101,
			RightTop		= 0b0110,
			HorizontalTop	= 0b0111,
			Bottom			= 0b1000,
			BottomLeft		= 0b1001,
			Vertical		= 0b1010,
			VerticalLeft	= 0b1011,
			BottomRight		= 0b1100,
			HorizontalBottom= 0b1101,
			VerticalRight	= 0b1110,
			All				= 0b1111
		};

		inline Side operator|(Side a, Side b)
		{
			return static_cast<Side>(static_cast<int>(a) | static_cast<int>(b));
		}

		inline Side operator&(Side a, Side b)
		{
			return static_cast<Side>(static_cast<int>(a) & static_cast<int>(b));
		}

		class RectangleBody;
		typedef std::vector<std::shared_ptr<RectangleBody>> BodiesList;
		typedef std::map<Side, BodiesList*> SideToBodyMap;

		class RectangleBody :
			public PrimitiveBody
		{
		private:
			FloatSize _size;
			Corners _corners;
			Corners _previousCorners;
			std::shared_ptr < Rectangle>_debugEntity;
			Side _solidSides;
			Side _collidedSides;
			Side _touchedSides;
			SideToBodyMap _touchedBodies;
			SideToBodyMap _collidedBodies;

		protected:
			void UpdateCorners();

		public:
			RectangleBody(FloatRectangle size);
			RectangleBody(FloatRectangle size, MovableType type); 

			bool IsOverlaps(std::shared_ptr < RectangleBody>anotherRectangle);
			bool IsTouches(std::shared_ptr < RectangleBody>anotherRectangle);
			void SetSize(FloatSize newSize);
			void MoveTo(FloatPoint newPosition);
			FloatSize GetOverlapSize(std::shared_ptr < RectangleBody> anotherRectangle);
			FloatSize GetSize();
			Corners GetCorners();
			Corners GetPreviousCorners();
			std::shared_ptr <Entity> CreateDebugEntity();
			void UpdateDebugEntity();
			bool IsSideSolid(Side side);
			void SetSolidSides(int sides);
			void SetSolidSides(Side sides);

			bool IsSideCollided(Side side);
			void AddCollidedSide(Side side);

			bool IsSideTouched(Side side);
			BodiesList GetTouchedBodies(Side side);
			void AddTouchedBody(Side side, shared_ptr<RectangleBody> body);

			void ResetInteractions();

			void StorePreviousCorners();
		};
	}
}

