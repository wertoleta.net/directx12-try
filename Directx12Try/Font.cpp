﻿#include "pch.h"
#include "Font.h"

namespace Tringine
{
	typedef std::map<std::wstring, std::shared_ptr<Font>> StringToFontMap;
	StringToFontMap Font::LoadedFonts;

	Font::Font()
	{
	}


	Font::~Font()
	{
	}

	void Font::Add(std::wstring name, std::shared_ptr<Font> font)
	{
		if (LoadedFonts.count(name) > 0)
		{
			throw std::exception("Font with same name already exists");
		}
		LoadedFonts.insert({ name, font });
	}

	void Font::Remove(std::wstring name)
	{
		std::shared_ptr<Font> font = Get(name);
		if (font == nullptr)
		{
			throw std::exception("Font with same name doesnot exist");
		}
		LoadedFonts.erase(name);
	}

	std::shared_ptr<Font> Font::Get(std::wstring name)
	{
		StringToFontMap::iterator iterator = LoadedFonts.find(name);
		if (iterator == LoadedFonts.end())
		{
			return nullptr;
		}
		return iterator->second;
	}

	std::shared_ptr<Font> Font::GetFirst()
	{
		StringToFontMap::iterator iterator = LoadedFonts.begin();
		if (iterator == LoadedFonts.end())
		{
			return nullptr;
		}
		return iterator->second;
	}

	void Font::Delete(std::wstring name)
	{
		StringToFontMap::iterator iterator = LoadedFonts.find(name);
		if (iterator == LoadedFonts.end())
		{
			return;
		}
		std::shared_ptr<Font> font = iterator->second;
		LoadedFonts.erase(iterator);
	}

	void Font::AllCalculatePixelToViewport(float screenWidth, float screenHeight)
	{
		StringToFontMap::iterator iterator;
		for (iterator = LoadedFonts.begin(); iterator != LoadedFonts.end(); iterator++)
		{
			std::shared_ptr<Font> font = iterator->second;
			font->CalculatePixelToViewport(screenWidth, screenHeight);
		}
	}

	FontChar* Font::GetChar(int charId)
	{
		for (int i = 0; i < numCharacters; ++i)
		{
			if (charId == CharPointerList[i].id)
				return &CharPointerList[i];
		}
		return nullptr;
	}

	void Font::CalculatePixelToViewport(float screenWidth, float screenHeight)
	{
		ViewportPadding.Left = PixelPadding.Left / screenWidth;
		ViewportPadding.Right = PixelPadding.Right / screenWidth;
		ViewportPadding.Top = PixelPadding.Top / screenHeight;
		ViewportPadding.Bottom = PixelPadding.Bottom / screenHeight;
		ViewportLineHeight = PixelLineHeight / screenHeight;
		ViewportBaseHeight = PixelBaseHeight / screenHeight;

		for (int i = 0; i < numCharacters; i++)
		{
			FontChar *c = &CharPointerList[i];
			c->ViewportSize.Width = c->PixelSize.Width / screenWidth;
			c->ViewportSize.Height = c->PixelSize.Height / screenHeight;
			c->ViewportOffset.X = c->PixelOffset.X / screenWidth;
			c->ViewportOffset.Y = c->PixelOffset.Y / screenHeight;
			c->ViewportAdvanceX = c->PixelAdvanceX / screenWidth;
			for (int j = 0; j < c->numKernings; j++)
			{
				c->KerningsList[j].ViewportAmount = c->KerningsList[j].PixelAmount / screenWidth;
			}
		}
	}
}